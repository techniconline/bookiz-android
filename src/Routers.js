import React, {Component, Fragment} from 'react';
import {AsyncStorage, Linking, I18nManager} from 'react-native';
import {Router, Stack, Scene, Tabs, Drawer, Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import SplashScreen from 'react-native-splash-screen'
import { connect } from 'react-redux';
import {ACCESS_TOKEN} from "./redux/types/auth";
import Login from "./pages/Auth/Login";
import Register from "./pages/Auth/Register";
import Home from "./pages/Home/Home";
import Favorites from "./pages/Favorites/Favorites";
import Settings from "./pages/Settings/Settings";
import Entities from "./pages/Entities/Entities";
import Entity from "./pages/Entities/Entity/Entity";
import Sidebar from "./Sidebar";
import Services from "./pages/Services/Services";
import Basket from "./pages/Basket/Basket";
import Auth from "./pages/Auth/Auth";
import Search from "./pages/Search/Search";
import {ACCESS_LANGUAGE} from "./redux/types";
import { save_language, get_cart, get_user } from './redux/actions';
import Payment from "./pages/Basket/Payment";
import Bookings from "./pages/Bookings/Bookings";
import Booking from "./pages/Bookings/Booking";
import Language from "./pages/Settings/Language/Language";
import Profile from "./pages/Settings/Profile/Profile";
import Blog from "./pages/Home/Blog/Blog";
import {secondary} from "./resources/Colors";
import {ImageIcon, Text} from "./components/Elements";
import EntityMap from "./pages/Entities/Entity/EntityMap";
import {
    IconFavorite, IconFavoriteActive,
    IconHome,
    IconHomeActive,
    IconList,
    IconListActive,
    IconSearch,
    IconSearchActive,
    IconSettings, IconSettingsActive
} from "./resources/Icon";

class Routers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_login: false,
            dir: 'left',
            deepLink: null
        }
    }
    componentDidMount() {
        this.checkLogin();
        I18nManager.allowRTL(false);
        Linking.getInitialURL().then((url) => {
            if (url) {
                this.handleOpenURL(url)
            }
        });
        Linking.addEventListener('url', this.handleOpenURL.bind(this))
    }
    componentWillUnmount() {
        Linking.removeEventListener('url', this.handleOpenURL.bind(this))
    }
    handleOpenURL(url) {
        const host = 'bookizapp://fromPayment';
        url = url.substring(host.length, url.length);
        const urlParams = new URLSearchParams(url);
        const page = urlParams.get('page');
        const action = urlParams.get('action');
        this.setState({ deepLink: { page, url, action }});
        // Actions.replace('main');
    }
    async checkLogin() {
        const token = await AsyncStorage.getItem(ACCESS_TOKEN);
        if(token !== null) {
            this.props.get_cart();
            this.props.get_user();
        }
        const language = await AsyncStorage.getItem(ACCESS_LANGUAGE);
        if(language !== null) {
            const lan_data = JSON.parse(language);
            this.props.save_language(lan_data.lang, lan_data.dir);
            this.setState({ dir: lan_data.dir });
        }
        this.setState({ is_login: (token !== null) }, () => {
            setTimeout(() => {
                SplashScreen.hide();
                if(this.state.deepLink !== null) {
                    const { page , action } = this.state.deepLink;
                    if(action) Actions.push(page, { fromUrl: true, action });
                }
            }, (this.state.is_login) ? 1000 : 1);
        });
    }
    textTabStyle(focused) {
        return {
            fontSize: 12,
            width: '100%',
            textAlign: 'center',
            color: focused ? secondary : "#999"
        }
    }
    render() {
        const { is_login } = this.state;
        return (
            <Router>
                <Stack key="root" hideNavBar >
                    <Scene key="auth" initial={!is_login} hideNavBar component={Auth} />
                    <Scene key="login" hideNavBar component={Login} />
                    <Scene key="register" hideNavBar component={Register} />
                    <Scene key="forgot" hideNavBar component={Login} />
                    <Scene key="entities" hideNavBar component={Entities} />
                    <Scene key="entity" hideNavBar component={Entity} />
                    <Scene key="entityMap" hideNavBar component={EntityMap} />
                    <Scene key="services" hideNavBar component={Services} />
                    <Scene key="search" hideNavBar component={Search} />
                    <Scene key="basket" hideNavBar component={Basket} />
                    <Scene key="payment" hideNavBar component={Payment} />
                    <Scene key="booking" hideNavBar component={Booking} />
                    <Scene key="bookings" hideNavBar component={Bookings} />
                    <Scene key="settings" hideNavBar component={Settings} />
                    <Scene key="language" hideNavBar component={Language} />
                    <Scene key="profile" hideNavBar component={Profile} />
                    <Scene key="blog" hideNavBar component={Blog} />
                    {/*<Drawer*/}
                    {/*hideNavBar*/}
                    {/*key="main"*/}
                    {/*contentComponent={Sidebar}*/}
                    {/*initial={is_login}*/}
                    {/*// drawerPosition={(this.props.direction === 'rtl') ? 'right' : 'left'}*/}
                    {/*drawerPosition="left"*/}
                    {/*>*/}
                    <Tabs key="main"
                          initial={is_login}
                          tabs={true}
                          hideNavBar
                          tabBarStyle={{ backgroundColor: '#f5f5f5', borderTopColor: '#ddd' }}>
                        {/*<Scene key='tab1'*/}
                        {/*hideNavBar title='Home'*/}
                        {/*component={Home}*/}
                        {/*icon={({ focused }) => (*/}
                        {/*<Image style={{ width: 30, height: 30 }}*/}
                        {/*source={focused ? Logo : BackgroundImg}*/}
                        {/*resizeMode="contain" />*/}
                        {/*)} />*/}
                        <Scene key='home'
                               hideNavBar
                               component={Home}
                               tabBarLabel={()=>null}
                               icon={({ focused }) => <ImageIcon size={28} source={(focused) ? IconHomeActive : IconHome } />}
                                // title="HOME"
                               // tabBarLabel={({ focused }) => (
                               //     <Text style={this.textTabStyle(focused)}>HOME</Text>
                               // )}
                               // icon={({ focused }) => (
                               //     <Icon name="home" size={25} color={focused ? secondary : "#999"} />
                               // )} />
                        />
                        <Scene key='search'
                               hideNavBar
                               component={Search}
                               tabBarLabel={()=>null}
                               icon={({ focused }) => <ImageIcon size={28} source={(focused) ? IconSearchActive : IconSearch } />}
                               // title="SEARCH"
                               // tabBarLabel={({ focused }) => (
                               //     <Text style={this.textTabStyle(focused)}>SEARCH</Text>
                               // )}
                               // icon={({ focused }) => (
                               //     <Icon name="search" size={25} color={focused ? secondary : "#999"} />
                               // )} />
                        />
                        <Scene key='appointments'
                               hideNavBar
                               component={Bookings}
                               tabBarLabel={()=>null}
                               icon={({ focused }) => <ImageIcon size={28} source={(focused) ? IconListActive : IconList } />}
                                // title='ORDERS'
                               // tabBarLabel={({ focused }) => (
                               //     <Text style={this.textTabStyle(focused)}>ORDERS</Text>
                               // )}
                               // icon={({ focused }) => (
                               //     <Icon name="calendar" size={25} color={focused ? secondary : "#999"} />
                               // )}
                                />
                        <Scene key='favorites'
                               hideNavBar
                               component={Favorites}
                               tabBarLabel={()=>null}
                               icon={({ focused }) => <ImageIcon size={28} source={(focused) ? IconFavoriteActive : IconFavorite } />}
                               // title='FAVORITES'
                               // tabBarLabel={({ focused }) => (
                               //     <Text style={this.textTabStyle(focused)}>FAVORITES</Text>
                               // )}
                               // icon={({ focused }) => (
                                //     <Icon name="heart" size={25} solid color={focused ? secondary : "#999"} />
                                // )}
                        />
                        <Scene key='profile'
                               hideNavBar
                               component={Settings}
                               tabBarLabel={()=>null}
                               icon={({ focused }) => <ImageIcon size={28} source={(focused) ? IconSettingsActive : IconSettings } />}
                               // title='ACCOUNT'
                               // tabBarLabel={({ focused }) => (
                               //     <Text style={this.textTabStyle(focused)}>ACCOUNT</Text>
                               // )}
                               // icon={({ focused }) => (
                               //     <Icon name="user" size={25} color={focused ? secondary : "#999"} />
                               // )}
                        />
                    </Tabs>
                    {/*</Drawer>*/}
                </Stack>
            </Router>
        )
    }
}

const mapStateToProps = state => {
    return {}
};

export default connect(mapStateToProps, { save_language, get_cart, get_user })(Routers);
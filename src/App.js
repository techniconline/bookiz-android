import React  from 'react';
import { createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import reducers from './redux/reducers';
import Routers from "./Routers";
import Loading from "./Loading";
import Loading1 from "./Loading1";

export default () =>{
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    // return <Loading />;
    // return <Loading1 />;
    return (
        <Provider store={store}>
            <Routers />
        </Provider>
    )
};


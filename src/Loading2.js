import React, { Component } from 'react';
import { Animated, View, Easing, Modal } from 'react-native';
import {secondary} from "./resources/Colors";
import SplashScreen from "react-native-splash-screen";

class Loading extends Component{
    state = {
        pos: new Animated.Value(0),
        rotate: new Animated.Value(0),
        count: 0
    };
    componentDidMount() {
        this.downAnimation(200)
    }
    downAnimation (delay = 0) {
        this.state.pos.setValue(0)
        Animated.timing(
            this.state.pos,
            {
                toValue: 11,
                delay,
                duration: 300,
                easing: Easing.in
            }
        ).start(() => {
            this.upAnimation()
        })
    }
    upAnimation() {
        this.state.pos.setValue(11)
        Animated.timing(
            this.state.pos,
            {
                toValue: 0,
                duration: 300,
                easing: Easing.in
            }
        ).start(() => {
            this.setState({ count: this.state.count+1}, () => {
                if(this.state.count === 3) this.rotateAnimation();
                else this.downAnimation();
            })
        })
    }
    rotateAnimation() {
        this.state.rotate.setValue(0)
        this.setState({ count: 0 });
        Animated.timing(
            this.state.rotate,
            {
                toValue: 360,
                duration: 800,
                easing: Easing.linear
            }
        ).start(() => {
            this.downAnimation()
        })
    }
    renderContent() {
        return (
            <View style={styles.wrapper}>
                <View style={styles.container}>
                    <Animated.View style={{
                        ...styles.line,
                        position: 'absolute',
                        height: 150,
                        backgroundColor: 'transparent',
                        // opacity: this.state.pos,         // Bind opacity to animated value
                        transform: [{ rotateZ: this.state.rotate.interpolate({
                                inputRange: [0, 360],
                                outputRange: ['0deg', '360deg'],
                            })}]
                    }} >
                        {/*<View style={{*/}
                        {/*...styles.ball*/}
                        {/*}} />*/}
                        <Animated.View style={{
                            ...styles.ball,
                            transform: [{translateY: this.state.pos}]
                        }} />
                    </Animated.View>
                    <View style={styles.line} />
                </View>
            </View>
        )
    }
    render() {
        return (this.props.modal) ? <Modal visible={true}>{this.renderContent()}</Modal> : this.renderContent()
    }
}

const styles = {
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ball: {
        position: 'relative',
        width: 20,
        height: 20,
        borderRadius: 30,
        backgroundColor: secondary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    line: {
        width: 20,
        height: 100,
        borderRadius: 30,
        backgroundColor: secondary,
        marginTop: 10
    }
};

export default Loading;
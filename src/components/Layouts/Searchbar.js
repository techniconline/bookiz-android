import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Input} from "../Form";
import {LTR_FF_Regular, RTL_FF_Regular} from "../theme";

export const Searchbar = ({ value, onChange, placeholder, containerStyle, searchStyle, inputStyle, rtl }) => {
    const container = [styles.container, { flexDirection: (rtl) ? 'row-reverse' : 'row'}];
    const search = [styles.search, { flexDirection: (rtl) ? 'row-reverse' : 'row'}];
    const input = [styles.input, {
        fontFamily: (rtl) ? RTL_FF_Regular : LTR_FF_Regular,
        textAlign: (rtl) ? 'right' : 'left',
        direction: (rtl) ? 'rtl' : 'ltr',
        marginRight: (rtl) ? 10 : 0,
        marginLeft: (!rtl) ? 10: 0,
    }];
    if(Array.isArray(containerStyle)) container.concat(containerStyle);
    else container.push(containerStyle);

    if(Array.isArray(searchStyle)) search.concat(searchStyle);
    else search.push(searchStyle);

    if(Array.isArray(inputStyle)) input.concat(inputStyle);
    else input.push(inputStyle);

    return (
        <View style={container}>
            <View style={search}>
                <Input value={value}
                       onChange={onChange}
                       placeholder={placeholder}
                       style={input} />
                <TouchableOpacity style={styles.icon}>
                    <Icon name="search" color="#999" size={18} />
                </TouchableOpacity>
            </View>
        </View>
    )
};

Searchbar.defaultProps = {
    rtl: false,
    value: '',
    placeholder: '',
    containerStyle: {},
    searchStyle: {},
    inputStyle: {}
};
Searchbar.propTypes = {
    rtl: PropTypes.bool,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    searchStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    inputStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    onChange: PropTypes.func,
};

const styles = {
    container: {
        position: 'relative',
        width: '100%',
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.5)',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    search: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#999'
    },
    input: {
        width: '100%',
        borderWidth: 0,
        marginLeft: 5,
        textAlign: 'left'
    },
    icon: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 5
    }
};
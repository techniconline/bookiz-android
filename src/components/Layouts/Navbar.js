import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { Text } from '../Elements';
import {Primary, HeadTitleColor, RTL_FF_Medium} from "../theme";

export const Navbar = ({ style, title, centerComponent, rightComponent, leftComponent, rtl }) => {
    const navStyles = [styles.container];
    if(Array.isArray(style)) navStyles.concat(style);
    else navStyles.push(style);
    const titleStyle = [styles.title, (rtl) ? { fontFamily: RTL_FF_Medium } : null];
    return (
        <View style={navStyles}>
            {/*{(leftComponent !== null) ? <View style={styles.left}>{leftComponent}</View> : null}*/}
            <View style={styles.left}>{leftComponent}</View>
            <View style={styles.center}>
                {(centerComponent) ? centerComponent : (
                    <View style={styles.titleContainer}>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={titleStyle}>{title}</Text>
                    </View>
                )}
            </View>
            {/*{(rightComponent !== null) ? <View style={styles.right}>{rightComponent}</View> : null}*/}
            <View style={styles.right}>{rightComponent}</View>
        </View>
    )
};

Navbar.defaultProps = {
    style: {},
    title: '',
    rtl: false,
    centerComponent: null,
    rightComponent: null,
    leftComponent: null,
};
Navbar.propTypes = {
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    title: PropTypes.string,
    rtl: PropTypes.bool,
    centerComponent: PropTypes.node,
    rightComponent: PropTypes.node,
    leftComponent: PropTypes.node,
};

const styles = {
    container: {
        position: 'relative',
        width: '100%',
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Primary,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        elevation: 5
    },
    title: {
        width: '100%',
        fontSize: 18,
        textAlign: 'center',
        color: HeadTitleColor
    },
    left: {
        flex: 1,
        maxWidth: 90,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
        overflow: 'hidden',
    },
    center: {
        flex: 1,
        maxWidth: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        marginHorizontal: '2%'
    },
    right: {
        flex: 1,
        maxWidth: 90,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
        overflow: 'hidden',
    }
};
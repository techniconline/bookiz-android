import {LTR_FF_Medium, RTL_FF_Medium, RTL_FF_Regular} from "../../theme";
import {danger} from "../../../resources/Colors";

export default {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontFamily: LTR_FF_Medium,
        fontSize: 16,
        textAlign: 'left',
        direction: 'ltr',
        color: '#222'
    },
    rtl_text: {
        fontFamily: RTL_FF_Regular,
        fontSize: 15,
        textAlign: 'right',
        direction: 'rtl',
        color: '#222'
    },
    errorTxt: {
        width: '100%',
        textAlign: 'center',
        color: danger,
        marginBottom: 10
    },
    errorAlert: {
        fontSize: 14,
        width: '100%',
        textAlign: 'center',
        color: '#fff',
        backgroundColor: danger,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        borderRadius: 5,
        paddingVertical: 5,
        paddingHorizontal: 7,
        marginVertical: 3
    },
    page_loading: {
        width: '100%',
        paddingTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    }
}
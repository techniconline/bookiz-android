import React from 'react';
import PropTypes from 'prop-types';
import {Text as Txt, ActivityIndicator, View, Image} from 'react-native';
import styles from './ElementStyles';
import {primary} from "../../../resources/Colors";
import _ from 'lodash';

export const Text = ({ children, style, rtl, ...props }) => {
    let textStyle = [(rtl) ? styles.rtl_text : styles.text];
    if(Array.isArray(style)) textStyle = [...textStyle, ...style];
    else textStyle.push(style);
    return (
        <Txt {...props} style={textStyle}>{children}</Txt>
    )
};
Text.defaultProps = {
    style: {},
    rtl: false
};
Text.propTypes = {
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    rtl: PropTypes.bool
};

export const FullLoading = (color = primary) => {
    return (
        <View style={styles.container}>
            <ActivityIndicator animating color={color} size="large" />
        </View>
    )
};
export const PageLoading = () => {
    return (
        <View style={styles.page_loading}>
            <ActivityIndicator animating color={primary} size="large" />
        </View>
    )
};
export const ErrorPage = ({errors, rtl}) => {
    return (
        <View style={styles.container}>
            {_.map(errors, (error, key) => (
                <Text rtl={rtl} key={key} style={styles.errorTxt}>{error[0]}</Text>
            ))}
        </View>
    )
};
export const ErrorAlert = ({errors, rtl}) => {
    return (
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
            {_.map(errors, (error, key) => (
                <Text rtl={rtl} key={key} style={styles.errorAlert}>{error[0]}</Text>
            ))}
        </View>
    )
};


Text.defaultProps = {
    errors: [],
    rtl: false
};
Text.propTypes = {
    errors: PropTypes.array,
    rtl: PropTypes.bool
};

export const ImageIcon = ({source, size, style}) => {
    if(source === null) return null;
    const styles = {
        width: size,
        height: size,
        ...style
    };
    return (
        <Image source={source} style={styles}  />
    );
};
ImageIcon.defaultProps = {
    source: null,
    size: 15,
    style: {}
};
ImageIcon.propTypes = {
    source: PropTypes.oneOfType([PropTypes.string, PropTypes.uri]),
    size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
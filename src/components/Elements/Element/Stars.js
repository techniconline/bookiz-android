import React from 'react';
import PropTypes from 'prop-types';
import Icon from "react-native-vector-icons/FontAwesome5";
import {Gray, Warning} from "../../theme";
import {View} from "react-native";

export const Stars = ({ value, rtl, size, disableColor, enableColor }) => {
    value = (!isNaN(Number(value))) ? Number(value) : 0;
    return (
        <View style={rtl ? styles.rtl_stars : styles.stars}>
            <Icon name="star" size={size} solid color={value > 0.5 ? enableColor : disableColor} />
            <Icon name="star" size={size} solid color={value > 1.5 ? enableColor : disableColor} />
            <Icon name="star" size={size} solid color={value > 2.5 ? enableColor : disableColor} />
            <Icon name="star" size={size} solid color={value > 3.5 ? enableColor : disableColor} />
            <Icon name="star" size={size} solid color={value > 4.5 ? enableColor : disableColor} />
        </View>
    )
};

Stars.defaultProps = {
    disableColor: Gray,
    enableColor: Warning,
    value: 0,
    size: 12,
    rtl: false
};
Stars.propTypes = {
    value:  PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    rtl: PropTypes.bool,
    disableColor: PropTypes.string,
    enableColor: PropTypes.string,
    size: PropTypes.number,
};

const styles = {
    stars: {
        flexDirection: 'row',
    },
    rtl_stars: {
        flexDirection: 'row-reverse',
    },
};
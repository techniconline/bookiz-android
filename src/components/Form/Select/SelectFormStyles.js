import {LTR_FF_Medium} from "../../theme";

export default {
    label: {
        fontFamily: LTR_FF_Medium,
        fontSize: 16,
        color: '#666',
        marginBottom: 5
    }
};
import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './SelectFormStyles';
import {Select} from "./Select";

export const SelectForm = ({ value, mode, label, options, onChange, labelStyle, selectStyle, containerStyle, prompt, bordered, leftIconSize, leftIcon, leftIconColor, leftIconStyle, rightIconSize, rightIcon, rightIconColor, rightIconStyle, ...Props }) => {
    const labelStyles = [styles.label];
    if(Array.isArray(labelStyle)) labelStyles.concat(labelStyle);
    else labelStyles.push(labelStyle);
    return (
        <View style={{ marginBottom: 15 }}>
            <Text style={labelStyles}>{label}</Text>
            <Select
                {...Props}
                style={selectStyle}
                containerStyle={containerStyle}
                onChange={onChange}
                value={value}
                mode={mode}
                prompt={prompt}
                bordered={bordered}
                options={options}
                leftIconSize={leftIconSize}
                leftIcon={leftIcon}
                leftIconColor={leftIconColor}
                leftIconStyle={leftIconStyle}
                rightIconSize={rightIconSize}
                rightIcon={rightIcon}
                rightIconColor={rightIconColor}
                rightIconStyle={rightIconStyle}
            />
        </View>
    )
};

SelectForm.defaultProps = {
    label: '',
    value: '',
    prompt: '',
    options: [],
    labelStyle: {},
    selectStyle: {},
    containerStyle: {},
    mode: 'dialog',
    bordered: false,
    leftIcon: '',
    leftIconSize: 30,
    leftIconColor: '#666',
    leftIconStyle: {},
    rightIcon: '',
    rightIconSize: 30,
    rightIconColor: '#666',
    rightIconStyle: {},
};

SelectForm.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    prompt: PropTypes.string,
    options: PropTypes.array.isRequired,
    labelStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    selectStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    containerStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    dialog: PropTypes.string,
    bordered: PropTypes.bool,
    leftIcon: PropTypes.string,
    leftIconSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    leftIconColor: PropTypes.string,
    leftIconStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    rightIcon: PropTypes.string,
    rightIconSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    rightIconColor: PropTypes.string,
    rightIconStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    onChange: PropTypes.func
};
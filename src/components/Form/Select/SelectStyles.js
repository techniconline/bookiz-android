import {LTR_FF_Medium, direction, textAlign} from "../../theme";

export default {
    container: {
        width: '100%',
        position: 'relative',
        flexDirection: 'row',
        paddingHorizontal: 10,
        borderColor: '#666',
        borderBottomWidth: 2,
        overflow: 'hidden'
    },
    picker: {
        width: '100%',
        maxHeight: 40,
        color: '#333',
        direction
    },
    item: {
        fontFamily: LTR_FF_Medium,
        fontSize: 16,
        direction,
        textAlign
    },
    bordered: {
        borderWidth: 1,
        borderBottomWidth: 1,
        borderRadius: 3,
        borderColor: 'rgba(0,0,0,0.3)',
        overflow: 'hidden'
    },
    leftIconStyle: {
        position: 'absolute',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        left: 0,
        padding: 5
    },
    rightIconStyle: {
        position: 'absolute',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        right: 0,
        padding: 5
    }
};
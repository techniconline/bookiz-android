import React from 'react';
import { View, Picker } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styles from './SelectStyles';


export const Select = ({ value, mode, options, onChange, style, prompt, containerStyle, bordered, leftIconSize, leftIcon, leftIconColor, leftIconStyle, rightIconSize, rightIcon, rightIconColor, rightIconStyle, ...Props }) => {
    const containerStyles = [styles.container];
    const PickerStyles = [styles.picker];
    const itemStyles = [styles.item];
    const leftIconStyles = [styles.leftIconStyle];
    const rightIconStyles = [styles.rightIconStyle];

    const hasLeftIcon = leftIcon.trim() !== '';
    const hasRightIcon = rightIcon.trim() !== '';

    if(Array.isArray(style)) PickerStyles.concat(style);
    else PickerStyles.push(style);

    if(Array.isArray(style)) containerStyles.concat(containerStyle);
    else containerStyles.push(containerStyle);

    if(hasLeftIcon) {
        containerStyles.push({ paddingLeft: leftIconSize + 7 });
        if(Array.isArray(leftIconStyle)) leftIconStyles.concat(leftIconStyle);
        else leftIconStyles.push(leftIconStyle);
    }
    if(hasRightIcon) {
        containerStyles.push({ paddingRight: rightIconSize + 7 });
        if(Array.isArray(rightIconStyle)) rightIconStyles.concat(rightIconStyle);
        else rightIconStyles.push(rightIconStyle);
    }

    if(bordered) containerStyles.push(styles.bordered);
    return (
        <View style={containerStyles}>
            {(hasRightIcon) ? (<View style={rightIconStyles}><Icon name={rightIcon} size={rightIconSize} color={rightIconColor} /></View>) :null}
            <Picker
                {...Props}
                prompt={prompt}
                selectedValue={value}
                mode={mode}
                itemStyle={itemStyles}
                style={PickerStyles}
                onValueChange={onChange}>
                {_.map(options, (opt, key) => <Picker.Item key={key} label={opt.label} value={opt.value} /> )}
            </Picker>
            {(hasLeftIcon) ? (<View style={leftIconStyles}><Icon name={leftIcon} size={leftIconSize} color={leftIconColor} /></View>) :null}
        </View>
    )
};

Select.defaultProps = {
    value: '',
    prompt: '',
    options: [],
    style: {},
    containerStyle: {},
    mode: 'dialog',
    bordered: false,
    leftIcon: '',
    leftIconSize: 30,
    leftIconColor: '#666',
    leftIconStyle: {},
    rightIcon: '',
    rightIconSize: 30,
    rightIconColor: '#666',
    rightIconStyle: {},
};

Select.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    prompt: PropTypes.string,
    options: PropTypes.array.isRequired,
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    containerStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    dialog: PropTypes.string,
    bordered: PropTypes.bool,
    leftIcon: PropTypes.string,
    leftIconSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    leftIconColor: PropTypes.string,
    leftIconStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    rightIcon: PropTypes.string,
    rightIconSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    rightIconColor: PropTypes.string,
    rightIconStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    onChange: PropTypes.func
};
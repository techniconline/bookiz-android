import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Text } from '../../Elements';
import PropTypes from 'prop-types';
import styles from './ButtonStyles.js';
import { Primary } from "../../theme";

export const Button = ({ children, onPress, title, style, textStyle, backgroundColor, titleColor, rtl }) => {
    const btnStyle = [styles.button, {backgroundColor}];
    const btnTxtStyle = [rtl ? styles.rtl_title : styles.title, { color: titleColor }];

    if(Array.isArray(style)) btnStyle.concat(style);
    else btnStyle.push(style);

    if(Array.isArray(textStyle)) btnTxtStyle.concat(textStyle);
    else btnTxtStyle.push(textStyle);

    return (
        <TouchableOpacity activeOpacity={0.7} style={btnStyle} onPress={onPress}>
            {(title !== '') ? (
                <Text rtl={rtl} style={btnTxtStyle}>{title}</Text>
            ) : children}
        </TouchableOpacity>
    )
};

Button.defaultProps = {
    backgroundColor: Primary,
    titleColor: '#fff',
    title: '',
    rtl: false,
    style: {},
    textStyle: {},
};

Button.propTypes = {
    title: PropTypes.string,
    backgroundColor: PropTypes.string,
    titleColor: PropTypes.string,
    rtl: PropTypes.bool,
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    textStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
};
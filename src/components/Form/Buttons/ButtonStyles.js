import {LTR_FF_Medium, RTL_FF_Medium} from "../../theme";

export default {
    button: {
        width: '100%',
        padding: 10,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
    },
    title: {
        fontFamily: LTR_FF_Medium,
        fontSize: 16,
        textAlign: 'center'
    },
    rtl_title: {
        fontFamily: RTL_FF_Medium,
        fontSize: 16,
        textAlign: 'center'
    }
}
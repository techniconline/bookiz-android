import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './InputFormStyles';
import {Input} from "./Input";

export const InputForm = ({ value, label, placeholder, onChange, inputStyle, labelStyle, type, secureTextEntry, ...Props }) => {
    const labelStyles = [styles.label];
    if(Array.isArray(labelStyle)) labelStyles.concat(labelStyle);
    else labelStyles.push(labelStyle);

    return (
        <View style={{ marginBottom: 15 }}>
            <Text style={labelStyles}>{label}</Text>
            <Input
                {...Props}
                style={inputStyle}
                onChange={onChange}
                value={value}
                type={type}
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
            />
        </View>
    )
};
const keyboardTypes = [
    'default', 'number-pad', 'decimal-pad', 'numeric', 'email-address', 'phone-pad',
    'ascii-capable', 'numbers-phone-pad', 'twitter', 'web-search' // ========= IOS ONLY =========== //
];

InputForm.defaultProps = {
    value: '',
    type: 'default',
    label: '',
    inputStyle: {},
    placeholder: '',
    secureTextEntry: false
};

InputForm.propTypes = {
    value: PropTypes.string.isRequired,
    type: PropTypes.oneOf(keyboardTypes),
    label: PropTypes.string.isRequired,
    inputStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    placeholder: PropTypes.string,
    secureTextEntry: PropTypes.bool,
    onChange: PropTypes.func
};
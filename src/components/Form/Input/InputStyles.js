import {LTR_FF_Medium, direction, textAlign} from "../../theme";

export default {
    container: {
        width: '100%',
        flexDirection: 'row',
        position: 'relative',
        alignItems: 'center'
    },
    input: {
        fontFamily: LTR_FF_Medium,
        fontSize: 16,
        width: '100%',
        color: '#333',
        direction,
        textAlign,
        paddingHorizontal: 10,
        paddingVertical: 5,
        lineHeight: 20,
        overflow: 'hidden'
    },
    bordered: {
        borderWidth: 1,
        borderBottomWidth: 1,
        borderRadius: 3,
        borderColor: 'rgba(0,0,0,0.3)',
        overflow: 'hidden'
    },
    leftIconStyle: {
        position: 'absolute',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        left: 0,
        padding: 5
    },
    rightIconStyle: {
        position: 'absolute',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        right: 0,
        padding: 5
    }
};
import React from 'react';
import { View, TextInput, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';
import styles from './InputStyles';
import {LTR_FF_Regular, RTL_FF_Regular} from "../../theme";


export const Input = ({ value, rtl, refs, placeholder, onChange, style, containerStyle, type, secureTextEntry, bordered, leftIconSize, leftIcon, leftIconColor, leftIconStyle, rightIconSize, rightIcon, rightIconColor, rightIconStyle, rightLoading, rightLoadingColor, rightLoadingSize, leftLoading, leftLoadingColor, leftLoadingSize, ...Props }) => {
    let containerStyles = [styles.container];
    let InputStyle = [styles.input, {
        fontFamily: (rtl) ? RTL_FF_Regular : LTR_FF_Regular,
        textAlign: (rtl) ? 'right' : 'left',
        direction: (rtl) ? 'rtl' : 'ltr',
        marginRight: (rtl) ? 10 : 0,
        marginLeft: (!rtl) ? 10: 0,
    }];
    const leftIconStyles = [styles.leftIconStyle];
    const rightIconStyles = [styles.rightIconStyle];

    const hasLeftIcon = leftIcon.trim() !== '';
    const hasRightIcon = rightIcon.trim() !== '';

    if(Array.isArray(style)) InputStyle = [...InputStyle, ...style];
    else InputStyle.push(style);

    if(Array.isArray(containerStyle)) containerStyles.concat(containerStyle);
    else containerStyles.push(containerStyle);

    if(hasLeftIcon) {
        InputStyle.push({ paddingLeft: leftIconSize + 10 });
        if(Array.isArray(leftIconStyle)) leftIconStyles.concat(leftIconStyle);
        else leftIconStyles.push(leftIconStyle);
    }
    if(hasRightIcon) {
        InputStyle.push({ paddingRight: rightIconSize + 10 });
        if(Array.isArray(rightIconStyle)) rightIconStyles.concat(rightIconStyle);
        else rightIconStyles.push(rightIconStyle);
    }
    if(bordered) InputStyle.push(styles.bordered);
    return (
        <View style={containerStyles}>
            {(rightLoading) ? <View style={styles.rightIconStyle}>
                                <ActivityIndicator size={rightLoadingSize} color={rightLoadingColor} />
                            </View>
                            : (hasRightIcon) ? (
                                <View style={rightIconStyles}>
                                    <Icon name={rightIcon} size={rightIconSize} color={rightIconColor} />
                                </View>
                                ) :null}
            <TextInput
                {...Props}
                style={InputStyle}
                onChangeText={onChange}
                value={value}
                keyboardType={type}
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                ref={rf => { if(refs) { refs(rf) } }}
            />
            {(leftLoading) ? <View style={styles.leftIconStyle}>
                                <ActivityIndicator size={leftLoadingSize} color={leftLoadingColor} />
                            </View>
                            : (hasLeftIcon) ? (
                                <View style={leftIconStyles}>
                                    <Icon name={leftIcon} size={leftIconSize} color={leftIconColor} />
                                </View>) :null}
        </View>
    )
};
const keyboardTypes = [
    'default', 'number-pad', 'decimal-pad', 'numeric', 'email-address', 'phone-pad',
    'ascii-capable', 'numbers-phone-pad', 'twitter', 'web-search' // ========= IOS ONLY =========== //
];

Input.defaultProps = {
    value: '',
    rtl: false,
    type: 'default',
    style: {},
    containerStyle: {},
    placeholder: '',
    secureTextEntry: false,
    bordered: false,
    leftIcon: '',
    leftIconSize: 20,
    leftIconColor: '#666',
    leftIconStyle: {},
    rightIcon: '',
    rightIconSize: 20,
    rightIconColor: '#666',
    rightIconStyle: {},
    rightLoading: false,
    rightLoadingSize: 22,
    rightLoadingColor: '#666',
    leftLoading: false,
    leftLoadingSize: 22,
    leftLoadingColor: '#666'
};

Input.propTypes = {
    value: PropTypes.string.isRequired,
    rtl: PropTypes.bool,
    type: PropTypes.oneOf(keyboardTypes),
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    containerStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    placeholder: PropTypes.string,
    secureTextEntry: PropTypes.bool,
    bordered: PropTypes.bool,
    leftIcon: PropTypes.string,
    leftIconSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    leftIconColor: PropTypes.string,
    leftIconStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    rightIcon: PropTypes.string,
    rightIconSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    rightIconColor: PropTypes.string,
    rightIconStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    rightLoading: PropTypes.bool,
    rightLoadingSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    rightLoadingColor: PropTypes.string,
    leftLoading: PropTypes.bool,
    leftLoadingSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    leftLoadingColor: PropTypes.string,
    onChange: PropTypes.func
};
import {Font_Bold, Font_Medium, Font_Regular, Rtl_Font_Bold, Rtl_Font_Medium, Rtl_Font_Regular} from "../redux/types";
import {blue, danger, gray, success, warning, primary, secondary} from "../resources/Colors";

/* ====== Fonts ====== */
export const LTR_FF_Regular = Font_Regular;
export const LTR_FF_Medium = Font_Medium;
export const LTR_FF_Bold = Font_Bold;
export const RTL_FF_Regular = Rtl_Font_Regular;
export const RTL_FF_Medium = Rtl_Font_Medium;
export const RTL_FF_Bold = Rtl_Font_Bold;

/* ====== Colors ====== */
export const Primary = primary;
export const Blue = blue;
export const Gray = gray;
export const Success = success;
export const Danger = danger;
export const Warning = warning;
export const HeadTitleColor = secondary;

export const textAlign = 'left';
export const direction = 'ltr';

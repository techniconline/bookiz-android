import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
export Tabs from './Tabs';

export const Tab = ({ children, heading}) => (
    <View style={{ flex: 1 }} heading={heading}>
        {children}
    </View>
);

Tab.propTypes = {
    heading: PropTypes.oneOfType([PropTypes.node, PropTypes.string])
};
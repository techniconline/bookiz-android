import React, { Component } from 'react';
import {TouchableOpacity, View, ViewPagerAndroid} from "react-native";
import _ from 'lodash';
import {Text} from "../Elements";

class Tabs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 0
        }
    }
    onRef(viewPager) {
        if(this.props.ref)this.props.ref(viewPager);
        this.viewPager = viewPager;
    }
    render() {
        const { position } = this.props;
        const children = (Array.isArray(this.props.children)) ? this.props.children : [this.props.children];
        return (
            <View style={(position === 'top')?{flexDirection: 'column-reverse'}:styles.container}>
                <ViewPagerAndroid style={styles.viewPager} ref={this.onRef.bind(this)} initialPage={this.state.page}>
                    {_.map(children, (child, key) => (
                        <View style={styles.pageStyle} key={key}>
                            {child.props.children}
                        </View>
                    ))}
                </ViewPagerAndroid>
                <View style={[styles.tabBar, (position === 'top')?styles.tabBarTop:null]}>
                    {_.map(children, (child, key) => (
                        <TouchableOpacity
                            key={key}
                            activeOpacity={0.8}
                            style={styles.tab}
                            onPress={()=>{ this.viewPager.setPage(key); }}>
                            {(typeof child.props.heading === 'string') ? (
                                <Text style={styles.tabText}>{child.props.heading}</Text>
                            ) : child.props.heading}
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
        )
    }
}


const styles = {
    container: {
        flex: 1
    },
    viewPager: {
        flex: 1,
        backgroundColor: '#ddd'
    },
    pageStyle: {
        alignItems: 'center',
        padding: 20,
    },
    tabBar: {
        flexDirection: 'row',
        width: '100%',
        height: 50,
        borderTopWidth: 1,
        borderTopColor: '#000',
        elevation: 5
    },
    tabBarTop: {
        flexDirection: 'row',
        width: '100%',
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: '#000',
        elevation: 5
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabText: {
        width: '100%',
        textAlign: 'center'
    }
};


export default Tabs;
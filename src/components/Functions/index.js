export * from './Date';
export * from './Errors';
export const gender = (type) => {
    return (type === 1) ? 'Female' : 'Male';
};
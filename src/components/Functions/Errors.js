export const http_errors = (errors, response: false) => {
    let result = [[]];
    if(response) {
        result = [[(errors.data.error) ? errors.data.error : (errors.data.message) ? errors.data.message : 'UNKNOWN ERROR']];
    }else {
        console.log('errors request', errors.request);
        console.log('errors config', errors.config);

        result = (errors.response) ? errors.response.data.errors : (errors.request) ? [[errors.request._response]] : null;
    }

    return result
};


export default {
    sign_in: {
        en: 'Sign in',
        fa: 'ورود'
    },
    sign_up: {
        en: 'Sign up',
        fa: 'ثبت نام'
    },
    empty_date: {
        en: 'Not Found Data.',
        fa: 'داده ای یافت نشد.'
    },
    cancel: {
        en: 'Cancel',
        fa: 'بستن'
    },
    yes: {
        en: 'Yes',
        fa: 'بله'
    },
    no: {
        en: 'No',
        fa: 'نه'
    },
    select: {
        en: 'Select',
        fa: 'انتخاب'
    },
    home: {
        en: 'home',
        fa: 'خانه'
    },
    favorite: {
        en: 'Favorite',
        fa: 'علاقه‌مندی'
    },
    orders: {
        en: 'Orders',
        fa: 'سفارشات'
    },
    order: {
        en: 'Order',
        fa: 'سفارش'
    },
    favorites: {
        en: 'Favorites',
        fa: 'علاقه‌مندی ها'
    },

    blog: {
        en: 'Blog',
        fa: 'پست'
    },
    settings: {
        en: 'settings',
        fa: 'تنظیمات'
    },
    account: {
        en: 'Account',
        fa: 'حساب کاربری'
    },

    language: {
        en: 'Language',
        fa: 'زبان'
    },
    logout: {
        en: 'log out',
        fa: 'خروج از حساب کاربری'
    },
    search: {
        en: 'search',
        fa: 'جستجو'
    },
    results: {
        en: 'Results',
        fa: 'نتایج'
    },
    total_results: {
        en: 'Total Results',
        fa: 'تعداد نتیجه'
    },
    search_placeholder: {
        en: 'Search Salon OR Service',
        fa: 'جستجو سالن یا سرویس'
    },
    choose_location: {
        en: 'Choose your location',
        fa: 'محل خود را انتخاب کنید'
    },
    current_location: {
        en: 'Current location',
        fa: 'موقعیت کنونی'
    },
    select_services: {
        en: 'Select Services',
        fa: 'انتخاب سرویس'
    },
    services: {
        en: 'Services',
        fa: 'سرویس ها'
    },
    service: {
        en: 'Service',
        fa: 'سرویس'
    },
    added: {
        en: 'Added',
        fa: 'اضافه شد'
    },
    entity: {
        en: 'Entity',
        fa: 'سالن'
    },
    go_basket: {
        en: 'Go Basket',
        fa: 'سبد خرید'
    },
    basket: {
        en: 'Basket',
        fa: 'سبد خرید'
    },
    payment: {
        en: 'Payment',
        fa: 'پرداخت'
    },
    payment_type: {
        en: 'Payment Type',
        fa: 'نوع پرداخت'
    },
    staff: {
        en: 'Staff',
        fa: 'متخصص'
    },
    info: {
        en: 'Info',
        fa: 'اطلاعات'
    },
    map: {
        en: 'Map',
        fa: 'نقشه'
    },
    show_map: {
        en: 'Show Map',
        fa: 'نمایش نقشه'
    },
    comments: {
        en: 'Comments',
        fa: 'نظرات'
    },
    rate: {
        en: 'Rate',
        fa: 'امتیاز'
    },
    time: {
        en: 'Time',
        fa: 'زمان'
    },
    times: {
        en: 'Times',
        fa: 'زمانبندی'
    },
    description: {
        en: 'Description',
        fa: 'توضیحات'
    },
    address: {
        en: 'Address',
        fa: 'آدرس'
    },
    social_media: {
        en: 'Social Media',
        fa: 'شبکه اجتماعی'
    },
    call_entity: {
        en: 'Call Entity',
        fa: 'تماس با سالن'
    },
    price_quality: {
        en: 'Price / Quality',
        fa: 'هزینه / کیفیت'
    },
    support: {
        en: 'Support',
        fa: 'پشتیبانی'
    },
    place: {
        en: 'Place',
        fa: 'محیط'
    },
    comments_recently: {
        en: 'Comments Recently',
        fa: 'نظرات اخیر'
    },
    select_date: {
        en: 'Select Date',
        fa: 'انتخاب روز'
    },
    select_time: {
        en: 'Select Time',
        fa: 'انتخاب زمان'
    },
    select_staff: {
        en: 'Select Staff',
        fa: 'انتخاب متخصص'
    },
    anyone: {
        en: 'Anyone',
        fa: 'هر متخصصی'
    },
    delete_cart: {
        en: 'Delete from Cart',
        fa: 'حذف از سبد'
    },
    confirm_delete_service_cart: {
        en: 'Are you sure to delete this service from cart?',
        fa: 'آیا از حذف این سرویس مطمئن هستید؟'
    },
    disable_cart_service: {
        en: 'This service disable because has not timesheet',
        fa: 'این سرویس به علت نداشتن زمانبندی غیرفعال است.'
    },
    submit: {
        en: 'Submit',
        fa: 'ارسال'
    },
    save: {
        en: 'Save',
        fa: 'ذخیره'
    },
    complete_services: {
        en: 'Please Complete Services detail by click on service',
        fa: 'لطفا اطلاعات سرویس ها را با کلیک بروی آنها تکمیل کنید'
    },
    select_payment: {
        en: 'Please Select Payment type',
        fa: 'لطفا درگاه پرداخت را انتخاب نمایید.'
    },
    // Profile
    profile: {
        en: 'profile',
        fa: 'پروفایل'
    },
    first_name: {
        en: 'First Name',
        fa: 'نام'
    },
    last_name: {
        en: 'Last Name',
        fa: 'نام خانوادگی'
    },
    gender: {
        en: 'Gender',
        fa: 'جنسیت'
    },
    select_gender: {
        en: 'Select Gender',
        fa: 'انتخاب جنسیت'
    },
    female: {
        en: 'Female',
        fa: 'زن'
    },
    male: {
        en: 'Male',
        fa: 'مرد'
    },
    marital: {
        en: 'Marital',
        fa: 'تاهل'
    },
    select_marital: {
        en: 'Select Marital',
        fa: 'انتخاب تاهل'
    },
    single: {
        en: 'Single',
        fa: 'مجرد'
    },
    married: {
        en: 'Married',
        fa: 'متاهل'
    },
    country: {
        en: 'Country',
        fa: 'کشور'
    },
    select_country: {
        en: 'Select Country',
        fa: 'انتخاب کشور'
    },
    state: {
        en: 'State',
        fa: 'ایالت / استان'
    },
    select_state: {
        en: 'Select State',
        fa: 'انتخاب ایالت / استان'
    },
    city: {
        en: 'City',
        fa: 'شهر'
    },
    select_city: {
        en: 'Select City',
        fa: 'انتخاب شهر'
    },

}
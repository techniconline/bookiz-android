export const BackgroundImg = require('./Images/background.jpg');
export const SidebarImage = require('./Images/sidebar.jpg');
export const Avatar = require('./Images/avatar.jpeg');
export const Logo = require('./Images/logo.png');
// export const AuthImage = require('./Images/auth.jpg');
// export const LoginImage = require('./Images/login.jpg');
export const AuthImage = require('./Images/auth.png');
export const LoginImage = require('./Images/login.png');
export const Profile_Background_Image = require('./Images/profile.jpg');

/* ========== ICONS ========== */
export const Icon_Face = require('./Images/icons/face.png');
export const Icon_Body = require('./Images/icons/body.png');
export const Icon_Nail = require('./Images/icons/nail.png');
export const Icon_Spa = require('./Images/icons/spa.png');
export const Icon_Barber = require('./Images/icons/barber.png');
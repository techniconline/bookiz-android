import React, { Component } from 'react';
import { Animated, View, Easing } from 'react-native';
import {secondary} from "./resources/Colors";
import SplashScreen from "react-native-splash-screen";

class Loading extends Component{
    state = {
        pos: new Animated.Value(0),
        rotate: new Animated.Value(0)
    };
    componentDidMount() {
        SplashScreen.hide();
        this.downAnimation(200)
    }
    downAnimation (delay = 0) {
        this.state.pos.setValue(0)
        Animated.timing(
            this.state.pos,
            {
                toValue: 11,
                delay,
                duration: 300,
                easing: Easing.in
            }
        ).start(() => {
            this.upAnimation()
        })
    }
    upAnimation() {
        this.state.pos.setValue(11)
        Animated.timing(
            this.state.pos,
            {
                toValue: 0,
                duration: 300,
                easing: Easing.in
            }
        ).start(() => {
            this.downAnimation()
            // this.rotateAnimation()
        })
    }
    rotateAnimation() {
        this.state.rotate.setValue(0)
        Animated.timing(
            this.state.rotate,
            {
                toValue: 0,
                duration: 800,
                easing: Easing.linear
            }
        ).start(() => {
            this.downAnimation()
        })
    }
    render() {
        return (
            <View style={styles.wrapper}>
                <View style={styles.container}>
                    <View style={{
                        ...styles.line,
                        position: 'absolute',
                        backgroundColor: 'transparent',
                        // opacity: this.state.pos,         // Bind opacity to animated value
                        // transform: [{ rotateX: this.state.rotate + 'deg'}]
                        }} >
                        <Animated.View style={{
                            ...styles.ball,
                            transform: [{translateY: this.state.pos.interpolate({
                                    inputRange: [0, 11],
                                    outputRange: [8, 0]
                                })}]
                        }} />
                    </View>
                    <View style={{
                        ...styles.line,
                        position: 'absolute',
                        height: 160,
                        backgroundColor: 'transparent',
                        // opacity: this.state.pos,         // Bind opacity to animated value
                        // transform: [{ rotateX: this.state.rotate + 'deg'}]
                        }} >
                        <Animated.View style={{
                            ...styles.ball,
                            transform: [{translateY: this.state.pos}]
                        }} />
                    </View>
                    <View style={styles.line} />
                </View>
            </View>
        )
    }
}

const styles = {
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ball: {
        position: 'relative',
        width: 20,
        height: 20,
        borderRadius: 30,
        backgroundColor: secondary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    line: {
        width: 20,
        height: 100,
        borderRadius: 30,
        backgroundColor: secondary,
        marginTop: 10
    }
};

export default Loading;
import { AsyncStorage } from 'react-native';
import {x_get, get, axios} from "./http";
import {
    ACCESS_LANGUAGE,
    GET_CITIES,
    GET_CITIES_FAILED,
    GET_CITIES_SUCCESS,
    GET_COUNTRIES,
    GET_COUNTRIES_FAILED,
    GET_COUNTRIES_SUCCESS,
    GET_HOME,
    GET_HOME_FAILED,
    GET_HOME_SUCCESS,
    GET_LOCATION,
    GET_LOCATION_FAILED,
    GET_LOCATION_SUCCESS,
    GET_STATES,
    GET_STATES_FAILED,
    GET_STATES_SUCCESS,
    MAPBOX_API,
    SAVE_LANGUAGE
} from '../types';

export const get_home = () => {
    return dispatch => {
        dispatch({ type: GET_HOME });
        x_get('').then(result => {
            console.log(result);
            if(result.data.action) {
                dispatch({ type: GET_HOME_SUCCESS, payload: result.data.widgets });
            }else {
                dispatch({ type: GET_HOME_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]] });
            }
        }).catch(errors => {
            dispatch({ type: GET_HOME_FAILED, payload: errors.response });
        });
    }
};

export const save_language = (lang = 'en', dir = 'ltr') => {
    return async dispatch => {
        await AsyncStorage.setItem(ACCESS_LANGUAGE, JSON.stringify({lang, dir}));
        dispatch({ type: SAVE_LANGUAGE, payload: {lang, dir}});
    };
};

export const get_countries = () => {
    return dispatch => {
        dispatch({ type: GET_COUNTRIES });
        get('core/location/json').then(result => {
            if(result.data.action) {
                dispatch({ type: GET_COUNTRIES_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_COUNTRIES_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]] });
            }
        }).catch(errors => {
            dispatch({ type: GET_COUNTRIES_FAILED, payload: errors.response });
        });
    }
};
export const get_states = (country_id = "null") => {
    return dispatch => {
        dispatch({ type: GET_STATES });
        if(country_id !== "null") {
            get('core/location/json', { country_id }).then(result => {
                if(result.data.action) {
                    dispatch({ type: GET_STATES_SUCCESS, payload: result.data.data });
                }else {
                    dispatch({ type: GET_STATES_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]] });
                }
            }).catch(errors => {
                dispatch({ type: GET_STATES_FAILED, payload: errors.response });
            });
        }
    }
};
export const get_cities = (country_id = "null", state_id = "null") => {
    return dispatch => {
        dispatch({ type: GET_CITIES });
        if(country_id !== "null" && state_id !== "null") {
            get('core/location/json', { country_id, state_id }).then(result => {
                if(result.data.action) {
                    dispatch({ type: GET_CITIES_SUCCESS, payload: result.data.data });
                }else {
                    dispatch({ type: GET_CITIES_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]] });
                }
            }).catch(errors => {
                dispatch({ type: GET_CITIES_FAILED, payload: errors.response });
            });
        }
    }
};



export const get_location = (text = '', long = null, lat = null) => {
    return dispatch => {
        dispatch({ type: GET_LOCATION });
        let url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'; // Full FREE
        // let url = 'https://api.mapbox.com/geocoding/v5/mapbox.places-permanent/'; // Pricing, But 2500 monthly free
        url += (long !== null && lat !== null) ? `${long},${lat}.json` : `${text}.json`;
        url += `?types=place,poi,address&access_token=${MAPBOX_API}`;
        console.log(url)
        axios.get(url).then(result => {
            dispatch({ type: GET_LOCATION_SUCCESS, payload: result.data });
            console.log(result);
        }).catch(errors => {
            dispatch({ type: GET_LOCATION_FAILED, payload: [['Problem in map']] });
            console.log('errors', errors.response);
        });
    }
};
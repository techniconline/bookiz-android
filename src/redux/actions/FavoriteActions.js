import {post, wstoken, get} from './http';
import {
    GET_FAVORITES,
    GET_FAVORITES_FAILED,
    GET_FAVORITES_SUCCESS,
    ON_LIKED,
    ON_LIKED_FAILED,
    ON_LIKED_SUCCESS
} from "../types";

export const get_favorites = (next_page_url = null, refresh = false) => {
    return dispatch => {
        dispatch({ type: GET_FAVORITES, page_loading: (next_page_url !== null), refresh });
        get('feedback/like/list/get', { like: 1, model_type: 'entity' } , next_page_url).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_FAVORITES_SUCCESS, payload: result.data.data.data, response: result.data.data });
            }else {
                dispatch({ type: GET_FAVORITES_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]] });
            }
        }).catch(errors => {
            dispatch({ type: GET_FAVORITES_FAILED, payload: errors.response });
        });
    }
};


export const on_like_entity = (entity_id, liked = true) => {
    return dispatch => {
        dispatch({type: ON_LIKED});
        const url = `feedback/like/entity/${entity_id}/${(liked) ? 1 : 0}/save?wstoken=${wstoken}`;
        console.log('url', url)
        post(url).then(result => {
            console.log('result', result);
            dispatch({type: ON_LIKED_SUCCESS});
        }).catch(errors => {
            console.log('errors.response', errors.response);
            dispatch({type: ON_LIKED_FAILED, payload: errors.response });
        });
    }
};
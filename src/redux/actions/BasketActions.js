import {
    ACCESS_CART, ACCESS_TOKEN,
    ADD_CART, ADD_CART_FAILED,
    ADD_CART_SUCCESS,
    CLEAN_BASKET, GET_CART, GET_CART_FAILED, GET_CART_SUCCESS,
    REMOVE_CART, REMOVE_CART_FAILED,
    REMOVE_CART_SUCCESS
} from "../types";
import {API_URL, get, post, wstoken} from './http';
import axios from 'axios';
import {ToastAndroid, NetInfo, AsyncStorage, Linking} from 'react-native';


export const get_cart = () => {
    return dispatch => {
        dispatch({ type: GET_CART });
        NetInfo.isConnected.fetch().done(async connected => {
            let data = await AsyncStorage.getItem(ACCESS_CART);
            if(data !== null) dispatch({ type: GET_CART_SUCCESS, payload: JSON.parse(data) });
            if (connected) call_cart(dispatch);
        });
    }
};

export const call_cart = (dispatch) => {
    get('sale/cart/booking').then(async result => {
        console.log('call_cart result', result);
        if(result.data.action) {
            await AsyncStorage.setItem(ACCESS_CART, JSON.stringify(result.data.data));
            dispatch({ type: GET_CART_SUCCESS, payload: result.data.data});
        }else {
            await AsyncStorage.removeItem(ACCESS_CART);
            dispatch({ type: GET_CART_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]], empty: true});
        }
    }).catch(errors => {
        dispatch({ type: GET_CART_FAILED, payload: errors.response});
    });
};

export const on_basket = (service_id, entity) => {
    return dispatch => {
        dispatch({ type: ADD_CART, service_id, entity });
        const route = `sale/cart/add/entity_relation_services/${service_id}?wstoken=${wstoken}`;
        post(route).then(result => {
            if(result.data.action) {
                dispatch({ type: ADD_CART_SUCCESS, service_id, entity, response: result.data.data });
            }else {
                if(result.data.remove_cart_item) {
                    dispatch({ type: REMOVE_CART_SUCCESS, service_id, entity, response: result.data.data });
                }else {
                    dispatch({ type: ADD_CART_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]]});
                }

            }
            ToastAndroid.showWithGravity(result.data.message, ToastAndroid.SHORT, ToastAndroid.TOP);
        }).catch(errors => {
            dispatch({ type: ADD_CART_FAILED, payload: errors.response});
        });
    }
};

export const remove_basket = (id = 0, cart_id = 0) => {
    return dispatch => {
        dispatch({ type: REMOVE_CART });
        post(`sale/cart/delete/${cart_id}`).then(result => {
            if(result.data.action) {
                dispatch({ type: REMOVE_CART_SUCCESS, id, cart_id, response: result.data.data });
            }else {
                dispatch({ type: REMOVE_CART_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]]});
            }
            ToastAndroid.showWithGravity(result.data.message, ToastAndroid.SHORT, ToastAndroid.TOP);
        }).catch(errors => {
            dispatch({ type: REMOVE_CART_FAILED, payload: errors.response});
        });
    }
};

export const clean_basket = () => {
    return {
        type: CLEAN_BASKET
    }
};

export const on_payment = (data = {}) => {
    return async dispatch => {
        const Authorization = await AsyncStorage.getItem(ACCESS_TOKEN);
        const headers = (Authorization) ? { Authorization } : {};
        axios.post(`${API_URL}sale/cart/booking/save?wstoken=${wstoken}`, data, { headers }).then(result => {
            console.log(result);
            const url = result.data.redirect.replace('api.','www.');
            if(result.data.action) {
                Linking.canOpenURL(url).then(supported => {
                    return Linking.openURL(url);
                });
            }else {
                ToastAndroid.showWithGravity(result.data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
            }
        }).catch(errors => {
            console.log(errors.response);
            console.log(errors.request);
        });
    }
};
import {GET_CATEGORIES, GET_CATEGORIES_SUCCESS} from "../types";


export const get_categories = () => {
    return dispatch => {
        dispatch({ type: GET_CATEGORIES });
        const payload = [
            {
                id: 1,
                title: 'Nail',
                image_url: 'https://www.inkyournail.com/wp-content/uploads/2013/06/red-nail-art-24.jpg'
            },
            {
                id: 2,
                title: 'Eyebrows',
                image_url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlpMXz2MLhYo8b9ZYI_e4hQXjNHkB3UlLTn1nxsRaW6qhTXC72'
            },
            {
                id: 3,
                title: 'Hair',
                image_url: 'https://i.pinimg.com/originals/b5/05/ec/b505ec735ca63fc4a9b85a0814f7bfde.jpg'
            },
            {
                id: 4,
                title: 'Epilation',
                image_url: 'https://s3.envato.com/files/146408978/preview.jpg'
            },
            {
                id: 5,
                title: 'Massage',
                image_url: 'https://www.tlcmassageschool.com/wp-content/uploads/2012/01/massage.jpg'
            },
            {
                id: 6,
                title: 'Piercings',
                image_url: 'https://i.pinimg.com/736x/18/61/0d/18610d0cf70db7793616b16878ab436c--dermal-piercing-hip-hip-dermals.jpg'
            },
            {
                id: 7,
                title: 'Makeup',
                image_url: 'https://i.pinimg.com/736x/1d/92/d9/1d92d9640227b4418d08f08cdd885271--amazing-makeup-wedding.jpg'
            },
            {
                id: 8,
                title: 'Face Mask',
                image_url: 'https://d1yn1kh78jj1rr.cloudfront.net/image/preview/rDtN98Qoishumwih/graphicstock-beautiful-woman-applying-cucumber-on-her-face-with-cosmetic-mask-isolated-on-white-background_HdrfLrnrhg_SB_PM.jpg'
            }
        ];
        setTimeout(() => {
            dispatch({ type: GET_CATEGORIES_SUCCESS, payload });
        }, 1000);
    }
};
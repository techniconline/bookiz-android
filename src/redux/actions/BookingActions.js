import {GET_BOOKINGS, GET_BOOKINGS_FAILED, GET_BOOKINGS_SUCCESS} from "../types";
import {x_get} from "./http";

export const get_bookings = (next_page_url = null, refresh = false) => {
    return dispatch => {
        dispatch({ type: GET_BOOKINGS, page_loading: (next_page_url !== null), refresh });
        const url = (next_page_url !== null) ? next_page_url : 'booking/user/booking/list';
        x_get(url).then(result => {
            console.log(result);
            if(result.data.action) {
                dispatch({ type: GET_BOOKINGS_SUCCESS, payload: result.data.data.data, response: result.data.data });
            }else {
                dispatch({ type: GET_BOOKINGS_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]] });
            }
        }).catch(errors => {
            dispatch({ type: GET_BOOKINGS_FAILED, payload: errors.response });
        });
    }
};
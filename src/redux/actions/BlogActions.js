import {GET_BLOG, GET_BLOG_FAILED, GET_BLOG_SUCCESS} from "../types";
import {x_get} from "./http";

export const get_blog = (id = 0) => {
    return dispatch => {
        dispatch({ type: GET_BLOG });
        x_get(`blog/view/${id}`).then(result => {
            console.log('get_blog', result);
            if(result.data.action) {
                dispatch({ type: GET_BLOG_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_BLOG_FAILED, payload: [[result.data.error ? result.data.error : result.data.message]] });
            }
        }).catch(errors => {
            dispatch({ type: GET_BLOG_FAILED, payload: errors.response });
        });
    }
};
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import {ACCESS_TOKEN} from "../types/auth";

export const API_URL = 'http://api.beautyday.ir/';
export const wstoken = 'e8c37c4ac2a3ab2d202cc8bc12584fd05521ee69';
export axios from 'axios';
export const post = async (route, data = {}, url = null) => {
    url = (url!==null) ? url : API_URL + route;
    const Authorization = await AsyncStorage.getItem(ACCESS_TOKEN);
    const headers = (Authorization) ? { Authorization } : {};
    return axios.post(url, { ...data, wstoken}, { headers });
};
export const x_get = async (route, url = null) => {
    url = ((url!==null) ? url : API_URL + route) + '?wstoken=' + wstoken;
    const Authorization = await AsyncStorage.getItem(ACCESS_TOKEN);
    const headers = (Authorization) ? { Authorization } : {};
    return axios.get(url, { headers});
};
export const get = async (route, data = {}, url = null) => {
    url = (url!==null) ? url : API_URL + route;
    const Authorization = await AsyncStorage.getItem(ACCESS_TOKEN);
    const headers = (Authorization) ? { Authorization } : {};
    return axios.post(url, { ...data, _method: 'GET', wstoken}, { headers });
};
export const x_delete = async (route, url = null) => {
    url = (url!==null) ? url : API_URL + route;
    const Authorization = await AsyncStorage.getItem(ACCESS_TOKEN);
    const headers = (Authorization) ? { Authorization } : {};
    return axios.post(url, { _method: 'DELETE', wstoken}, { headers });
};
import {
    GET_COMMENTS_ENTITY, GET_COMMENTS_ENTITY_FAILED, GET_COMMENTS_ENTITY_SUCCESS,
    GET_ENTITIES, GET_ENTITIES_FAILED,
    GET_ENTITIES_SUCCESS,
    GET_ENTITY, GET_ENTITY_FAILED,
    GET_ENTITY_SUCCESS, GET_SERVICES_ENTITY, GET_SERVICES_ENTITY_FAILED,
    GET_SERVICES_ENTITY_SUCCESS, ON_LIKED
} from "../types";
import {get, post, wstoken} from './http';

export const get_entities = (data = {}, next_page_url = null, refresh = false) => {
    return dispatch => {
        dispatch({type: GET_ENTITIES, page_loading: (next_page_url !== null), refresh });
        get('entity/search', data, next_page_url).then(result => {
            if (result.data.action) {
                dispatch({type: GET_ENTITIES_SUCCESS, payload: result.data.data.data, response: result.data.data});
            } else {
                dispatch({
                    type: GET_ENTITIES_FAILED,
                    payload: [[(result.data.error) ? result.data.error : result.data.message]]
                });
            }
        }).catch(errors => {
            dispatch({type: GET_ENTITIES_FAILED, payload: errors.response});
        });
    }
};

export const get_entity = (item) => {
    return dispatch => {
        dispatch({type: GET_ENTITY});
        get(`entity/view/${item.id}/${item.title}`).then(result => {
            if (result.data.action) {
                call_services_entity(dispatch, result.data.data.id);
                call_comments_entity(dispatch, result.data.data.id);
                dispatch({type: GET_ENTITY_SUCCESS, payload: result.data.data});
            } else {
                dispatch({
                    type: GET_ENTITY_FAILED,
                    payload: [[(result.data.error) ? result.data.error : result.data.message]]
                });
            }
        }).catch(errors => {
            dispatch({type: GET_ENTITY_FAILED, payload: errors.response});
        });
    }
};

const call_services_entity = (dispatch, entity_id) => {
    dispatch({type: GET_SERVICES_ENTITY});
    get(`entity/user/show_services_entity/${entity_id}`).then(result => {
        console.log(result);
        if (result.data.action) {
            dispatch({type: GET_SERVICES_ENTITY_SUCCESS, payload: result.data.data});
        } else {
            dispatch({
                type: GET_SERVICES_ENTITY_FAILED,
                payload: [[(result.data.error) ? result.data.error : result.data.message]]
            });
        }
    }).catch(errors => {
        dispatch({type: GET_SERVICES_ENTITY_FAILED, payload: errors.response});
    });
};
export const get_services_entity = (entity_id) => {
    return dispatch => call_services_entity(dispatch, entity_id);
};

const call_comments_entity = (dispatch, entity_id) => {
    dispatch({type: GET_COMMENTS_ENTITY});
    get(`feedback/comment/entities/${entity_id}/get`).then(result => {
        if (result.data.action) {
            dispatch({type: GET_COMMENTS_ENTITY_SUCCESS, payload: result.data.data});
        } else {
            dispatch({
                type: GET_COMMENTS_ENTITY_FAILED,
                payload: [[(result.data.error) ? result.data.error : result.data.message]]
            });
        }
    }).catch(errors => {
        dispatch({type: GET_COMMENTS_ENTITY_FAILED, payload: errors.response});
    });
};
export const get_comments_entity = (entity_id) => {
    return dispatch => call_comments_entity(dispatch, entity_id);
};

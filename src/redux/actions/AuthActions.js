import {post, x_get, axios, wstoken, API_URL} from './http';
import { AsyncStorage, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
    ACCESS_TOKEN, ACCESS_USER, GET_USER, GET_USER_FAILED, GET_USER_SUCCESS,
    ON_AUTH,
    ON_LOGIN_FAILED,
    ON_LOGIN_SUCCESS,
    ON_REGISTER_FAILED,
    ON_REGISTER_SUCCESS, SAVE_PROFILE, SAVE_PROFILE_FAILED, SAVE_PROFILE_SUCCESS
} from "../types";
import { call_cart } from "./BasketActions";
import {http_errors} from "../../components/Functions";

export const on_login = (data) => {
    return dispatch => {
        dispatch({ type: ON_AUTH });
        post('user/jwt/login', data).then(async result => {
            if(result.data.action) {
                await AsyncStorage.setItem(ACCESS_TOKEN, 'bearer '+result.data.data.token);
                call_cart();
                call_user();
                Actions.replace('main');
                dispatch({ type: ON_LOGIN_SUCCESS });
            }else {
                dispatch({ type: ON_LOGIN_FAILED, payload: http_errors(result, true) });
            }
        }).catch(errors => {
            dispatch({ type: ON_LOGIN_FAILED, payload: http_errors(errors) });
        });
    };
};

export const on_register = (data) => {
    return dispatch => {
        dispatch({ type: ON_AUTH });
        post('user/jwt/register', data).then(async result => {
            if(result.data.action) {
                await AsyncStorage.setItem(ACCESS_TOKEN, 'bearer '+result.data.token);
                Actions.replace('main');
                dispatch({ type: ON_REGISTER_SUCCESS });
            }else {
                dispatch({ type: ON_REGISTER_FAILED, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            dispatch({ type: ON_REGISTER_FAILED, payload: errors.response.data.errors });
        });
    };
};


export const on_logout = () => {
    return dispatch => {
        post('user/jwt/logout').then(result => {
        }).catch(error => {
        });
    };
};


const call_user = (dispatch) => {
    AsyncStorage.getItem(ACCESS_USER).then(result => {
        if(result !== null) {
            dispatch({ type: GET_USER_SUCCESS, payload: JSON.parse(result)})
        }
    });
    x_get('user/profile/edit').then(async result => {
        if(result.data.action){
            AsyncStorage.setItem(ACCESS_USER, JSON.stringify(result.data.data));
            dispatch({ type: GET_USER_SUCCESS, payload: result.data.data})
        }else {
            dispatch({ type: GET_USER_FAILED, payload: [[result.data.error ? result.data.error : result.data.message]]})
        }
    }).catch(errors => {
        dispatch({ type: GET_USER_FAILED, payload: errors.response})
    })
};

export const get_user = () => {
    return dispatch => {
        dispatch({ type: GET_USER });
        call_user(dispatch);
    }
};

const createFormData = (photo, body) => {
    const data = new FormData();
    data.append("avatar", {
        name: photo.fileName,
        type: photo.type,
        uri: Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
    });
    Object.keys(body).forEach(key => {
        data.append(key, body[key]);
    });
    return data;
};


export const save_profile = (data = {}, avatar = null, delete_avatar = false) => {
    return async dispatch => {
        dispatch({ type: SAVE_PROFILE });
        if(avatar !== null) {
            data = createFormData(avatar, data);
        }
        const Authorization = await AsyncStorage.getItem(ACCESS_TOKEN);
        const headers = (Authorization) ? { Authorization } : {};
        axios.post(`${API_URL}user/profile/save?wstoken=${wstoken}`, data, {headers}).then(result => {
            if(result.data.action) {
                dispatch({ type: SAVE_PROFILE_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: SAVE_PROFILE_FAILED, payload: [[result.data.error ? result.data.error : result.data.message]]});
            }
        }).catch(errors => {
            dispatch({ type: SAVE_PROFILE_FAILED, payload: errors.response});
        });
    }
};
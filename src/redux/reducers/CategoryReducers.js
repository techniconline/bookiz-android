import {GET_CATEGORIES, GET_CATEGORIES_SUCCESS} from "../types";

const INITIAL_STATE = {
    loading: false,
    data: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_CATEGORIES:
            return { ...state, loading: true };
        case GET_CATEGORIES_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        default:
            return state;
    }
}
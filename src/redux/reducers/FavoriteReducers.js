import {GET_FAVORITES, GET_FAVORITES_FAILED, GET_FAVORITES_SUCCESS} from "../types";

const INITIAL_STATE = {
    loading: true,
    loading_page: false,
    loading_refresh: false,
    data: [],
    errors: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_FAVORITES:
            return {
                ...state,
                loading: (!action.refresh && !action.page_loading),
                loading_page: (!action.refresh && action.page_loading),
                loading_refresh: (action.refresh),
                errors: null
            };
        case GET_FAVORITES_SUCCESS:
            const data = (action.response.current_page > 1) ? [...state.data, ...action.payload] : action.payload;
            return {
                ...state,
                loading: false,
                loading_page: false,
                loading_refresh: false,
                data,
                response: action.response
            };
        case GET_FAVORITES_FAILED:
            return {
                ...state,
                loading: false,
                loading_page: false,
                loading_refresh: false,
                errors: action.payload,
            };
        default:
            return state;
    }
}
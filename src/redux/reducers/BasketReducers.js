import {
    ADD_CART,
    ADD_CART_SUCCESS,
    CLEAN_BASKET, GET_CART, GET_CART_FAILED,
    GET_CART_SUCCESS,
    REMOVE_BASKET,
    REMOVE_CART_SUCCESS
} from "../types";
import _ from 'lodash';

const INITIAL_STATE = {
    add_loading: [],
    loading: false,
    basket: null,
    errors: null,
    data: [],
    counter: 0,
    entity: null
};

export default (state = INITIAL_STATE, action) => {
    const data = state.data;
    const add_loading = state.add_loading;
    switch (action.type) {
        case GET_CART:
            return { ...state, loading: true, errors: null };
        case GET_CART_SUCCESS:
            const get_data = state.data;
            _.map(action.payload.cart_items, cart_item => { if(get_data.indexOf(cart_item.item_id) === -1) get_data.push(cart_item.item_id)});
            return { ...state, loading: false, basket: action.payload, data: get_data, counter: get_data.length };
        case GET_CART_FAILED:
            let counter_empty = state.counter;
            let data_empty = state.data;
            if(action.empty) {
                counter_empty = 0;
                data_empty = [];
            }
            return { ...state, loading: false, errors: action.payload, counter: counter_empty, data: data_empty };
        case ADD_CART:
            add_loading.push(action.service_id+'_'+action.entity.id);
            return { ...state, add_loading };
        case ADD_CART_SUCCESS:
            const inda = add_loading.indexOf(action.service_id+'_'+action.entity.id);
            add_loading.splice(inda, 1);
            return {
                ...state,
                entity: action.entity,
                counter: action.response.count,
                data: [ ...data, action.service_id ],
                add_loading
            };
        case REMOVE_CART_SUCCESS:
            const indr = add_loading.indexOf(action.service_id+'_'+action.entity.id);
            add_loading.splice(indr, 1);
            const remove_data = _.filter(data, item => item !== action.service_id);
            return { ...state, counter: state.counter-1, data: remove_data, add_loading };
        case REMOVE_BASKET:
            delete data[action.id];
            return { ...state, data, counter: state.counter-1 };
        case CLEAN_BASKET:
            return INITIAL_STATE;
        default:
            return state;
    }
}
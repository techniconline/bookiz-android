import {
    GET_COMMENTS_ENTITY, GET_COMMENTS_ENTITY_FAILED, GET_COMMENTS_ENTITY_SUCCESS,
} from "../types";

const INITIAL_STATE = {
    loading: false,
    loading_page: false,
    loading_refresh: false,
    data: [],
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_COMMENTS_ENTITY:
            return { ...state, loading: true };
        case GET_COMMENTS_ENTITY_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_COMMENTS_ENTITY_FAILED:
            return { ...state, loading: false, errors: action.payload };
        default:
            return state;
    }
}

import {GET_BLOG, GET_BLOG_FAILED, GET_BLOG_SUCCESS} from "../types";

const INITIAL_STATE = {
    loading: false,
    data: [],
    errors: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_BLOG:
            return { ...state, loading: true, errors: null };
        case GET_BLOG_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_BLOG_FAILED:
            return { ...state, loading: false, errors: action.payload };
        default:
            return state;
    }
}
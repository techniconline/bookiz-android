import {
    GET_USER, GET_USER_FAILED,
    GET_USER_SUCCESS,
    ON_AUTH,
    ON_LOGIN_FAILED,
    ON_LOGIN_SUCCESS,
    ON_REGISTER_FAILED,
    ON_REGISTER_SUCCESS, SAVE_PROFILE, SAVE_PROFILE_FAILED, SAVE_PROFILE_SUCCESS
} from "../types";

const INITIAL_STATE = {
    loading: false,
    login_errors: null,
    register_errors: null,
    user_loading: false,
    user_errors: null,
    user: null,
    token: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ON_AUTH:
            return { ...state, loading: true, register_errors: null, login_errors: null };
        case ON_LOGIN_SUCCESS:
            return { ...state, loading: false };
        case ON_LOGIN_FAILED:
            return { ...state, loading: false, login_errors: action.payload };
        case ON_REGISTER_SUCCESS:
            return { ...state, loading: false };
        case ON_REGISTER_FAILED:
            return { ...state, loading: false, register_errors: action.payload };
        case GET_USER:
            return { ...state, user_loading: true, user_errors: null };
        case GET_USER_SUCCESS:
            return { ...state, user_loading: false, user: action.payload };
        case GET_USER_FAILED:
            return { ...state, user_loading: false, user_errors: action.payload };
        case SAVE_PROFILE:
            return { ...state, profile_loading: true, profile_errors: null };
        case SAVE_PROFILE_SUCCESS:
            return { ...state, profile_loading: false, user: { ...state.user, ...action.payload} };
        case SAVE_PROFILE_FAILED:
            return { ...state, profile_loading: false, profile_errors: action.payload };
        default:
            return state;
    }
}
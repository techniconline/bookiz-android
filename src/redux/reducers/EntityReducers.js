import {
    GET_ENTITIES, GET_ENTITIES_FAILED,
    GET_ENTITIES_SUCCESS,
    GET_ENTITY, GET_ENTITY_FAILED,
    GET_ENTITY_SUCCESS,
    GET_SERVICES_ENTITY,
    GET_SERVICES_ENTITY_SUCCESS
} from "../types/index";

const INITIAL_STATE = {
    loading: false,
    loading_page: false,
    loading_refresh: false,
    data: [],
    response: null,
    errors: null,
    single_loading: false,
    single_data: null,
    single_errors: null,
    services_loading: false,
    services: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_ENTITIES:
            return {
                ...state,
                loading: (!action.refresh && !action.page_loading),
                loading_page: (!action.refresh && action.page_loading),
                loading_refresh: (action.refresh),
                errors: null
            };
        case GET_ENTITIES_SUCCESS:
            const data = (action.response.current_page > 1) ? [...state.data, ...action.payload] : action.payload;
            return {
                ...state,
                loading: false,
                loading_page: false,
                loading_refresh: false,
                data,
                response: action.response
            };
        case GET_ENTITIES_FAILED:
            return {
                ...state,
                loading: false,
                loading_page: false,
                loading_refresh: false,
                errors: action.payload,
            };
        case GET_ENTITY:
            return { ...state, single_loading: true };
        case GET_ENTITY_SUCCESS:
            return { ...state, single_loading: false, single_data: action.payload };
        case GET_ENTITY_FAILED:
            return { ...state, single_loading: false, single_errors: action.payload };
        case GET_SERVICES_ENTITY:
            return { ...state, services_loading: true };
        case GET_SERVICES_ENTITY_SUCCESS:
            return { ...state, services_loading: false, services: action.payload };
        default:
            return state;
    }
}

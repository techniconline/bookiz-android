import { combineReducers } from 'redux';
import AuthReducers from "./AuthReducers";
import CategoryReducers from "./CategoryReducers";
import EntityReducers from "./EntityReducers";
import BasketReducers from "./BasketReducers";
import SettingsReducers from "./SettingsReducers";
import EntityCommentReducers from "./EntityCommentReducers";
import FavoriteReducers from "./FavoriteReducers";
import BookingReducers from "./BookingReducers";
import BlogReducers from "./BlogReducers";

export default combineReducers({
    auth: AuthReducers,
    category: CategoryReducers,
    favorite: FavoriteReducers,
    entity: EntityReducers,
    entity_comment: EntityCommentReducers,
    basket: BasketReducers,
    booking: BookingReducers,
    setting: SettingsReducers,
    blog: BlogReducers,
});
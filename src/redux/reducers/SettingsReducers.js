import {
    GET_HOME,
    GET_HOME_SUCCESS,
    GET_HOME_FAILED,
    SAVE_LANGUAGE,
    GET_COUNTRIES,
    GET_COUNTRIES_SUCCESS,
    GET_STATES_SUCCESS,
    GET_CITIES_SUCCESS,
    GET_LOCATION, GET_LOCATION_SUCCESS, GET_LOCATION_FAILED
} from "../types";

const INITIAL_STATE = {
    lang: 'en',
    direction: 'ltr',

    home_loading: true,
    home_data: null,
    home_errors: null,

    location_loading: false,
    location_errors: null,
    location_data: null,

    countries: [],
    states: [],
    cities: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_HOME:
            return { ...state, home_loading: true, home_errors: null };
        case GET_HOME_SUCCESS:
            return { ...state, home_loading: false, home_data: action.payload };
        case GET_HOME_FAILED:
            return { ...state, home_loading: false, home_errors: action.payload };
        case SAVE_LANGUAGE:
            return { ...state, lang: action.payload.lang, direction: action.payload.dir };
        case GET_COUNTRIES_SUCCESS:
            return { ...state, countries: action.payload };
        case GET_STATES_SUCCESS:
            return { ...state, states: action.payload };
        case GET_CITIES_SUCCESS:
            return { ...state, cities: action.payload };
        case GET_LOCATION:
            return { ...state, location_loading: true, location_data: null, location_errors: null };
        case GET_LOCATION_SUCCESS:
            return { ...state, location_loading: false, location_data: action.payload };
        case GET_LOCATION_FAILED:
            return { ...state, location_loading: false, location_errors: action.payload };
        default:
            return state;
    }
}

export const Font_Regular = 'Roboto-Regular';
export const Font_Medium = 'Roboto-Medium';
export const Font_Bold = 'Roboto-Bold';

export const Rtl_Font_Regular = 'IRANSansMobile';
export const Rtl_Font_Medium = 'IRANSansMobile_Medium';
export const Rtl_Font_Bold = 'IRANSansMobile_Bold';

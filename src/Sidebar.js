import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ImageBackground, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Avatar, SidebarImage} from "./resources/Image";
import {Text} from "./components/Elements";
import { Actions } from 'react-native-router-flux';
import { on_logout } from './redux/actions';
import translate from './translate';

class Sidebar extends Component {
    constructor(props) {
        super(props);
    }
    onLogout() {
        this.props.on_logout();
        AsyncStorage.getAllKeys().then(async keys => {
            await AsyncStorage.multiRemove(keys);
            Actions.replace('auth');
        });
    }
    render() {
        const { lang, user, direction } = this.props;
        const rtl = direction === 'rtl';
        let small_avatar_url = null;
        let full_name = null;
        let mobile = null;
        let email = null;
        if(user !== null) {
            email = user.user_connectors[0].connector.name === 'email' ? user.user_connectors[0].value : null;
            mobile = user.user_connectors[0].connector.name !== 'email' ? user.user_connectors[0].value : null;
            small_avatar_url = user.small_avatar_url;
            full_name = user.first_name + " " + user.last_name;
        }
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: '#fff' }}>
                <ScrollView>
                    {(user === null) ? (
                        <ImageBackground style={styles.backImage} source={SidebarImage}>
                            <View style={styles.userInfo}>
                                <View style={styles.avatarContainer}>
                                    <Image source={Avatar} style={styles.avatar} />
                                </View>
                                <Text style={styles.name} />
                                <Text style={styles.name} />
                            </View>
                        </ImageBackground>
                        ) : (
                        <ImageBackground style={styles.backImage} source={SidebarImage}>
                            <View style={styles.userInfo}>
                                <View style={styles.avatarContainer}>
                                    <Image source={{ uri: small_avatar_url }} style={styles.avatar} />
                                </View>
                                <Text rtl={rtl} style={styles.name}>{full_name}</Text>
                                <Text rtl={rtl} style={styles.name}>{(email !== null) ? email : mobile}</Text>
                            </View>
                        </ImageBackground>
                    )}
                    <View style={styles.list}>
                        <TouchableOpacity style={styles.item}>
                            <Icon name="home" size={18} color="#303F9F" />
                            <Text rtl={rtl} style={styles.itemTxt}>{translate.home[lang]}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.item}>
                            <Icon name="user-alt" solid size={18} color="#4CAF50" />
                            <Text rtl={rtl} style={styles.itemTxt}>{translate.profile[lang]}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.item} onPress={()=>{ Actions.push('settings'); }}>
                            <Icon name="cogs" solid size={18} color="#03A9F4" />
                            <Text rtl={rtl} style={styles.itemTxt}>{translate.settings[lang]}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.item} onPress={this.onLogout.bind(this)}>
                            <Icon name="cogs" solid size={18} color="#03A9F4" />
                            <Text rtl={rtl} style={styles.itemTxt}>{translate.logout[lang]}</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    backImage: {
        width: '100%',
    },
    userInfo: {
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    avatarContainer: {
        width: 85,
        height: 85,
        borderRadius: 70,
        marginBottom: 8,
        overflow: 'hidden'
    },
    avatar: {
        width: '100%',
        height: '100%',
    },
    name: {
        fontSize: 16,
        color: '#fff',
        marginTop: 2,
        textAlign: 'left'
    },
    list:{
        paddingVertical: 5
    },
    item: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    itemTxt: {
        fontSize: 18,
        color: '#666',
        marginLeft: 10
    }
};

const mapStateToProps = state => {
    const { lang, direction } = state.setting;
    return { lang, direction, user:  state.auth.user };
};

export default connect(mapStateToProps,  { on_logout })(Sidebar);
import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import Sidebar from "./Sidebar";
import {Drawer, Scene, Stack, Tabs} from "react-native-router-flux";
import Home from "./pages/Home/Home";
import Icon from "react-native-vector-icons/FontAwesome5";
import Bookings from "./pages/Bookings/Bookings";
import Favorites from "./pages/Favorites/Favorites";
import Settings from "./pages/Settings/Settings";
import Register from "./pages/Auth/Register";
import Login from "./pages/Auth/Login";
import Entities from "./pages/Entities/Entities";
import Entity from "./pages/Entities/Entity/Entity";
import Services from "./pages/Services/Services";
import Search from "./pages/Search/Search";
import Basket from "./pages/Basket/Basket";
import Payment from "./pages/Basket/Payment";

class MainRouter extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { lang, direction, is_login } = this.props;
        const rtl = direction === 'rtl';
        console.log('MainRouter');
        return (
            <Fragment>
                <Drawer
                    hideNavBar
                    key="main"
                    contentComponent={Sidebar}
                    // drawerPosition={(this.props.direction === 'rtl') ? 'right' : 'left'}
                    drawerPosition="left"
                >
                    <Tabs key="mainTab"
                          tabs={true}
                          hideNavBar
                          tabBarStyle={{ backgroundColor: '#fff'}}>
                        {/*<Scene key='tab1'*/}
                        {/*hideNavBar title='Home'*/}
                        {/*component={Home}*/}
                        {/*icon={({ focused }) => (*/}
                        {/*<Image style={{ width: 30, height: 30 }}*/}
                        {/*source={focused ? Logo : BackgroundImg}*/}
                        {/*resizeMode="contain" />*/}
                        {/*)} />*/}
                        <Scene key='home'
                               hideNavBar title="HOME"
                               component={Home}
                               icon={({ focused }) => (
                                   <Icon name="home" size={25} color={focused ? '#EC407A' : "#999"} />
                               )} />

                        <Scene key='appointments'
                               hideNavBar
                               title='APPOINTMENTS'
                               component={Bookings}
                               icon={({ focused }) => (
                                   <Icon name="calendar" size={25} color={focused ? '#EC407A' : "#999"} />
                               )} />
                        <Scene key='favorites'
                               hideNavBar
                               title='FAVORITES'
                               component={Favorites}
                               icon={({ focused }) => (
                                   <Icon name="heart" size={25} solid color={focused ? '#EC407A' : "#999"} />
                               )} />
                        <Scene key='profile'
                               hideNavBar
                               title='PROFILE'
                               component={Settings}
                               icon={({ focused }) => (
                                   <Icon name="user" size={25} color={focused ? '#EC407A' : "#999"} />
                               )} />
                    </Tabs>
                </Drawer>
                <Scene key="entities" hideNavBar component={Entities} />
                <Scene key="entity" hideNavBar component={Entity} />
                <Scene key="services" hideNavBar component={Services} />
                <Scene key="search" hideNavBar component={Search} />
                <Scene key="basket" hideNavBar component={Basket} />
                <Scene key="payment" hideNavBar component={Payment} />
                <Scene key="settings" hideNavBar component={Settings} />
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    const { direction, lang } = state.setting;
    return { direction, lang }
};

export default connect(mapStateToProps, null)(MainRouter);
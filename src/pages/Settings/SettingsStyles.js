import {Font_Medium, Rtl_Font_Medium} from "../../redux/types";
import { Dimensions } from 'react-native';

export default {
    container: {
        flex: 1,
        paddingVertical: 20,
        paddingHorizontal: 20
    },
    profile_container: {
        width: '100%',
        minHeight: 200,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7,
        elevation: 5,
        overflow: 'hidden'
    },
    profile: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    img_container: {
        width: 90,
        height: 90,
        borderRadius: 90,
        backgroundColor: '#eee',
        elevation: 5,
        marginBottom: 10,
        overflow: 'hidden'
    },
    img: {
        width: '100%',
        height: '100%'
    },
    name: {
        fontSize: 18,
        color: '#fff'
    },
    mobile: {
        fontSize: 15,
        color: '#fff'
    },
    links: {
        width: '100%',
        borderRadius: 7,
        elevation: 5,
        backgroundColor: '#fff',
        paddingHorizontal: 10,
        overflow: 'hidden'
    },
    link: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderBottomColor: '#eee',
        borderBottomWidth: 1
    },
    rtl_link: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderBottomColor: '#eee',
        borderBottomWidth: 1
    },
    txt_container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    rtl_txt_container: {
        flexDirection: 'row-reverse',
        alignItems: 'center'
    },
    link_text: {
        fontSize: 18,
        fontFamily: Font_Medium,
        color: '#666',
        marginLeft: 10
    },
    rtl_link_text: {
        fontSize: 18,
        fontFamily: Rtl_Font_Medium,
        color: '#666',
        marginRight: 10
    }
}
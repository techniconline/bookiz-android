import React, { Component } from 'react';
import { connect } from 'react-redux';
import {TouchableOpacity, View} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import {Navbar, Text} from "../../../components";
import { save_language } from '../../../redux/actions';
import styles from "./LanguageStyles";
import translate from '../../../translate';
import _ from 'lodash';
import {Actions} from "react-native-router-flux";

class Language extends Component {
    constructor(props) {
        super(props);
        this.state = {
            languages: [
                { lang: 'en', text: 'English', direction: 'ltr' },
                { lang: 'fa', text: 'فارسی', direction: 'rtl' },
            ]
        }
    }
    changeLanguage(language) {
        const { save_language } = this.props;
        save_language(language.lang, language.direction);
    }
    render() {
        const { lang, direction } = this.props;
        const rtl = direction === 'rtl';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 10 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <Icon color="#fff" size={20} name="arrow-left" solid />
            </TouchableOpacity>
        );
        return (
            <View style={{ flex: 1 }}>
                <Navbar rtl={rtl} leftComponent={left} title={translate.language[lang]} />
                <View style={styles.container}>
                    {_.map(this.state.languages, (language, key) => (
                        <TouchableOpacity key={key} activeOpacity={0.6} onPress={this.changeLanguage.bind(this, language)} style={rtl ? styles.rtl_item: styles.item}>
                            {(language.lang === lang) ?
                                <View style={[rtl ? styles.rtl_select : styles.select, { borderColor: '#000'}]}>
                                    <Icon color="#000" size={15} name="check" solid />
                                </View>
                                : <View style={rtl ? styles.rtl_select : styles.select} />}
                            <Text rtl={rtl} style={rtl ? styles.rtl_title : styles.title}>{language.text}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    const { lang, direction } = state.setting;
    return { lang, direction }
};

export default connect(mapStateToProps, { save_language })(Language);
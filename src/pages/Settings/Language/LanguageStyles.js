import {Font_Medium, Font_Regular, Rtl_Font_Medium} from "../../../redux/types";
import {danger, success} from "../../../resources/Colors";

export default {
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    item: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    rtl_item: {
        width: '100%',
        flexDirection: 'row-reverse',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    title: {
        fontFamily: Font_Medium,
        fontSize: 18,
        marginLeft: 10
    },
    rtl_title: {
        fontFamily: Rtl_Font_Medium,
        fontSize: 18,
        marginRight: 10
    },
    select: {
        flex: 1,
        maxWidth: 26,
        height: 26,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10
    },
    rtl_select: {
        flex: 1,
        maxWidth: 26,
        height: 26,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0,
        marginLeft: 10
    }
}
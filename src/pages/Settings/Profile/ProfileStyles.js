export default {
    container: {
        flex: 1,
        paddingVertical: 20,
        paddingHorizontal: 20
    },
    profile_container: {
        width: '100%',
        minHeight: 200,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7,
        elevation: 5,
        overflow: 'hidden'
    },
    profile: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    img_wrapper: {
        position: 'relative',
        width: 90,
        height: 90,
        marginBottom: 20,
    },
    img_container: {
        width: '100%',
        height: '100%',
        borderRadius: 90,
        backgroundColor: '#eee',
        elevation: 3,
        overflow: 'hidden'
    },
    img_icon: {
        position: 'absolute',
        bottom: 3,
        left: 3,
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 20,
        elevation: 4,
        zIndex: 100
    },
    img: {
        width: '100%',
        height: '100%'
    },
    name: {
        fontSize: 18,
        color: '#fff',
        textAlign: 'center',
        paddingVertical: 0
    },
    mobile: {
        fontSize: 15,
        color: '#fff',
        textAlign: 'center',
        paddingVertical: 0
    },
    form: {
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 5,
        marginTop: 20,
        marginBottom: 20,
        elevation: 4,
        overflow: 'hidden'
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        paddingHorizontal: 0
    },
    label: {
        color: '#000',
        fontSize: 14
    }
}
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Image, ImageBackground, TouchableOpacity, View, ScrollView, PermissionsAndroid, ActivityIndicator} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import ImagePicker from 'react-native-image-picker';
import {Button, ErrorPage, FullLoading, Input, InputForm, Navbar, SelectForm, Text} from "../../../components";
import { save_language, get_countries, get_states, get_cities, save_profile } from '../../../redux/actions';
import styles from "./ProfileStyles";
import translate from '../../../translate';
import _ from 'lodash';
import {Profile_Background_Image} from "../../../resources/Image";
import {Actions} from "react-native-router-flux";
import {secondary} from "../../../resources/Colors";

class Language extends Component {
    constructor(props) {
        super(props);
        this.state = {
            set_data: false,
            avatar: null,
            delete_avatar: false,
            small_avatar_url: null,
            first_name: '',
            last_name: '',
            mobile: null,
            email: null,
            gender: null,
            marital: null,
            country_id: null,
            state_id: null,
            city_id: null,
        }
    }
    componentDidMount() {
        this.setData();
        this.props.get_countries();
    }
    componentDidUpdate(){
        this.setData();
    }
    setData(){
        const { set_data } = this.state;
        const { user } = this.props;
        if(!set_data && user !== null) {
            const {
                small_avatar_url,
                first_name,
                last_name,
                marital,
                gender,
                user_connectors,
                country_id,
                state_id,
                city_id,
            } = user;
            const email = user_connectors[0].connector.name === 'email' ? user_connectors[0].value : null;
            const mobile = user_connectors[0].connector.name !== 'email' ? user_connectors[0].value : null;
            if(country_id !== null) this.props.get_states(country_id);
            if(state_id !== null) this.props.get_cities(country_id, state_id);
            this.setState({
                set_data: true,
                small_avatar_url,
                first_name,
                last_name,
                marital,
                gender,
                email,
                mobile,
                country_id,
                state_id,
                city_id,
            });
        }
    }
    onChangeCountry(country_id) {
        this.setState({ country_id ,state_id: null, city_id: null });
        this.props.get_states(country_id);
    }
    onChangeState(state_id) {
        this.setState({ state_id, city_id: null });
        this.props.get_cities(this.state.country_id, state_id);
    }
    onSubmit() {
        const {
            delete_avatar,
            avatar,
            first_name,
            last_name,
            marital,
            gender,
            country_id,
            state_id,
            city_id,
        } = this.state;
        const body = {
            first_name,
            last_name,
            marital,
            gender,
            country_id,
            state_id,
            city_id,
        };
        this.props.save_profile(body, avatar , delete_avatar);
    }
    onAvatar() {
        let { avatar } = this.state;
        PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE]).then(granted => {
            const cameraPermission = granted['android.permission.CAMERA'] === "granted";
            const galleryPermission = granted['android.permission.READ_EXTERNAL_STORAGE'] === "granted";

            const options = {
                title: 'AVATAR',
                customButtons: [{ name: 'delete', title: 'DELETE AVATAR' }],
            };
            if(avatar !== null) options.customButtons.push({ name: 'unset', title: 'UNSET AVATAR' });
            if (galleryPermission && cameraPermission) {
                ImagePicker.showImagePicker(options, (response) => {
                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        if(response.customButton === 'delete') {
                            this.setState({ avatar: null, delete_avatar: true });
                        }else if(response.customButton === 'unset') {
                            this.setState({ avatar: null });
                        }
                    } else {
                        this.setState({ avatar: response, delete_avatar: false });
                    }
                });
            } else if(galleryPermission) {
                ImagePicker.launchImageLibrary(options, (response) => {
                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        if(response.customButton === 'delete') {
                            this.setState({ avatar: null, delete_avatar: true });
                        }else if(response.customButton === 'unset') {
                            this.setState({ avatar: null });
                        }
                    } else {
                        this.setState({ avatar: response, delete_avatar: false });
                    }
                });
            }else if (cameraPermission) {
                ImagePicker.launchCamera(options, (response) => {
                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        if(response.customButton === 'delete') {
                            this.setState({ avatar: null, delete_avatar: true });
                        }else if(response.customButton === 'unset') {
                            this.setState({ avatar: null });
                        }
                    } else {
                        this.setState({ avatar: response, delete_avatar: false });
                    }
                });
            }
        });
    }
    renderButton() {
        const { lang, direction, profile_loading } = this.props;
        const rtl = direction === 'rtl';
        if(profile_loading) return <Button backgroundColor={secondary}><ActivityIndicator size="small" color="#fff" animating /></Button>;
        return <Button rtl={rtl} title={translate.save[lang]} backgroundColor={secondary} onPress={this.onSubmit.bind(this)} />;
    }
    render() {
        const { lang, direction, user, user_loading, user_errors, countries, states, cities } = this.props;
        const rtl = direction === 'rtl';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 10 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <Icon color="#fff" size={20} name="arrow-left" solid />
            </TouchableOpacity>
        );
        const {
            delete_avatar,
            avatar,
            small_avatar_url,
            first_name,
            last_name,
            mobile,
            gender,
            email,
            country_id,
            state_id,
            city_id,
            marital } = this.state;
        const marital_options = [
            { value: null, label: translate.select_marital[lang] },
            { value: 1, label: translate.single[lang] },
            { value: 2, label: translate.married[lang] }
        ];
        const gender_options = [
            { value: null, label: translate.select_gender[lang] },
            { value: 1, label: translate.female[lang] },
            { value: 2, label: translate.male[lang] }
        ];
        let countries_options = [{ label: translate.select_country[lang], value: null }];
        let states_options = [{ label: translate.select_state[lang], value: null }];
        let cities_options = [{ label: translate.select_city[lang], value: null }];
        _.map(countries, country => countries_options.push({ label: country.name, value: country.id}));
        _.map(states, state => states_options.push({ label: state.name, value: state.id}));
        _.map(cities, city => cities_options.push({ label: city.name, value: city.id}));

        return (
            <View style={{ flex: 1 }}>
                <Navbar leftComponent={left} rtl={rtl} title={translate.profile[lang]} />
                {(user_loading && user === null) ? <FullLoading /> :
                    (user_errors !== null) ? <ErrorPage errors={user_errors} /> : (
                        <ScrollView>
                            <View style={styles.container}>
                                <ImageBackground source={Profile_Background_Image} style={styles.profile_container}>
                                    <View style={styles.profile}>
                                        <TouchableOpacity activeOpacity={0.8} style={styles.img_wrapper} onPress={this.onAvatar.bind(this)}>
                                            <View style={styles.img_container}>
                                                <Image source={{ uri: (delete_avatar) ? null: (avatar !== null) ? avatar.uri :small_avatar_url}} style={styles.img} />
                                            </View>
                                            <View style={styles.img_icon}>
                                                <Icon color="#999" size={14} name="plus" solid />
                                            </View>
                                        </TouchableOpacity>
                                        <Input style={styles.name} bordered={false} value={first_name + ' ' + last_name}/>
                                        <Text style={styles.mobile}>{(email !== null) ? email : mobile}</Text>
                                    </View>
                                </ImageBackground>
                                <View style={styles.form}>
                                    <InputForm label={translate.first_name[lang]}
                                               value={first_name}
                                               placeholder="John"
                                               inputStyle={styles.input}
                                               labelStyle={styles.label}
                                               onChange={(first_name) => { this.setState({ first_name }) }} />
                                    <InputForm label={translate.last_name[lang]}
                                               value={last_name}
                                               placeholder="Smith"
                                               inputStyle={styles.input}
                                               labelStyle={styles.label}
                                               onChange={(last_name) => { this.setState({ last_name }) }} />
                                    {/*<InputForm label="Mobile"*/}
                                               {/*value={mobile}*/}
                                               {/*type="number-pad"*/}
                                               {/*placeholder="Mobile"*/}
                                               {/*inputStyle={styles.input}*/}
                                               {/*labelStyle={styles.label}*/}
                                               {/*onChange={(mobile) => { this.setState({ mobile }) }} />*/}
                                    {/*<InputForm label="Email"*/}
                                               {/*value={email}*/}
                                               {/*type="email-address"*/}
                                               {/*placeholder="example@example.com"*/}
                                               {/*inputStyle={styles.input}*/}
                                               {/*labelStyle={styles.label}*/}
                                               {/*onChange={(email) => { this.setState({ email }) }} />*/}
                                    <SelectForm mode="dropdown"
                                                value={gender}
                                                label={translate.gender[lang]}
                                                containerStyle={styles.input}
                                                labelStyle={styles.label}
                                                options={gender_options}
                                                onChange={(gender) => { this.setState({ gender }) }} />
                                    <SelectForm mode="dropdown"
                                                value={marital}
                                                label={translate.marital[lang]}
                                                containerStyle={styles.input}
                                                labelStyle={styles.label}
                                                options={marital_options}
                                                onChange={(marital) => { this.setState({ marital }) }} />
                                    {(countries_options.length > 1) ? (
                                        <SelectForm value={country_id}
                                                    label={translate.country[lang]}
                                                    containerStyle={styles.input}
                                                    labelStyle={styles.label}
                                                    options={countries_options}
                                                    onChange={this.onChangeCountry.bind(this)} />
                                    ) : null}
                                    {(country_id !== null && states_options.length > 1) ? (
                                        <SelectForm value={state_id}
                                                    label={translate.state[lang]}
                                                    containerStyle={styles.input}
                                                    labelStyle={styles.label}
                                                    options={states_options}
                                                    onChange={this.onChangeState.bind(this)} />
                                    ) : null}
                                    {(state_id !== null && cities_options.length > 1) ? (
                                        <SelectForm value={city_id}
                                                    label={translate.city[lang]}
                                                    containerStyle={styles.input}
                                                    labelStyle={styles.label}
                                                    options={cities_options}
                                                    onChange={(city_id) => { this.setState({ city_id }) }} />
                                    ) : null}
                                </View>
                                {this.renderButton()}
                            </View>
                        </ScrollView>
                    )}
            </View>
        )
    }
}

const mapStateToProps = state => {
    const {
        lang,
        direction,
        countries,
        states,
        cities
    } = state.setting;
    const {
        user,
        user_loading,
        user_errors,
        profile_loading,
        profile_errors
    } = state.auth;
    return {
        lang,
        direction,
        user,
        user_loading,
        user_errors,
        profile_loading,
        profile_errors,
        countries,
        states,
        cities
    };
};

export default connect(mapStateToProps, { save_language, get_countries, get_states, get_cities, save_profile })(Language);
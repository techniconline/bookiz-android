import React, { Component } from 'react';
import {View, TouchableOpacity, ImageBackground, Image, ScrollView, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import {ErrorPage, FullLoading, ImageIcon, Navbar, Text} from '../../components';
import styles from './SettingsStyles';
import translate from '../../translate';
import Icon from "react-native-vector-icons/FontAwesome5";
import {Profile_Background_Image} from "../../resources/Image";
import { on_logout } from '../../redux/actions';
import {
    IconLanguageActive,
    IconLeftAngle,
    IconLogoutActive,
    IconRightAngle,
    IconUserActive
} from "../../resources/Icon";

class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            set_data: false,
            small_avatar_url: null,
            email: null,
            mobile: null,
            full_name: null,
        }
    }
    componentDidMount() {
        this.setData();
    }
    componentDidUpdate(){
        this.setData();
    }
    setData() {
        const { set_data } = this.state;
        const { user } = this.props;
        if(!set_data && user !== null) {
            const {
                small_avatar_url,
                full_name,
                user_connectors
            } = user;
            const email = user_connectors[0].connector.name === 'email' ? user_connectors[0].value : null;
            const mobile = user_connectors[0].connector.name !== 'email' ? user_connectors[0].value : null;
            this.setState({
                set_data: true,
                small_avatar_url,
                full_name,
                email,
                mobile,
            });
        }
    }
    onLogout() {
        this.props.on_logout();
        AsyncStorage.getAllKeys().then(async keys => {
            await AsyncStorage.multiRemove(keys);
            Actions.replace('auth');
        });
    }
    render() {
        const { lang, direction, user, user_loading, user_errors } = this.props;
        const rtl = direction === 'rtl';
        let small_avatar_url = null;
        let full_name = null;
        let mobile = null;
        let email = null;
        if(user !== null) {
            email = user.user_connectors[0].connector.name === 'email' ? user.user_connectors[0].value : null;
            mobile = user.user_connectors[0].connector.name !== 'email' ? user.user_connectors[0].value : null;
            small_avatar_url = user.small_avatar_url;
            full_name = user.full_name;
        }
        return (
            <View style={{ flex: 1 }}>
                <Navbar rtl={rtl} title={translate.settings[lang]} />
                <ScrollView>
                    <View style={styles.container}>
                        {/*<ImageBackground source={Profile_Background_Image} style={styles.profile_container}>*/}
                            {/*{(user_loading && user === null) ? <FullLoading /> : (user_errors !== null) ? <ErrorPage errors={user_errors} /> : (*/}
                                {/*<View style={styles.profile}>*/}
                                    {/*<View style={styles.img_container}>*/}
                                        {/*<Image source={{ uri: small_avatar_url }} style={styles.img} />*/}
                                    {/*</View>*/}
                                    {/*<Text style={styles.name}>{full_name}</Text>*/}
                                    {/*<Text style={styles.mobile}>{(email !== null) ? email : mobile}</Text>*/}
                                {/*</View>*/}
                            {/*)}*/}
                        {/*</ImageBackground>*/}
                        <View style={styles.links}>
                            <TouchableOpacity style={rtl ? styles.rtl_link : styles.link} onPress={() => { Actions.language(); }}>
                                <View style={rtl ? styles.rtl_txt_container : styles.txt_container}>
                                    {/*<Icon color="#999" size={20} name="globe" solid />*/}
                                    <ImageIcon source={IconLanguageActive} size={20} />
                                    <Text style={rtl ? styles.rtl_link_text : styles.link_text}>{translate.language[lang]}</Text>
                                </View>
                                {/*{(rtl) ? <Icon color="#666" size={15} name="chevron-left" solid /> : <Icon color="#666" size={15} name="chevron-right" solid />}*/}
                                {(rtl) ? <ImageIcon source={IconLeftAngle} size={16} /> : <ImageIcon source={IconRightAngle} size={16} />}
                            </TouchableOpacity>
                            <TouchableOpacity style={[rtl ? styles.rtl_link : styles.link]} onPress={() => { Actions.profile(); }}>
                                <View style={rtl ? styles.rtl_txt_container : styles.txt_container}>
                                    {/*<Icon color="#999" size={20} name="user" solid />*/}
                                    <ImageIcon source={IconUserActive} size={20} />
                                    <Text style={rtl ? styles.rtl_link_text : styles.link_text}>{translate.profile[lang]}</Text>
                                </View>
                                {/*{(rtl) ? <Icon color="#666" size={15} name="chevron-left" solid /> : <Icon color="#666" size={15} name="chevron-right" solid />}*/}
                                {(rtl) ? <ImageIcon source={IconLeftAngle} size={16} /> : <ImageIcon source={IconRightAngle} size={16} />}
                            </TouchableOpacity>
                            <TouchableOpacity style={[rtl ? styles.rtl_link : styles.link, { borderBottomWidth: 0}]} onPress={this.onLogout.bind(this)}>
                                <View style={rtl ? styles.rtl_txt_container : styles.txt_container}>
                                    {/*<Icon color="#999" size={20} name="sign-out-alt" solid />*/}
                                    <ImageIcon source={IconLogoutActive} size={20} />
                                    <Text style={rtl ? styles.rtl_link_text : styles.link_text}>{translate.logout[lang]}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = state => {
    const { user, user_loading, user_errors } = state.auth;
    const { lang, direction } = state.setting;
    return {
        lang,
        direction,
        user,
        user_loading,
        user_errors
    };
};

export default connect(mapStateToProps, { on_logout })(Settings);
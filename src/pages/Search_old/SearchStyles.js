export default {
    searchContainer: {
        flex: 1,
        backgroundColor: '#eee'
    },
    searchTitle: {
        paddingVertical: 5,
        paddingHorizontal: 20,
        color: '#999',
        borderTopColor: '#ccc',
        borderBottomColor: '#ccc',
        borderTopWidth: 1,
        borderBottomWidth: 1,
    },
    footerText: {
        fontSize: 12,
        textAlign: 'center',
        paddingVertical: 5,
        borderTopColor: '#ccc',
        borderTopWidth: 1,
        color: '#999',
    }
}
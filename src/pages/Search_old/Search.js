import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {FlatList, TouchableOpacity, View, ActivityIndicator} from 'react-native';
import {Navbar, Searchbar} from "../../components/Layouts";
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";
import SearchItem from "./SearchItem";
import styles from './SearchStyles';
import {ErrorPage, FullLoading, PageLoading, Text} from "../../components/Elements";
import { get_entities } from '../../redux/actions';
import translate from '../../translate';
import {RTL_FF_Medium, RTL_FF_Regular} from "../../components/theme";

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            page: 1
        }
    }
    componentDidMount() {
        this.props.get_entities({ text: '' });
    }
    onSearch(text) {
        this.setState({ text });
        this.props.get_entities({ text });
    }
    renderHeader() {
        const { lang, direction } = this.props;
        return (
            <Text style={styles.searchTitle} rtl={(direction === 'rtl')}>{translate.results[lang]}</Text>
        )
    }
    renderFooter() {
        const { response, loading_page, lang, direction } = this.props;
        if(response === null) return null;
        if(loading_page) return <PageLoading />;
        return <Text rtl={direction === 'rtl'} style={styles.footerText}>{response.total} {translate.total_results[lang]}</Text>
    }
    handleLoadMore() {
        const {response, get_entities, loading_page, loading_refresh, loading} = this.props;
        if(!loading && !loading_page && !loading_refresh && response.next_page_url !== null)
            get_entities({ text: this.state.text }, response.next_page_url);
    }
    handleRefresh() {
        this.props.get_entities({ text: this.state.text }, null, true);
    }
    render() {
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 10 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <Icon color="#fff" size={20} name="arrow-left" solid />
            </TouchableOpacity>
        );
        const { loading, errors, data, loading_refresh, lang, direction } = this.props;
        // console.log('direction', direction === 'rtl', direction);
        return (
            <View style={{ flex: 1 }}>
                <Navbar title={translate.search[lang].toUpperCase()}
                        rtl={direction === 'rtl'}
                        style={{ elevation: 0 }}
                        leftComponent={left} />
                <Searchbar value={this.state.text}
                           rtl={direction === 'rtl'}
                           onChange={this.onSearch.bind(this)}
                           placeholder={translate.search_placeholder[lang]} />
                {(loading) ? <FullLoading /> : (errors) ? <ErrorPage errors={errors} /> : (
                    <View style={styles.searchContainer}>
                        <FlatList
                            data={data}
                            keyExtractor={(item, index) => String(item.id)}
                            ListFooterComponent={() => this.renderFooter() }
                            ListHeaderComponent={() => this.renderHeader()}
                            renderItem={({item}) => <SearchItem rtl={(direction === 'rtl')} item={item} />}
                            onEndReachedThreshold={0.01}
                            refreshing={loading_refresh}
                            onEndReached={this.handleLoadMore.bind(this)}
                            onRefresh={this.handleRefresh.bind(this)}
                        />
                    </View>
                )}
            </View>
        )
    }
}

const mapStateToProps = state => {
    const { lang, direction } = state.setting;
    return {
        loading: state.entity.loading,
        loading_page: state.entity.loading_page,
        loading_refresh: state.entity.loading_refresh,
        errors: state.entity.errors,
        data: state.entity.data,
        response: state.entity.response,
        lang,
        direction
    }
};

export default connect(mapStateToProps, { get_entities })(Search);
import React, {Component} from 'react';
import { connect } from 'react-redux';
import {FlatList, TouchableOpacity, View, PermissionsAndroid} from 'react-native';
import SearchItem from "./SearchItem";
import styles from './SearchStyles';
import {ErrorPage, FullLoading, PageLoading, Text} from "../../components/Elements";
import { get_entities, get_location } from '../../redux/actions';
import translate from '../../translate';
import {Input} from "../../components/Form";
import Loading from "../../Loading";

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            full_loading: true,
            text: '',
            location: null,
            locationText: '',
            fineLocation: false,
            locationResult: false,
            loadingLocation: false,
            page: 1
        }
    }
    componentDidMount() {
        this.props.get_entities({ text: '' });
    }
    componentDidUpdate() {
        if(this.state.full_loading && this.props.data !== null && (this.props.data.length > 0 || !this.props.loading)) {
            this.setState({ full_loading: false });
        }
        if(this.state.loadingLocation && this.props.location_data !== null) {
            const item = this.props.location_data.features[0];
            this.setState({
                loadingLocation: false,
                fineLocation: false,
                locationResult: false,
                location: (item) ?
                    {
                        longitude: item.center[0],
                        latitude: item.center[1],
                        text: item.place_name
                    } : null}, () => { this.locationInput.blur(); this.onSearch(this.state.text); })
        }
    }
    onSearch(text) {
        this.setState({ text });
        const { location } = this.state;
        if(location !== null) {
            this.props.get_entities({ text, latitude: location.latitude, longitude: location.longitude });
        }else {
            this.props.get_entities({ text });
        }
    }
    onLocation(locationText) {
        this.setState({ locationText });
        this.props.get_location(locationText);
    }
    renderHeader() {
        const { lang, direction } = this.props;
        return (
            <Text style={styles.searchTitle} rtl={(direction === 'rtl')}>{translate.results[lang]}</Text>
        )
    }
    renderFooter() {
        const { response, loading_page, lang, direction } = this.props;
        if(response === null) return null;
        if(loading_page) return <PageLoading />;
        return <Text rtl={direction === 'rtl'} style={styles.footerText}>{response.total} {translate.total_results[lang]}</Text>
    }
    handleLoadMore() {
        const {response, get_entities, loading_page, loading_refresh, loading} = this.props;
        if(!loading && !loading_page && !loading_refresh && response.next_page_url !== null)
            get_entities({ text: this.state.text }, response.next_page_url);
    }
    handleRefresh() {
        this.props.get_entities({ text: this.state.text }, null, true);
    }
    getCurrentLocation() {
        PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(granted => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED) this.watchPosition();
        }).catch(error => {
            console.log('error', error);
        });
        PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(granted => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED) this.watchPosition(true);
        }).catch(error => {
            console.log('error', error);
        })

    }
    watchPosition(fineLocation = false) {
        const options = { timeout: 20000 };
        navigator.geolocation.getCurrentPosition(
            (position) => {
                if(!this.state.fineLocation) {
                    this.setState({ loadingLocation: true });
                    this.setState({ fineLocation }, () => {
                        this.props.get_location('', position.coords.longitude, position.coords.latitude)
                    });
                }
            }, (error) => console.log(error), options);
    }
    onSelectLocation(item) {
        this.locationInput.blur();
        this.setState({
            location: {
                longitude: item.center[0],
                latitude: item.center[1],
                text: item.place_name
            },
            locationResult: false,
            locationText: ''
        }, () => {
            this.onSearch(this.state.text);
        });
    }
    renderItemLocation(item) {
        const rtl = this.props.direction === 'rtl';
        return (
            <TouchableOpacity style={{ marginBottom: 5, paddingVertical: 5 }} onPress={this.onSelectLocation.bind(this, item)}>
                <Text rtl={rtl} style={{ color: '#444'}}>{item.place_name}</Text>
            </TouchableOpacity>
        )
    }
    render() {
        const { loading, errors, data, loading_refresh, lang, direction, location_loading, location_data } = this.props;
        const rtl = direction === 'rtl';
        return (this.state.full_loading) ? <Loading /> : (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={styles.form}>
                    <Input value={this.state.text}
                           leftIcon={(!rtl) ? "search" : ''}
                           rightIcon={(rtl) ? "search" : ''}
                           leftIconColor="#666"
                           rightIconColor="#666"
                           leftIconSize={18}
                           rightIconSize={18}
                           rightLoading={loading && rtl}
                           leftLoading={loading && !rtl}
                           rightLoadingColor="#666"
                           leftLoadingColor="#666"
                           containerStyle={styles.input}
                           rtl={direction === 'rtl'}
                           onChange={this.onSearch.bind(this)}
                           placeholderTextColor="rgba(0,0,0,0.3)"
                           placeholder={translate.search_placeholder[lang]} />
                    <Input value={this.state.locationText}
                           leftIcon={(!rtl) ? "map-marker-alt" : ''}
                           rightIcon={(rtl) ? "map-marker-alt" : ''}
                           leftIconColor="#666"
                           rightIconColor="#666"
                           leftIconSize={18}
                           rightIconSize={18}
                           containerStyle={styles.input}
                           rtl={direction === 'rtl'}
                           onChange={this.onLocation.bind(this)}
                           placeholderTextColor="rgba(0,0,0,0.3)"
                           rightLoading={location_loading && rtl}
                           leftLoading={location_loading && !rtl}
                           rightLoadingColor="#666"
                           onFocus={() => { this.setState({ locationResult: true }); }}
                           numberOfLines={1}
                           refs={(rf) => { this.locationInput = rf; }}
                        // onBlur={() => { this.setState({ locationResult: false }); }}
                           leftLoadingColor="#666"
                           placeholder={(this.state.location !== null) ? this.state.location.text : translate.choose_location[lang]} />
                </View>
                {(loading) ? <FullLoading /> : (errors) ? <ErrorPage errors={errors} /> : (this.state.locationResult) ? (
                    <View style={{ flex: 1, paddingHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.getCurrentLocation.bind(this)} style={{ marginBottom: 15, paddingBottom: 8, borderBottomColor: 'rgba(0,0,0,0.1)', borderBottomWidth: 1 }}>
                            <Text rtl={rtl}>{translate.current_location[lang]}</Text>
                        </TouchableOpacity>
                        {(location_loading) ? <FullLoading /> : (location_data !== null && location_data.features) ?
                            <FlatList
                                data={location_data.features}
                                keyExtractor={(item, index) => String(item.id)}
                                renderItem={({item}) => this.renderItemLocation(item)}
                            />
                            : null}

                    </View>
                ) : (
                    <View style={styles.searchContainer}>
                        <FlatList
                            data={data}
                            keyExtractor={(item, index) => String(item.id)}
                            ListFooterComponent={() => this.renderFooter() }
                            ListHeaderComponent={() => this.renderHeader()}
                            renderItem={({item}) => <SearchItem rtl={rtl} item={item} />}
                            onEndReachedThreshold={0.01}
                            refreshing={loading_refresh}
                            onEndReached={this.handleLoadMore.bind(this)}
                            onRefresh={this.handleRefresh.bind(this)}
                        />
                    </View>
                )}
            </View>
        )
    }
}

const mapStateToProps = state => {
    const { lang, direction, location_loading, location_errors, location_data } = state.setting;
    return {
        location_loading,
        location_errors,
        location_data,
        loading: state.entity.loading,
        loading_page: state.entity.loading_page,
        loading_refresh: state.entity.loading_refresh,
        errors: state.entity.errors,
        data: state.entity.data,
        response: state.entity.response,
        lang,
        direction
    }
};

export default connect(mapStateToProps, { get_entities, get_location })(Search);
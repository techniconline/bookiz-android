import {RTL_FF_Medium} from "../../components/theme";

export default {
    itemContainer: {
        backgroundColor: '#fff',
        paddingHorizontal: 10,
    },
    item: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    rtl_item: {
        flexDirection: 'row-reverse',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    imgContainer: {
        width: 75,
        height: 75,
        borderRadius: 100,
        overflow: 'hidden',
        backgroundColor: '#eee',
        borderWidth: 1,
        borderColor: '#f5f5f5'
    },
    img: {
        width: '100%',
        height: '100%'
    },
    info: {
        marginLeft: 10,
        justifyContent: 'center'
    },
    rtl_info: {
        marginRight: 10,
        justifyContent: 'center'
    },
    title: {
        fontSize: 18,
        color: '#333',
        marginBottom: 10
    },
    rtl_title: {
        fontFamily: RTL_FF_Medium,
        fontSize: 16,
        color: '#333',
        marginBottom: 10
    },
    meta_container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    rtl_meta_container: {
        flexDirection: 'row-reverse',
        alignItems: 'center'
    },
    meta: {
        flexDirection: 'row',
        marginRight: 10
    },
    rtl_meta: {
        flexDirection: 'row-reverse',
        marginRight: 10
    },
    metaText: {
        fontSize: 14,
        marginLeft: 5,
        color: '#999',
        maxWidth: 100
    },
    rtl_metaText: {
        fontSize: 12,
        marginRight: 5,
        color: '#999',
        maxWidth: 100
    }
};
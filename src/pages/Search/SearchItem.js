import React from 'react';
import { TouchableOpacity, View, Image } from 'react-native';
import {ImageIcon, Text} from "../../components/Elements";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Actions } from 'react-native-router-flux';
import styles from './SearchItemStyles';
import {IconMapMarkerCircle} from "../../resources/Icon";

const SearchItem = (props) => {
    const { title, rate, entity_address, media_image } = props.item;
    return (
        <TouchableOpacity activeOpacity={0.8} style={styles.itemContainer} onPress={()=>{ Actions.push('entity', { item: props.item}) }}>
            <View style={(props.rtl) ? styles.rtl_item : styles.item}>
                <View style={styles.imgContainer}>
                    <Image source={{ uri: media_image.url_thumbnail}} style={styles.img} />
                </View>
                <View style={(props.rtl) ? styles.rtl_info : styles.info}>
                    <Text style={(props.rtl) ? styles.rtl_title : styles.title} rtl={props.rtl}>{title}</Text>
                    <View style={(props.rtl) ? styles.rtl_meta_container : styles.meta_container}>
                        <View style={(props.rtl) ? styles.rtl_meta : styles.meta}>
                            <Icon name="star" size={12} solid={Number(rate.data_rate) > 0.5} color="#f1c40f" />
                            <Icon name="star" size={12} solid={Number(rate.data_rate) > 1.5} color="#f1c40f" />
                            <Icon name="star" size={12} solid={Number(rate.data_rate) > 2.5} color="#f1c40f" />
                            <Icon name="star" size={12} solid={Number(rate.data_rate) > 3.5} color="#f1c40f" />
                            <Icon name="star" size={12} solid={Number(rate.data_rate) > 4.5} color="#f1c40f" />
                        </View>
                        {(entity_address !== null) ? (
                            <View style={(props.rtl) ? styles.rtl_meta : styles.meta}>
                                {/*<Icon color="#999" size={16} name="map-marker-alt" solid />*/}
                                <ImageIcon source={IconMapMarkerCircle} size={16} />
                                <Text numberOfLines={1}
                                      ellipsizeMode="tail"
                                      rtl={props.rtl}
                                      style={props.rtl ? styles.rtl_metaText : styles.metaText}>{entity_address.address}</Text>
                            </View>
                        ) : null}
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
};

export default  SearchItem;


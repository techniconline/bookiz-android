export default {
    searchContainer: {
        flex: 1,
        backgroundColor: '#eee'
    },
    searchTitle: {
        paddingVertical: 5,
        paddingHorizontal: 20,
        color: '#999',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
    },
    footerText: {
        fontSize: 12,
        textAlign: 'center',
        paddingVertical: 5,
        borderTopColor: '#ccc',
        borderTopWidth: 1,
        color: '#999',
    },
    form: {
        width: '100%',
        paddingVertical: 15,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        elevation: 1
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        marginVertical: 8
    }
}
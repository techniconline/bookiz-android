import React from 'react';
import {Image, View, TouchableOpacity, StatusBar} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {AuthImage} from "../../resources/Image";
import {Text} from "../../components";
import styles from './AuthStyles';
import translate from '../../translate';

const Auth = () => {
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="black" barStyle="light-content" />
            <View style={styles.imgContainer}>
                <Image source={AuthImage} style={styles.img} />
                <View style={styles.circle} />
            </View>
            <View style={styles.selectAuth}>
                <TouchableOpacity onPress={()=>{ Actions.login(); }} style={styles.auth}>
                    <Text style={styles.authTxt}>{translate.sign_in.en}</Text>
                </TouchableOpacity>
                <View style={styles.line} />
                <TouchableOpacity onPress={()=>{ Actions.register(); }} style={styles.auth}>
                    <Text style={styles.authTxt}>{translate.sign_up.en}</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default Auth;
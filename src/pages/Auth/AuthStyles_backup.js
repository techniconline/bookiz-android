import {danger} from "../../resources/Colors";

export default {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 150,
        maxWidth: '70%',
        height: 100,
    },
    form: {
        width: 300,
        maxWidth: '90%',
        marginVertical: 15,
        padding: 15,
    },
    input: {
        borderWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'rgba(0,0,0,0.5)',
        borderRadius: 3,
        color: '#000',
        marginBottom: 10
    },
    errors: {
        backgroundColor: danger,
        borderRadius: 3,
        borderColor: 'rgba(0,0,0,0.2)',
        borderWidth: 1,
        paddingVertical: 5,
        paddingHorizontal: 8,
        marginBottom: 10
    },
    error: {
        color: '#fff',
        fontSize: 14
    },
    link: {
        width: '100%',
        marginBottom: 8,
    },
    linkTxt: {
        fontSize: 14,
        width: '100%',
        textAlign: 'center',
        color: '#fff'
    }
}
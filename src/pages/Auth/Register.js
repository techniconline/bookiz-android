import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import {Button, ImageIcon, Input, Navbar, Text} from "../../components";
import styles from './AuthStyles';
import {LoginImage} from "../../resources/Image";
import { on_register } from '../../redux/actions';
import {secondary} from "../../resources/Colors";
import {IconLeftArrow} from "../../resources/Icon";

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            connector: '',
            password: ''
        }
    }
    onSubmit() {
        const { first_name, last_name, connector, password } = this.state;
        const data = { first_name, last_name, connector, password };
        this.props.on_register(data);
    }
    renderButton() {
        return (this.props.loading) ? (
            <Button style={{ backgroundColor: secondary }}><ActivityIndicator color="#fff" size="small" animating /></Button>
        ) : (
            <Button title="Sign up" textStyle={{ color: '#fff'}} style={{ backgroundColor: secondary }} onPress={this.onSubmit.bind(this)} />
        );
    }
    renderError() {
        const { errors } = this.props;
        return (errors !== null) ? (
            <View style={styles.errors}>
                {_.map(errors, (error, key) => (<Text key={key} style={styles.error}>* {error[0]}</Text>))}
            </View>
        ) : null;
    }

    render() {
        const color = '#333';
        const placeholderColor = 'rgba(0,0,0,0.5)';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <ImageIcon source={IconLeftArrow} size={25} />
            </TouchableOpacity>
        );
        return (
            <View style={styles.container}>
                <Navbar style={styles.navbar} leftComponent={left} title="Sign in"/>
                <View style={styles.imgContainer}>
                    <Image source={LoginImage} style={styles.img}/>
                    <View style={styles.circle}/>
                </View>
                <View style={styles.loginFormContainer}>
                    <View style={styles.form}>
                        {this.renderError()}
                        <Input placeholder="Email"
                               type="email-address"
                               placeholderTextColor={placeholderColor}
                               containerStyle={styles.input}
                               style={{ color }}
                               value={this.state.connector}
                               leftIcon="envelope"
                               leftIconColor={color}
                               onChange={connector => { this.setState({ connector }); }} />
                        <Input placeholder="First Name"
                               placeholderTextColor={placeholderColor}
                               containerStyle={styles.input}
                               style={{ color }}
                               value={this.state.first_name}
                               leftIcon="user"
                               leftIconColor={color}
                               onChange={first_name => { this.setState({ first_name }); }} />
                        <Input placeholder="Last Name"
                               placeholderTextColor={placeholderColor}
                               containerStyle={styles.input}
                               style={{ color }}
                               value={this.state.last_name}
                               leftIcon="user"
                               leftIconColor={color}
                               onChange={last_name => { this.setState({ last_name }); }} />
                        <Input placeholder="Password"
                               placeholderTextColor={placeholderColor}
                               secureTextEntry
                               containerStyle={styles.input}
                               style={{ color }}
                               value={this.state.password}
                               leftIcon="key"
                               leftIconColor={color}
                               onChange={password => { this.setState({ password }); }} />
                    </View>
                    <View style={styles.btnContainer}>
                        {this.renderButton()}
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    const { loading, register_errors } = state.auth;
    return {
        errors: register_errors,
        loading
    }
};

export default connect(mapStateToProps, { on_register })(Register);
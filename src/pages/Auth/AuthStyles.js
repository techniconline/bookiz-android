import {danger, primary, secondary} from "../../resources/Colors";
import {Font_Regular} from "../../redux/types";

const color = '#666';

export default {
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: primary
    },
    navbar: {
        width: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: 'transparent',
        elevation: 0,
        zIndex: 10,
        borderBottomWidth: 0
    },
    circle: {
        position: 'absolute',
        bottom: -90,
        width: '180%',
        height: 375,
        borderBottomLeftRadius: 600,
        borderBottomRightRadius: 600,
        borderBottomWidth: 90,
        borderLeftWidth: 10,
        borderRightWidth: 10,
        borderColor: primary,
        zIndex: 10,
    },
    imgContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        position: 'relative',
        alignItems: 'center',
        overflow: 'hidden'
    },
    img: {
        flex: 1,
        width: '100%',
        height: '100%'
    },
    appName: {
        position: 'absolute',
        color,
        textShadowColor: '#000',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 5
    },
    selectAuth: {
        height: 65,
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'nowrap',
        elevation: 7
    },
    auth: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    authTxt: {
        width: '100%',
        fontFamily: Font_Regular,
        color,
        textAlign: 'center'
    },
    line: {
        width: 1,
        backgroundColor: 'rgba(0,0,0,0.2)',
        height: '100%'
    },
    loginFormContainer: {
        minHeight: 250,
        alignItems: 'center',
        flexWrap: 'nowrap',
        elevation: 7
    },
    form: {
        width: 300,
        maxWidth: '90%',
        marginVertical: 10,
        padding: 15,
    },
    input: {
        // borderWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: secondary,
        borderRadius: 3,
        color,
        marginBottom: 10
    },
    errors: {
        backgroundColor: danger,
        borderRadius: 3,
        borderColor: 'rgba(0,0,0,0.2)',
        borderWidth: 1,
        paddingBottom: 5,
        paddingHorizontal: 8,
        marginBottom: 10
    },
    error: {
        color: '#fff',
        fontSize: 14
    },
    link: {
        width: '100%',
        marginBottom: 8,
    },
    linkTxt: {
        fontSize: 14,
        width: '100%',
        textAlign: 'center',
        color
    },
    btnContainer: {
        width: 300,
        maxWidth: '90%',
        padding: 15,
    }
};
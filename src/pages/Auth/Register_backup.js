import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import {Button, Input, Text} from "../../components";
import styles from './AuthStyles';
import {BackgroundImg, Logo} from "../../resources/Image";
import { on_register } from '../../redux/actions';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            connector: '',
            password: ''
        }
    }
    onSubmit() {
        const { first_name, last_name, connector, password } = this.state;
        const data = { first_name, last_name, connector, password };
        this.props.on_register(data);
    }
    renderButton() {
        return (this.props.loading) ? (
            <Button><ActivityIndicator color="#fff" size="small" animating /></Button>
        ) : (
            <Button title="Register" onPress={this.onSubmit.bind(this)} />
        );
    }
    renderError() {
        const { errors } = this.props;
        console.log(errors);
        return (errors !== null) ? (
            <View style={styles.errors}>
                {_.map(errors, (error, key) => (<Text key={key} style={styles.error}>* {error[0]}</Text>))}
            </View>
        ) : null;
    }
    render() {
        return (
            <ImageBackground source={BackgroundImg} style={styles.container}>
                <Image source={Logo} style={styles.logo} resizeMode="contain" />
                <View style={styles.form}>
                    {this.renderError()}
                    <Input placeholder="Email"
                           type="email-address"
                           placeholderTextColor="rgba(0,0,0,0.5)"
                           containerStyle={styles.input}
                           value={this.state.connector}
                           leftIcon="envelope"
                           leftIconColor="#333"
                           onChange={connector => { this.setState({ connector }); }} />
                    <Input placeholder="First Name"
                           placeholderTextColor="rgba(0,0,0,0.5)"
                           containerStyle={styles.input}
                           value={this.state.first_name}
                           leftIcon="user"
                           leftIconColor="#333"
                           onChange={first_name => { this.setState({ first_name }); }} />
                    <Input placeholder="Last Name"
                           placeholderTextColor="rgba(0,0,0,0.5)"
                           containerStyle={styles.input}
                           value={this.state.last_name}
                           leftIcon="user"
                           leftIconColor="#333"
                           onChange={last_name => { this.setState({ last_name }); }} />
                    <Input placeholder="Password"
                           placeholderTextColor="rgba(0,0,0,0.5)"
                           secureTextEntry
                           containerStyle={styles.input}
                           value={this.state.password}
                           leftIcon="key"
                           leftIconColor="#333"
                           onChange={password => { this.setState({ password }); }} />
                    {this.renderButton()}
                </View>
                <TouchableOpacity style={styles.link} onPress={()=>{ Actions.pop(); }}>
                    <Text style={styles.linkTxt}>Login Account</Text>
                </TouchableOpacity>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => {
    const { loading, register_errors } = state.auth;
    return {
        errors: register_errors,
        loading
    }
};

export default connect(mapStateToProps, { on_register })(Register);
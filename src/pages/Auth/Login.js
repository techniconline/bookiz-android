import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import {Button, ErrorAlert, ImageIcon, Input, Navbar} from "../../components";
import { on_login } from '../../redux/actions';
import { LoginImage } from "../../resources/Image";
import styles from './AuthStyles';
import Icon from "react-native-vector-icons/FontAwesome5";
import {secondary} from "../../resources/Colors";
import {IconLeftArrow} from "../../resources/Icon";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            connector: '',
            password: ''
        }
    }
    onSubmit() {
        const { connector, password } = this.state;
        const data = { connector, password };
        this.props.on_login(data);
    }
    renderButton() {
        return (this.props.loading) ? (
            <Button style={{ backgroundColor: secondary }}><ActivityIndicator color="#fff" size="small" animating /></Button>
        ) : (
            <Button title="Sign in" textStyle={{ color: '#fff' }} style={{ backgroundColor: secondary }} onPress={this.onSubmit.bind(this)} />
        );
    }
    renderError() {
        const { errors } = this.props;
        return (errors !== null) ? <ErrorAlert errors={errors} />  : null;
    }
    render() {
        const color = '#333';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <ImageIcon source={IconLeftArrow} size={25} />
            </TouchableOpacity>
        );
        return (
            <View style={styles.container}>
                <Navbar style={styles.navbar} leftComponent={left} title="Sign in"/>
                <View style={styles.imgContainer}>
                    <Image source={LoginImage} style={styles.img}/>
                    <View style={styles.circle}/>
                </View>
                <View style={styles.loginFormContainer}>
                    <View style={styles.form}>
                        {this.renderError()}
                        <Input placeholder="Email"
                               type="email-address"
                               placeholderTextColor="rgba(0,0,0,0.5)"
                               containerStyle={styles.input}
                               style={{ color }}
                               value={this.state.connector}
                               leftIcon="envelope"
                               leftIconColor={color}
                               onChange={connector => { this.setState({ connector }); }} />
                        <Input placeholder="Password"
                               placeholderTextColor="rgba(0,0,0,0.5)"
                               secureTextEntry
                               containerStyle={styles.input}
                               style={{ color }}
                               value={this.state.password}
                               leftIcon="key"
                               leftIconColor={color}
                               onChange={password => { this.setState({ password }); }} />
                    </View>
                    <View style={styles.btnContainer}>
                        {this.renderButton()}
                    </View>
                </View>
            </View>
        );
    }
};

const mapStateToProps = state => {
    const { loading, login_errors } = state.auth;
    return {
        errors: login_errors,
        loading
    }
};

export default connect(mapStateToProps, { on_login })(Login);
import React, { Component } from 'react';
import { View, TouchableOpacity, ImageBackground, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import {Button, Input, Text} from "../../components";
import styles from './AuthStyles';
import {BackgroundImg, Logo} from "../../resources/Image";
import { on_login } from '../../redux/actions';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            connector: '',
            password: ''
        }
    }
    onSubmit() {
        const { connector, password } = this.state;
        const data = { connector, password };
        this.props.on_login(data);
    }
    renderButton() {
        return (this.props.loading) ? (
            <Button><ActivityIndicator color="#fff" size="small" animating /></Button>
        ) : (
            <Button title="Login" onPress={this.onSubmit.bind(this)} />
        );
    }
    renderError() {
        const { errors } = this.props;
        console.log(errors);
        return (errors !== null) ? (
            <View style={styles.errors}>
                {_.map(errors, (error, key) => (<Text key={key} style={styles.error}>* {error[0]}</Text>))}
            </View>
        ) : null;
    }
    render() {
        return (
            <ImageBackground source={BackgroundImg} style={styles.container}>
                <Image source={Logo} style={styles.logo} resizeMode="contain" />
                <View style={styles.form}>
                    {this.renderError()}
                    <Input placeholder="Email"
                           type="email-address"
                           placeholderTextColor="rgba(0,0,0,0.5)"
                           containerStyle={styles.input}
                           value={this.state.connector}
                           leftIcon="envelope"
                           leftIconColor="#333"
                           onChange={connector => { this.setState({ connector }); }} />
                    <Input placeholder="Password"
                           placeholderTextColor="rgba(0,0,0,0.5)"
                           secureTextEntry
                           containerStyle={styles.input}
                           value={this.state.password}
                           leftIcon="key"
                           leftIconColor="#333"
                           onChange={password => { this.setState({ password }); }} />
                    {this.renderButton()}
                </View>
                <TouchableOpacity style={styles.link} onPress={()=>{ Actions.push('register'); }}>
                    <Text style={styles.linkTxt}>Create Account</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.link} onPress={()=>{ Actions.push('forgot'); }}>
                    <Text style={styles.linkTxt}>Forgot Password?</Text>
                </TouchableOpacity>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => {
    const { loading, login_errors } = state.auth;
    return {
        errors: login_errors,
        loading
    }
};

export default connect(mapStateToProps, { on_login })(Login);
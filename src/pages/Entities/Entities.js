import React, { Component } from 'react';
import { connect } from 'react-redux';
import {TouchableOpacity, View, FlatList, ActivityIndicator} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Navbar, Searchbar} from "../../components";
import { get_entities } from '../../redux/actions';
import ItemEntity from './ItemEntity';
import styles from "./EntitiesStyles";
import {primary} from "../../resources/Colors";

class Entities extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: ''
        }
    }
    componentDidMount() {
        const {get_entities, category} = this.props;
        get_entities(category.id);
    }
    render() {
        const { category, loading } = this.props;
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 10 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <Icon color="#fff" size={20} name="arrow-left" solid />
            </TouchableOpacity>
        );
        return (
            <View style={{ flex: 1 }}>
                <Navbar title={category.title} leftComponent={left} />
                <Searchbar value={this.state.search} onChange={(search) => { this.setState({ search }); }}/>
                {(loading) ? (<View style={styles.fullLoading}><ActivityIndicator size="large" color={primary} animating /></View>): (
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={this.props.data}
                            renderItem={({item}) => <ItemEntity data={item} />}
                        />
                    </View>
                )}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.entity.data,
        loading: state.entity.loading
    }
};

export default connect(mapStateToProps, { get_entities })(Entities);
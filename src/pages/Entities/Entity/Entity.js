import React, {Component, Fragment} from 'react';
import {View, ScrollView, TouchableOpacity, ToastAndroid, ActivityIndicator} from 'react-native';
import { connect } from 'react-redux';
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";
import {Button, ErrorPage, FullLoading, ImageIcon, Navbar, Text} from "../../../components";
import { get_entity, on_like_entity } from '../../../redux/actions';
import styles from "./EntityStyles";
import Services from "./Services";
import Comments from "./Comments";
import Info from "./Info";
import MainSlider from "../../Home/MainSlider";
import translate from '../../../translate';
import EntityMap from "./EntityMap";
import Loading from "../../../Loading";
import {
    IconLeftArrow,
    IconLeftArrowWhite,
    IconLike,
    IconLikeActive,
    IconMapMarkerCubeWhite
} from "../../../resources/Icon";

class Entity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: 0,
            set_bookmark: false,
            bookmark: false,
            scroll: null
        }
    }
    componentDidMount() {
        this.props.get_entity(this.props.item);
    }
    componentDidUpdate() {
        if(!this.state.set_bookmark && !this.props.loading && this.props.data !== null) {
            this.setState({ set_bookmark: true, bookmark: this.props.data.liked.liked });
        }
    }
    renderContent() {
        const { loading, data, errors, direction, lang } = this.props;
        const rtl = direction === 'rtl';
        const { activeTab } = this.state;
        return (activeTab === 0) ? (
            <View style={{ flex: 1 }}>
                <Services
                    onChangeScroll={(y)=>{ this.scroll.scrollTo({x: 0, y , animated: true}); }}
                    rtl={rtl}
                    scroll={this.state.scroll}
                    lang={lang}
                    entity={{...this.props.data, id: this.props.item.id}} />
            </View>
        ) : (activeTab === 1) ? (
            <View style={{ flex: 1 }}>
                <Info data={data} rtl={rtl} lang={lang} />
            </View>
        ) : (activeTab === 2 && data.entity_address !== null) ? (
            <View style={{ flex: 1 }}>
                <EntityMap data={data} rtl={rtl} lang={lang} />
            </View>
        ) : (
            <View style={{ flex: 1 }}>
                <Comments entity={data} rtl={rtl} lang={lang} />
            </View>
        );
    }
    onBookmark() {
        const { set_bookmark, bookmark } = this.state;
        if(set_bookmark) {
            const id = this.props.data.id;
            this.props.on_like_entity(id, !bookmark);
            this.setState({ bookmark: !bookmark });
        }else {
            ToastAndroid.showWithGravity('Please Wait To Get Data.', ToastAndroid.SHORT, ToastAndroid.TOP);
        }
    }
    render() {
        const { loading, data, basket_count, lang, direction, errors } = this.props;
        const { bookmark, show_nav } = this.state;
        const rtl = direction === 'rtl';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                {/*<Icon color={(show_nav) ? "#333" : '#fff'} size={20} name="arrow-left" solid />*/}
                <ImageIcon source={show_nav ? IconLeftArrow : IconLeftArrowWhite } size={25} />
            </TouchableOpacity>
        );
        const right = (
            <View style={styles.rightNav}>
                <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} onPress={this.onBookmark.bind(this)}>
                    {/*<Icon name="heart" solid={this.state.bookmark} color="#e53935" size={22} />*/}
                    <ImageIcon source={bookmark ? IconLikeActive : IconLike } size={25} />
                </TouchableOpacity>
                {/*<TouchableOpacity style={styles.basket} activeOpacity={0.6} onPress={() => { Actions.basket(); }}>*/}
                    {/*<Icon color={(show_nav) ? "#333" : '#fff'} size={22} name="shopping-basket" solid />*/}
                    {/*<Text style={styles.basketCount}>{basket_count}</Text>*/}
                {/*</TouchableOpacity>*/}
            </View>
        );
        const styleCaption = {
            imgInfo: {
                width: '100%'
            },
            title: {
                width: '100%',
                fontSize: (rtl) ? 20 : 22,
                color: '#fff',
                paddingHorizontal: 15,
                textShadowColor: 'rgba(0, 0, 0, 0.6)',
                textShadowOffset: {width: 0, height: 1},
                textShadowRadius: 1
            },
            mapWrapper: {
                width: '100%',
                flexDirection: (rtl) ? 'row' : 'row-reverse',
                opacity: (data !== null && data.entity_address !== null) ? 1 : 0
            },
            mapContainer: {
                flexDirection: (rtl) ? 'row' : 'row-reverse',
                paddingVertical: 5,
                paddingHorizontal: 15,
                paddingBottom: 15
            },
            mapTxt: {
                fontSize: (rtl) ? 16 : 18,
                color: '#fff',
                marginHorizontal: 5
            }
        };
        // const tabBarStyles = { ...styles.tabBar, width: (data !== null && data.entity_address !== null) ? '25%' : '33%' };
        const tabBarStyles = styles.tabBar;
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <Navbar leftComponent={left} rightComponent={right} style={(show_nav) ? styles.navbar : styles.navbar_transparent} />
                {(loading) ? <Loading/> : (errors) ? <ErrorPage rtl={rtl} errors={errors} />
                    : (data !== null) ? (
                        <Fragment>
                            <ScrollView ref={scroll => { this.scroll = scroll}}
                                        onScroll={(e) => { this.setState({ show_nav: (e.nativeEvent.contentOffset.y > 250), scroll: e.nativeEvent.contentOffset.y });  }}>
                                <View style={styles.cover}>
                                    <MainSlider configs={{ dots: false, animate: false }}
                                                height="100%"
                                                data={(data.entity_slider_medias.length > 0) ? data.entity_slider_medias : [data.media_image]}
                                                caption={(
                                                    <Fragment>
                                                        <View style={styleCaption.imgInfo}>
                                                            <Text rtl={rtl} style={styleCaption.title}>{data.title}</Text>
                                                        </View>
                                                        <View style={styleCaption.mapWrapper}>
                                                            <TouchableOpacity style={styleCaption.mapContainer} onPress={() => { Actions.push('entityMap', { data, lang, rtl }); }} >
                                                                <ImageIcon source={IconMapMarkerCubeWhite} size={22} />
                                                                <Text rtl={rtl} style={styleCaption.mapTxt}>{translate.show_map[lang]}</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </Fragment>
                                                )}
                                    />
                                </View>
                                <View style={(rtl) ? styles.rtl_tabBars : styles.tabBars}>
                                    <TouchableOpacity style={[tabBarStyles, (this.state.activeTab === 0)?styles.activeTab:null]} onPress={()=>{this.setState({ activeTab: 0 })}}>
                                        <Text rtl={rtl} style={(this.state.activeTab === 0)?styles.activeTabTxt:styles.tabBarTxt} >{translate.services[lang]}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[tabBarStyles, (this.state.activeTab === 1)?styles.activeTab:null]} onPress={()=>{this.setState({ activeTab: 1 })}}>
                                        <Text rtl={rtl} style={(this.state.activeTab === 1)?styles.activeTabTxt:styles.tabBarTxt} >{translate.info[lang]}</Text>
                                    </TouchableOpacity>
                                    {/*{(data.entity_address !== null) ? (*/}
                                        {/*<TouchableOpacity style={[tabBarStyles, (this.state.activeTab === 2)?styles.activeTab:null]} onPress={()=>{this.setState({ activeTab: 2 })}}>*/}
                                            {/*<Text rtl={rtl} style={(this.state.activeTab === 2)?styles.activeTabTxt:styles.tabBarTxt} >{translate.map[lang]}</Text>*/}
                                        {/*</TouchableOpacity>*/}
                                    {/*) : null}*/}
                                    <TouchableOpacity style={[tabBarStyles, (this.state.activeTab === 3)?styles.activeTab:null]} onPress={()=>{this.setState({ activeTab: 3 })}}>
                                        <Text rtl={rtl} style={(this.state.activeTab === 3)?styles.activeTabTxt:styles.tabBarTxt} >{translate.comments[lang]}</Text>
                                    </TouchableOpacity>
                                </View>
                                {this.renderContent()}
                            </ScrollView>
                            {(basket_count > 0) ? (
                                <View style={styles.basketContainer}>
                                    <Text style={styles.basketText} rtl={rtl}>{(basket_count > 1 && rtl) ? basket_count+' '+translate.services[lang] : basket_count+' '+translate.service[lang]} {translate.added[lang]}</Text>
                                    <Button title={translate.go_basket[lang]}
                                            textStyle={styles.basketBtnText}
                                            style={styles.basketBtn}
                                            rtl={rtl}
                                            onPress={()=>{ Actions.push('basket'); }} />
                                </View>
                            ) : null}
                        </Fragment>
                    ) : null}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.entity.single_loading,
        errors: state.entity.single_errors,
        data: state.entity.single_data,
        basket_count: state.basket.counter,
        direction: state.setting.direction,
        lang: state.setting.lang,
    }
};

export default connect(mapStateToProps, { get_entity, on_like_entity })(Entity)
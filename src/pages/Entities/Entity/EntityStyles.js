import { Dimensions } from 'react-native';
import {blue, gray, primary, secondary, success} from "../../../resources/Colors";

export default {
    fullLoading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    navbar_transparent: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        backgroundColor: 'transparent',
        elevation: 0,
        borderBottomWidth: 0,
        zIndex: 100
    },
    navbar: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        zIndex: 100
    },
    cover: {
        flex: 1,
        position: 'relative',
        width: '100%',
        height: 300,
    },
    imgInfo: {
        width: '100%',
        backgroundColor: '#fff'
    },
    title: {
        width: '100%',
        fontSize: 22,
        color: '#fff',
        paddingHorizontal: 15,
        textShadowColor: 'rgba(0, 0, 0, 0.6)',
        textShadowOffset: {width: 0, height: 0},
        textShadowRadius: 1
    },
    openTime: {
        width: '100%',
        fontSize: 15,
        color: '#fff',
        paddingHorizontal: 15,
        paddingBottom: 10,
        textShadowColor: 'rgba(0, 0, 0, 0.6)',
        textShadowOffset: {width: 0, height: 0},
        textShadowRadius: 1
    },
    tabBars: {
        position: 'relative',
        flexDirection: 'row',
        width: '100%',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
        elevation: 3
    },
    rtl_tabBars: {
        position: 'relative',
        flexDirection: 'row-reverse',
        width: '100%',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
        elevation: 3
    },
    tabBar: {
        width: '33%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    activeTab: {
        borderBottomWidth: 2,
        borderBottomColor: secondary
    },
    activeTabTxt: {
        color: secondary
    },
    tabBarTxt: {
        color: gray
    },
    rightNav: {
        flexDirection: 'row-reverse',
    },
    basket: {
        position: 'relative',
        paddingHorizontal: 10
    },
    basketCount: {
        fontSize: 14,
        position: 'absolute',
        top: -2,
        left: -2,
        width: 20,
        height: 20,
        color: '#fff',
        backgroundColor: blue,
        borderRadius: 30,
        borderColor: 'rgba(0,0,0,0.1)',
        borderWidth: 1,
        textAlign: 'center'
    },
    basketContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 7,
        paddingHorizontal: 15,
        borderTopWidth: 1,
        borderTopColor: '#eee'
    },
    basketText: {
        width: '50%',
        maxWidth: Dimensions.get('window').width - 200,
        color: secondary,
        paddingHorizontal: 10
    },
    basketBtn: {
        width: 150,
        paddingVertical: 10,
        backgroundColor: secondary
    },
    basketBtnText: {
        fontSize: 15,
        color: '#fff'
    }
}
import React, {Component, Fragment} from 'react';
import { View, TouchableOpacity, Linking } from 'react-native';
import {Button, Text} from '../../../components';
import Icon from "react-native-vector-icons/FontAwesome5";
import Mapbox from '@mapbox/react-native-mapbox-gl';
import {success} from "../../../resources/Colors";
import {Font_Bold, Font_Medium, MAPBOX_API, Rtl_Font_Medium, Rtl_Font_Regular} from "../../../redux/types";
import call from 'react-native-phone-call';
import HTML from 'react-native-render-html';
import translate from '../../../translate';
import _ from 'lodash';

Mapbox.setAccessToken(MAPBOX_API);

class Info extends Component{
    constructor(props) {
        super(props);
    }
    socialColor(type) {
        let color = '#24334A';
        switch(type) {
            case 'email':
                color = '#f44336';
                break;
            case 'instagram':
                color = '#9575CD';
                break;
            case 'telegram':
                color = '#2196F3';
                break;
            case 'twitter':
                color = '#03A9F4';
                break;
            case 'facebook':
                color = '#1976D2';
                break;
        }
        return color;
    }
    socialIcon(type) {
        let name = 'share-alt';
        // const color = this.socialColor(type);
        const color = '#fff';
        let size = 25;
        switch(type) {
            case 'email':
                name = 'envelope';
                break;
            case 'instagram':
                name = 'instagram';
                break;
            case 'telegram':
                name = 'telegram-plane';
                break;
            case 'twitter':
                name = 'twitter';
                break;
            case 'facebook':
                name = 'facebook';
                break;
        }
        return <Icon name={name} color={color} size={size} />;
    }
    openSocial(url) {
        Linking.canOpenURL(url)
            .then((supported) => {
                if (!supported) {
                    console.log("Can't handle url: " + url);
                } else {
                    return Linking.openURL(url);
                }
            })
            .catch((err) => console.error('An error occurred', err));
    }
    render() {
        const { data, lang, rtl } = this.props;
        const { entity_address, description, entity_contacts } = data;
        return (
            <View style={{ flex: 1, alignItems: 'center', marginBottom: 15 }}>
                {/*<Text rtl={rtl} style={styles.sectionTitle}>{translate.times[lang]}</Text>*/}
                {/*<View style={styles.times}>*/}
                    {/*<View style={(rtl) ? styles.rtl_timeItem : styles.timeItem}>*/}
                        {/*<View style={rtl ? styles.rtl_dayContainer : styles.dayContainer}>*/}
                            {/*<Icon color="#999" size={15} name="calendar" solid />*/}
                            {/*<Text rtl={rtl} style={rtl ? styles.rtl_day : styles.day}>Sunday</Text>*/}
                        {/*</View>*/}
                        {/*<Text rtl={rtl}>8:00 - 17:00</Text>*/}
                    {/*</View>*/}
                {/*</View>*/}
                {(description === null && entity_address === null) ? <Text rtl={rtl} style={styles.sectionTitle}>{translate.empty_date[lang]}</Text> : null}
                {(description !== null) ? (
                    <Fragment>
                        <Text rtl={rtl} style={styles.sectionTitle}>{translate.description[lang]}</Text>
                        <View style={styles.description}>
                            <HTML tagsStyles={rtl ? styles.rtl_tagsStyles : styles.tagsStyles} html={description} />
                        </View>
                    </Fragment>
                ) : null}

                {(entity_address !== null) ? (
                    <Fragment>
                        <Text rtl={rtl} style={styles.sectionTitle}>{translate.address[lang]}</Text>
                        <View style={styles.addressContainer}>
                            {/*<Mapbox.MapView*/}
                                {/*attributionEnabled={false}*/}
                                {/*logoEnabled={false}*/}
                                {/*styleURL={Mapbox.StyleURL.Street}*/}
                                {/*zoomLevel={15}*/}
                                {/*centerCoordinate={[Number(entity_address.longitude), Number(entity_address.latitude)]}*/}
                                {/*style={styles.map}>*/}
                                {/*<Mapbox.PointAnnotation id={'entity_'+id} title={title} coordinate={[Number(entity_address.longitude), Number(entity_address.latitude)]}>*/}
                                    {/*<Mapbox.Callout title={title} />*/}
                                {/*</Mapbox.PointAnnotation>*/}
                            {/*</Mapbox.MapView>*/}
                            <Text rtl={rtl} style={styles.address}>{entity_address.address}</Text>
                        </View>
                    </Fragment>
                ) : null}

                    {(entity_contacts.length > 0) ? (
                        <Fragment>
                            <Text rtl={rtl} style={styles.sectionTitle}>{translate.social_media[lang]}</Text>
                            <View style={styles.socials}>
                                {_.map(entity_contacts, (contact, key) => (
                                    <TouchableOpacity key={key}
                                                      style={{...styles.social, backgroundColor: this.socialColor(contact.media_type)}}
                                                      onPress={this.openSocial.bind(this,contact.media_link)}>
                                        {this.socialIcon(contact.media_type)}
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </Fragment>
                    ) : null }
                {/*<Button style={styles.callBtn} onPress={() => { call({number: '9093900003', prompt: false}); }}>*/}
                    {/*<Icon color="#fff" size={18} name="phone" solid />*/}
                    {/*<Text rtl={rtl} style={rtl ? styles.rtl_callText : styles.callText}>{translate.call_entity[lang].toUpperCase()}</Text>*/}
                {/*</Button>*/}
            </View>
        )
    }
}

const styles = {
    sectionTitle: {
        width: '100%',
        fontSize: 14,
        color: '#999',
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    times: {
        width: '100%',
        backgroundColor: '#fff',
        // borderTopWidth: 1,
        // borderBottomWidth: 1,
        // borderColor: '#ccc',
        padding: 10
    },
    timeItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        borderBottomWidth: 1,
        borderColor: '#eee',
        paddingVertical: 10,
        paddingHorizontal: 5
    },
    rtl_timeItem: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        borderBottomWidth: 1,
        borderColor: '#eee',
        paddingVertical: 10,
        paddingHorizontal: 5
    },
    dayContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rtl_dayContainer: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
    },
    day: {
        color: '#333',
        marginLeft: 7
    },
    rtl_day: {
        color: '#333',
        marginRight: 7
    },
    description: {
        width: '100%',
        backgroundColor: '#fff',
        // borderTopWidth: 1,
        // borderBottomWidth: 1,
        // borderColor: '#ccc',
        paddingHorizontal: 15
    },
    addressContainer: {
        width: '100%',
        backgroundColor: '#fff',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#ccc'
    },
    map: {
        width: '100%',
        height: 200
    },
    address: {
        fontSize: 16,
        lineHeight: 25,
        color: '#666',
        paddingVertical: 5,
        paddingHorizontal: 15,
    },
    callBtn: {
        width: '90%',
        marginVertical: 15,
        backgroundColor: success,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    callText: {
        fontFamily: Font_Bold,
        fontSize: 18,
        color: '#fff',
        marginLeft: 8
    },
    rtl_callText: {
        fontFamily: Rtl_Font_Medium,
        fontSize: 16,
        color: '#fff',
        marginLeft: 8
    },
    socials: {
        flexDirection: 'row',
        marginVertical: 10,
    },
    social: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        // borderWidth: 1,
        marginHorizontal: 7
    },
    tagsStyles: {
        h1: { width: '100%', textAlign: 'left', fontFamily: Font_Medium },
        h2: { width: '100%', textAlign: 'left', fontFamily: Font_Medium },
        h3: { width: '100%', textAlign: 'left', fontFamily: Font_Medium },
        h4: { width: '100%', textAlign: 'left', fontFamily: Font_Medium },
        h5: { width: '100%', textAlign: 'left', fontFamily: Font_Medium },
        p: { color: '#666', width: '100%', textAlign: 'left', fontFamily: Font_Medium, fontSize: 16 },
        em: { width: '100%', textAlign: 'left', fontFamily: Font_Medium },
        li: { width: '100%', textAlign: 'left', fontFamily: Font_Medium },
        a: { width: '100%', textAlign: 'left', fontFamily: Font_Medium }
    },
    rtl_tagsStyles: {
        h1: { direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Medium },
        h2: {  direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Medium },
        h3: {  direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Medium },
        h4: {  direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Medium },
        h5: {  direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Medium },
        p: { color: '#666',  direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Regular, fontSize: 16 },
        em: {  direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Regular },
        li: {  direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Regular },
        a: {  direction: 'rtl', width: '100%', textAlign: 'right', fontFamily: Rtl_Font_Regular }
    }
};


export default Info;
import React, { Component } from "react";
import {TouchableOpacity, View} from 'react-native';
import Mapbox from "@mapbox/react-native-mapbox-gl";
import getDirections from 'react-native-google-maps-directions';
import { showLocation } from 'react-native-map-link';
import {MAPBOX_API} from "../../../redux/types";
import {Navbar} from "../../../components/Layouts";
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";
import {ImageIcon} from "../../../components/Elements";
import {IconLeftArrow, IconMapMarkerCircleWhite} from "../../../resources/Icon";
import {secondary} from "../../../resources/Colors";
Mapbox.setAccessToken(MAPBOX_API);

class EntityMap extends Component {
    constructor(props) {
        super(props);
    }
    handleGetDirections() {
        showLocation({
            latitude: this.props.data.entity_address.latitude,
            longitude: this.props.data.entity_address.longitude,
            // sourceLatitude: -8.0870631,  // optionally specify starting location for directions
            // sourceLongitude: -34.8941619,  // not optional if sourceLatitude is specified
            title: this.props.data.title,  // optional
            googleForceLatLon: false,  // optionally force GoogleMaps to use the latlon for the query instead of the title
            googlePlaceId: 'ChIJGVtI4by3t4kRr51d_Qm_x58',  // optionally specify the google-place-id
            alwaysIncludeGoogle: true, // optional, true will always add Google Maps to iOS and open in Safari, even if app is not installed (default: false)
            // dialogTitle: 'This is the dialog Title', // optional (default: 'Open in Maps')
            // dialogMessage: 'This is the amazing dialog Message', // optional (default: 'What app would you like to use?')
            // cancelText: 'This is the cancel button text', // optional (default: 'Cancel')
            // appsWhiteList: ['google-maps', 'waze'] // optionally you can set which apps to show (default: will show all supported apps installed on device)
            // app: 'uber'  // optionally specify specific app to use
        })
    }
    // handleGetDirections() {
    //     const data = {
    //         // source: {
    //         //     latitude: -33.8356372,
    //         //     longitude: 18.6947617
    //         // },
    //         destination: {
    //             latitude: this.props.data.entity_address.latitude,
    //             longitude: this.props.data.entity_address.longitude
    //         },
    //         params: [
    //             {
    //                 key: "travelmode",
    //                 value: "driving"        // may be "walking", "bicycling" or "transit" as well
    //             },
    //             {
    //                 key: "dir_action",
    //                 value: "navigate"       // this instantly initializes navigation using the given travel mode
    //             }
    //         ]
    //         // waypoints: [
    //         //     {
    //         //         latitude: -33.8600025,
    //         //         longitude: 18.697452,
    //         //     },
    //         //     {
    //         //         latitude: -33.8600026,
    //         //         longitude: 18.697453,
    //         //     },
    //         //     {
    //         //         latitude: -33.8600036,
    //         //         longitude: 18.697493,
    //         //     },
    //         //     {
    //         //         latitude: -33.8600046,
    //         //         longitude: 18.69743,
    //         //     },
    //         // ]
    //     };
    //
    //     getDirections(data)
    // }
    render() {
        const { data, lang, rtl } = this.props;
        const { id, title, entity_address, description } = data;
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <ImageIcon source={IconLeftArrow} size={25} />
            </TouchableOpacity>
        );
        return (
            <View style={styles.container}>
                <Navbar title={title} leftComponent={left} />
                <Mapbox.MapView
                    attributionEnabled={false}
                    logoEnabled={false}
                    styleURL={Mapbox.StyleURL.Street}
                    zoomLevel={15}
                    centerCoordinate={[Number(entity_address.longitude), Number(entity_address.latitude)]}
                    style={{ flex: 1 }}>
                    <Mapbox.PointAnnotation id={'entity_'+id} title={title} coordinate={[Number(entity_address.longitude), Number(entity_address.latitude)]}>
                        <Mapbox.Callout title={title} />
                    </Mapbox.PointAnnotation>
                </Mapbox.MapView>
                <TouchableOpacity activeOpacity={0.7}
                                  onPress={this.handleGetDirections.bind(this)}
                                  style={styles.direction}><ImageIcon source={IconMapMarkerCircleWhite} size={25} /></TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        position: 'relative'
    },
    direction: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
        height: 50,
        backgroundColor: secondary,
        position: 'absolute',
        bottom: 25,
        left: 25,
        borderRadius: 60,
        elevation: 3,
        zIndex: 100
    }
};

export default EntityMap;
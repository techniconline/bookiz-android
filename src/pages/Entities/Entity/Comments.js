import React, { Component} from 'react';
import { View, Image } from 'react-native';
import {ErrorPage, FullLoading, Text, format_date, Stars} from '../../../components';
import { connect } from 'react-redux';
import _ from 'lodash';
import styles from './CommentsStyles';
import translate from '../../../translate';

class Comments extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { entity, loading, data, errors, rtl, lang} = this.props;
        return (
            <View style={{ flex: 1, alignItems: 'center' }}>
                <Text rtl={rtl} style={styles.sectionTitle}>{translate.rate[lang]}</Text>
                <View style={rtl ? styles.rtl_rateContainer : styles.rateContainer}>
                    <View style={styles.rateNum}>
                        <Text rtl={rtl} style={styles.rate}>{entity.rate.data_rate}</Text>
                        {(loading) ? null : <Text rtl={rtl}  style={styles.commentsCount}>{data.length} {translate.comments[lang]}</Text> }
                    </View>
                    <View style={styles.rateDetails}>
                        {_.map(entity.rate_others, (item, key) => {
                            return (
                                <View key={key} style={rtl ? styles.rtl_rateItem : styles.rateItem}>
                                    <Text rtl={rtl} style={styles.rateTxt}>{item.options.title}</Text>
                                    <View style={styles.line} />
                                    <Stars rtl={rtl} value={item.values.rate} />
                                </View>
                            )
                        })}
                    </View>
                </View>
                <Text rtl={rtl} style={styles.sectionTitle}>{translate.comments_recently[lang]}</Text>
                {(loading) ? <FullLoading /> : (errors) ? <ErrorPage errors={errors} /> : (
                    <View style={styles.comments}>
                        {(data.length > 0 ) ? _.map(data, (comment, key) => (
                            <View style={styles.comment} key={key}>
                                <View style={rtl ? styles.rtl_commentInfo : styles.commentInfo}>
                                    <View style={rtl ? styles.rtl_imgContainer : styles.imgContainer}>
                                        <Image source={{ uri: 'https://www.sigmanest.com/wp-content/uploads/2017/11/Dummy.jpg' }} style={styles.img} />
                                    </View>
                                    <View>
                                        <Text rtl={rtl} style={rtl ? styles.rtl_username : styles.username}>{comment.name}</Text>
                                        <Text rtl={rtl} style={styles.date}>{format_date(comment.created_at)}</Text>
                                    </View>
                                </View>
                                <Text style={styles.commentContent}>{comment.comment}</Text>
                            </View>
                        )) : <Text rtl={rtl} style={styles.emptyComments}>{translate.empty_date[lang]}</Text>}
                    </View>
                )}
            </View>
        )
    }
}


const mapStateToProps = state => {
    const { data, loading, errors } = state.entity_comment;
    return { data, loading, errors };
};

export default connect(mapStateToProps, null)(Comments);
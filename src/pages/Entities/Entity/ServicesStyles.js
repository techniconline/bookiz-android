import { Dimensions } from 'react-native';
import {Font_Medium, Font_Regular, Rtl_Font_Medium} from "../../../redux/types";
import {blue, danger, secondary, success, warning} from "../../../resources/Colors";

export default {
    service_loading: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: 'rgba(0,0,0,0.1)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    servicesTitle: {
        width: '100%',
        fontFamily: Font_Medium,
        fontSize: 20,
        color: '#333',
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginBottom: 10
    },
    rtl_servicesTitle: {
        width: '100%',
        fontFamily: Rtl_Font_Medium,
        fontSize: 20,
        color: '#333',
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginBottom: 10
    },
    rtl_categories: {
        minWidth: Dimensions.get('window').width,
        flexDirection: 'row-reverse',
        paddingHorizontal: 10
    },
    categories: {
        flexDirection: 'row',
        paddingHorizontal: 10
    },
    category: {
        marginHorizontal: 5,
        borderRadius: 20,
        paddingVertical: 2,
        paddingHorizontal: 20,
        backgroundColor: '#eee'
    },
    selected_category: {
        marginHorizontal: 5,
        borderRadius: 20,
        paddingVertical: 2,
        paddingHorizontal: 20,
        backgroundColor: secondary
    },
    categoryTxt: {
        fontSize: 12,
        color: secondary
    },
    sectionTitle: {
        width: '100%',
        fontFamily: Font_Medium,
        fontSize: 22,
        color: '#333',
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    rtl_sectionTitle: {
        width: '100%',
        fontFamily: Rtl_Font_Medium,
        fontSize: 18,
        color: '#333',
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 15
        // elevation: 3
    },
    item: {
        position: 'relative',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        paddingBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    rtl_item: {
        position: 'relative',
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        paddingBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    title: {
        fontSize: 15,
    },
    meta: {
        fontFamily: Font_Regular,
        fontSize: 14,
        color: '#999'
    },
    info: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    rtl_info: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    data: {
        flex: 1,
        maxWidth: 150,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    rtl_data: {
        flex: 1,
        maxWidth: 150,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center'
    },
    prices: {
        width: 90,
        justifyContent: 'center',
        alignItems: 'center'
    },
    oldPrice: {
        fontSize: 13,
        width: '100%',
        color: danger,
        textAlign: 'center',
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    rtl_oldPrice: {
        fontSize: 11,
        width: '100%',
        color: danger,
        textAlign: 'center',
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    newPrice: {
        width: '100%',
        fontSize: 16,
        textAlign: 'center',
        color: secondary
    },
    rtl_newPrice: {
        width: '100%',
        fontSize: 14,
        textAlign: 'center',
        color: secondary
    },
    cartTxt: {
        color: '#fff'
    },
    select: {
        flex: 1,
        maxWidth: 30,
        height: 30,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10
    },
    rtl_select: {
        flex: 1,
        maxWidth: 30,
        height: 30,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0,
        marginLeft: 10
    }
};
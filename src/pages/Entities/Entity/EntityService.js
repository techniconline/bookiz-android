import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from "./ServicesStyles";
import {Alert, TouchableOpacity, View, ActivityIndicator} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import {Text} from "../../../components/Elements";
import {on_basket, clean_basket} from "../../../redux/actions";
import {secondary} from "../../../resources/Colors";

class EntityService extends Component {
    constructor(props) {
        super(props);
    }
    removeBasket() {
        const { add_loading, on_basket, service, entity } = this.props;
        if(add_loading.indexOf(service.id+'_'+entity.id) === -1)
            on_basket(service.id, entity);
    }
    addBasket() {
        const { on_basket, add_loading, basket_count, entity_basket, entity, service} = this.props;
        if(entity_basket !== null && entity_basket.id !== entity.id && basket_count !== 0) {
            Alert.alert(
                'Basket Problem',
                'You added services from other salon. \n If you want to add this service please clean your cart first',
                [
                    {text: 'Clean Cart', onPress: () => { this.props.clean_basket(); }},
                    {text: 'Cancel'},
                    {text: 'Clean & Add', onPress: () => { this.props.clean_basket(); setTimeout(()=>{on_basket(service, entity)}, 10); }},
                ],
            );
        }else {
            if(add_loading.indexOf(service.id+'_'+entity.id) === -1)
                on_basket(service.id, entity);
        }
    }
    render() {
        const { basket, service, rtl, index, length, add_loading, entity } = this.props;
        const cart_loading = add_loading.indexOf(service.id+'_'+entity.id) !== -1;
        return (
            <TouchableOpacity activeOpacity={(cart_loading) ? 1 : 0.7}
                              onPress={(basket.indexOf(service.id) > -1) ? this.removeBasket.bind(this) : this.addBasket.bind(this)}
                              style={[rtl ? styles.rtl_item: styles.item, (index === length-1) ? { borderBottomWidth: 0 } : null]}>
                {(basket.indexOf(service.id) > -1) ? (
                    <View style={[rtl ? styles.rtl_select : styles.select, { borderColor: secondary}]}><Icon color={secondary} size={12} name="check" solid /></View>
                ) : (
                    <View style={rtl ? styles.rtl_select : styles.select} />
                )}
                {(cart_loading) ? <View style={styles.service_loading}><ActivityIndicator size="large" color="#333" /></View> : null }
                <View style={rtl ? styles.rtl_info : styles.info}>
                    <Text rtl={rtl} style={styles.title}>{service.title}</Text>
                </View>
                <View style={rtl ? styles.rtl_data : styles.data}>
                    <View style={styles.prices}>
                        {(service.discount_amount !== 0) ?
                            <Text style={(rtl) ? styles.rtl_oldPrice : styles.oldPrice}>{service.discount_text}</Text> : null}
                        <Text style={(rtl) ? styles.rtl_newPrice : styles.newPrice}>{service.amount_text}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const mapStateToProps = state => {
    return {
        add_loading: state.basket.add_loading,
        basket: state.basket.data,
        basket_count: state.basket.counter,
        entity_basket: state.basket.entity,
    }
};

export default connect(mapStateToProps, { on_basket, clean_basket})(EntityService);
import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {View, ScrollView, TouchableOpacity} from 'react-native';
import {FullLoading, Text} from '../../../components';
import styles from './ServicesStyles';
import EntityService from "./EntityService";
import translate from '../../../translate';

class Services extends Component{
    constructor(props) {
        super(props);
        this.state = {
            scrolled: false,
            set_category: false,
            select_category: null,
            serviceOffset: {},
            serviceOffsetArr: []
        }
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.scroll !== null) {
            let select = null;
            _.map(this.state.serviceOffsetArr, (value, index) => {
                if(nextProps.scroll > value.offset - this.state.serviceOffsetArr[0].offset) select = value.id;
            });
            // if(this.state.select_category !== Number(index)+1) this.setState({ select_category: Number(index)+1 });
            if(this.state.select_category !== select && select !== null) this.setState({ select_category: select });
        }
    }
    // componentDidUpdate(props, state) {
    //     if(!this.props.loading && !this.state.set_category) {
    //         this.setState({ select_category: (this.props.services.length > 0) ? this.props.services[0].id : null, ser_category: true });
    //     }
    // }
    onSelectCategory(item) {
        this.setState({ select_category: item.id });
        this.props.onChangeScroll(this.state.serviceOffset[item.id]);
    }
    onScroll(scroll) {
        if(!this.state.scrolled) {
            this.scroll = scroll;
            if (this.props.rtl){
                setTimeout(() => {
                    if (this.scroll) {
                        this.scroll.scrollToEnd();
                        this.setState({scrolled: true})
                    }
                }, 0);
            } else {
                this.setState({scrolled: true})
            }
        }
    }
    render() {
        const { loading, services, rtl, entity, lang } = this.props;
        return (loading) ? <FullLoading color="#333" /> :
            <Fragment>
                <Text rtl={rtl} style={rtl ? styles.rtl_servicesTitle : styles.servicesTitle}>{translate.select_services[lang]}</Text>
                <ScrollView horizontal ref={this.onScroll.bind(this)} showsHorizontalScrollIndicator={false}>
                    <View style={rtl ? styles.rtl_categories : styles.categories}>
                        {_.map(services, (main_service, key) => { return (main_service.entity_relation_services.length > 0) ? (
                            <TouchableOpacity style={(this.state.select_category === main_service.id) ? styles.selected_category : styles.category} key={key} activeOpacity={0.7} onPress={this.onSelectCategory.bind(this, main_service)}>
                                <Text rtl={rtl} style={[styles.categoryTxt, (this.state.select_category === main_service.id) ? { color: '#fff'} : null]}>{main_service.title}</Text>
                            </TouchableOpacity>
                        ) : null })}
                    </View>
                </ScrollView>

                {_.map(services, (main_service, key) => { return (main_service.entity_relation_services.length > 0) ? (
                    <View key={key}
                          onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            const serviceOffset = this.state.serviceOffset;
                            serviceOffset[main_service.id] = layout.y;
                            const serviceOffsetArr = this.state.serviceOffsetArr;
                            serviceOffsetArr.push({ offset: layout.y, id: main_service.id });
                            this.setState({ serviceOffset, serviceOffsetArr });
                          }}>
                        <Text rtl={rtl} style={rtl ? styles.rtl_sectionTitle : styles.sectionTitle}>{main_service.title}</Text>
                        <View style={styles.container} >
                            {_.map(main_service.entity_relation_services, (service, ind) => <EntityService key={ind}
                                                                                                           index={ind}
                                                                                                           length={main_service.entity_relation_services.length}
                                                                                                           rtl={rtl}
                                                                                                           entity={entity}
                                                                                                           service={service} />)}
                        </View>
                    </View>
                ): null})}
            </Fragment>
    }
}

const mapStateToProps = state => {
    return {
        loading: state.entity.services_loading,
        services: state.entity.services,
        basket: state.basket.data,
        basket_count: state.basket.counter,
        entity_basket: state.basket.entity,
    }
};

export default connect(mapStateToProps, null)(Services);
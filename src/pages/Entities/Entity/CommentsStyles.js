import {Font_Bold, Font_Regular, Rtl_Font_Medium} from "../../../redux/types";

export default {
    sectionTitle: {
        width: '100%',
        fontSize: 14,
        color: '#999',
        padding: 15,
    },
    rateContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingVertical: 15
    },
    rtl_rateContainer: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingVertical: 15
    },
    rate: {
        fontSize: 22
    },
    commentsCount: {
        fontFamily: Font_Regular,
        fontSize: 14,
        color: '#999',
        marginTop: 5
    },
    rateNum: {
        flex: 1,
        maxWidth: 100,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 10
    },
    rateDetails: {
        flex: 1,
        marginHorizontal: 10
    },
    rateItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 5,
    },
    rtl_rateItem: {
        width: '100%',
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 5,
    },
    rateTxt: {
        fontFamily: Font_Regular,
        fontSize: 15,
        color: '#666'
    },
    line: {
        flex: 1,
        height: 1,
        backgroundColor: '#ddd',
        marginHorizontal: 8
    },
    emptyComments: {
        width: '100%',
        color: '#333',
        textAlign: 'center',
        paddingVertical: 10
    },
    comments: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 15,
    },
    comment: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
        paddingBottom: 15,
        marginBottom: 15
    },
    commentInfo: {
        flexDirection: 'row',
        marginBottom: 10
    },
    rtl_commentInfo: {
        flexDirection: 'row-reverse',
        marginBottom: 10
    },
    imgContainer: {
        width: 65,
        height: 65,
        borderRadius: 50,
        marginRight: 10,
        overflow: 'hidden'
    },
    rtl_imgContainer: {
        width: 65,
        height: 65,
        borderRadius: 50,
        marginLeft: 10,
        overflow: 'hidden'
    },
    img: {
        width: '100%',
        height: '100%'
    },
    username: {
        fontFamily: Font_Bold,
        fontSize: 18,
        color: '#333'
    },
    rtl_username: {
        fontFamily: Rtl_Font_Medium,
        fontSize: 16,
        color: '#333'
    },
    date: {
        fontSize: 14,
        color: '#999'
    },
    commentContent: {
        color: '#666'
    }
}
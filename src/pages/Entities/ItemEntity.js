import React from 'react';
import { TouchableOpacity, ImageBackground, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {Stars, Text} from '../../components';
import {Font_Medium, Rtl_Font_Medium} from "../../redux/types";

export default ItemEntity = ({ rtl, item }) => {
    const { title, media_image, short_description, rate } = item.model_data;
    return (
        <TouchableOpacity style={styles.container} activeOpacity={0.6} onPress={()=>{ Actions.entity({ item: item.model_data }); }}>
            <ImageBackground source={{ uri: media_image.url_image }} style={styles.image}>
                {(rate.data_rate) ? (
                    <View style={rtl ? styles.rtl_top_rate : styles.top_rate}>
                        <Stars disableColor="#fff" value={rate.data_rate} size={14} rtl={rtl} />
                    </View>
                ) : null}
                <View style={rtl ? styles.rtl_detail : styles.detail}>
                    <View style={styles.data}>
                        <Text rtl={rtl} style={rtl ? styles.rtl_title : styles.title}>{title}</Text>
                        {/*<Text style={styles.address}>{address}</Text>*/}
                        <Text numberOfLines={1} ellipsizeMode="tail" rtl={rtl} style={styles.meta}>{short_description}</Text>
                    </View>
                    {/*{(rate.data_rate) ? (*/}
                        {/*<View style={styles.rate}>*/}
                            {/*<Text rtl={rtl} style={styles.rateCount}>{rate.data_rate}</Text>*/}
                            {/*<Stars value={rate.data_rate} size={15} rtl={rtl} />*/}
                        {/*</View>*/}
                    {/*) : null}*/}

                </View>
            </ImageBackground>
        </TouchableOpacity>
    )
};

const styles = {
    container: {
        position: 'relative',
        width: '100%',
        height: 250,
        // marginVertical: 5,
        // borderRadius:  5,
        // overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end'
    },
    detail: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        backgroundColor: 'rgba(0,0,0,0.2)'
    },
    rtl_detail: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        padding: 10,
        backgroundColor: 'rgba(0,0,0,0.15)'
    },
    title: {
        fontFamily: Font_Medium,
        fontSize: 18,
        color: '#fff',
        textShadowColor: 'rgba(0, 0, 0, 0.6)',
        textShadowOffset: {width: 0, height: 0},
        textShadowRadius: 1
    },
    rtl_title: {
        fontFamily: Rtl_Font_Medium,
        fontSize: 18,
        color: '#fff',
        textShadowColor: 'rgba(0, 0, 0, 0.6)',
        textShadowOffset: {width: 0, height: 0},
        textShadowRadius: 1
    },
    meta: {
        fontSize: 14,
        marginTop: 5,
        color: '#fff',
        textShadowColor: 'rgba(0, 0, 0, 0.6)',
        textShadowOffset: {width: 0, height: 0},
        textShadowRadius: 1
    },
    top_rate: {
        position: 'absolute',
        top: 15,
        left: 15,
        backgroundColor: 'rgba(0,0,0,0.4)',
        borderRadius: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    rtl_top_rate: {
        position: 'absolute',
        top: 15,
        right: 15,
        backgroundColor: 'rgba(0,0,0,0.4)',
        borderRadius: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    rate: {
        width: 80,
        alignItems: 'center',
        justifyContent: 'center'
    },
    rateCount: {
        width: '100%',
        fontSize: 16,
        color: '#fff',
        textAlign: 'center'
    }
};
import React, {Fragment} from 'react';
import {Text} from "../../components";
import styles from "./BasketStyles";
import translate from "../../translate";
import {Image, View} from "react-native";

const BasketEntity = ({ entity, rtl, lang }) => (
    <Fragment>
        <Text rtl={rtl} style={styles.sectionTitle}>{translate.entity[lang]}</Text>
        <View style={rtl ? styles.rtl_entity : styles.entity}>
            <View style={rtl ? styles.rtl_entityImgContainer : styles.entityImgContainer}>
                <Image source={{ uri: entity.image }} style={styles.entityImg} />
            </View>
            <View>
                <Text rtl={rtl} style={styles.title}>{entity.entity.title}</Text>
                {(entity.entity.short_description !== null) ? <Text rtl={rtl} style={rtl ? styles.rtl_meta : styles.meta}>{entity.entity.short_description}</Text> : null}
            </View>
        </View>
    </Fragment>
);

export default BasketEntity;
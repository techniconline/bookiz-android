import {danger, secondary, success} from "../../resources/Colors";

export default {
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    sectionTitle: {
        width: '100%',
        fontSize: 14,
        backgroundColor: '#eee',
        color: '#999',
        padding: 15,
    },
    payment: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    rtl_payment: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    selectContainer: { paddingRight: 10 },
    rtl_selectContainer: { paddingLeft: 10 },
    select: {
        width: 30,
        height: 30,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    info: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row'
    },
    rtl_info: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row-reverse'
    },
    img: {
        width: 50,
        height: 50,
        marginRight: 10
    },
    rtl_img: {
        width: 50,
        height: 50,
        marginLeft: 10
    },
    service: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    rtl_service: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    info_service: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    rtl_info_service: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    data: {
        flex: 1,
        maxWidth: 150,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    rtl_data: {
        flex: 1,
        maxWidth: 150,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center'
    },
    prices: {
        width: 150,
        justifyContent: 'center',
        alignItems: 'center'
    },
    oldPrice: {
        fontSize: 13,
        width: '100%',
        color: danger,
        textAlign: 'center',
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    rtl_oldPrice: {
        fontSize: 11,
        width: '100%',
        color: danger,
        textAlign: 'center',
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    newPrice: {
        width: '100%',
        fontSize: 16,
        textAlign: 'center',
        color: secondary
    },
    rtl_newPrice: {
        width: '100%',
        fontSize: 14,
        textAlign: 'center',
        color: secondary
    },
    submitBtn: {
        backgroundColor: secondary,
        paddingVertical: 15
    },
}
import React, {Component, Fragment} from 'react';
import styles from "./BasketStyles";
import {ScrollView, TouchableOpacity, View} from "react-native";
import {Text} from "../../components/Elements";
import translate from "../../translate";
import _ from "lodash";
import Icon from "react-native-vector-icons/FontAwesome5";
import {Button} from "../../components/Form";
import {secondary} from "../../resources/Colors";


class ServiceOptions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date_times: null,
            selected_date: null,
            selected_time: null,
            selected_employee: null
        }
    }
    componentDidMount() {
        const { selected_service, lang } = this.props;
        const anyone = {
            entity_employee_id: -1,
            for_gender: "all",
            full_name: translate.anyone[lang]
        };
        if(typeof selected_service !== "undefined") {
            const { date, employee, item, time } = selected_service;
            let date_times = null;
            let selected_date = null;
            let selected_time = null;
            let selected_employee = null;
            if(typeof date !== "undefined") {
                selected_date = date;
                date_times = item.extera_data.timesheet[date];
            }
            if(typeof selected_time !== "undefined") selected_time = time;
            if(typeof employee !== "undefined") selected_employee = employee;
            else selected_employee = anyone;

            this.setState({ selected_date, date_times, selected_time, selected_employee});
        }else {
            this.setState({ selected_employee: anyone });
        }
    }
    format_date(data) {
        const months_min = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        const date = new Date(data);
        return date.getDate() + ' ' + months_min[date.getMonth()];
    }
    scrollToEnd() {
        this.carousel.scrollToEnd({ animated: false });
    }
    renderDate() {
        const { item, rtl} = this.props;
        const timesheets = item.extera_data.timesheet;
        return (
            <ScrollView contentContainerStyle={(rtl)?{flexDirection: 'row-reverse'}: null}
                        horizontal
                        ref={it => { this.carousel = it; }}
                        onContentSizeChange={(rtl) ? this.scrollToEnd.bind(this) : null}
                        showsHorizontalScrollIndicator={false}>
                <View style={rtl ? styles.rtl_date_list : styles.date_list}>
                    {_.map(timesheets, (date_times, key) => (
                        <TouchableOpacity key={key} style={(this.state.selected_date === key) ? styles.active_date : styles.date} onPress={()=>{ this.setState({ date_times, selected_date: key })}}>
                            <Text rtl={rtl} style={(this.state.selected_date === key) ? styles.active_date_text : styles.date_text}>{this.format_date(key)}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            </ScrollView>
        );
    }
    onSelect() {
        const { selected_date, selected_time, selected_employee } = this.state;
        this.props.onSelect({ item: this.props.item, date: selected_date, time: selected_time, employee: selected_employee});
    }
    render() {
        const { rtl, lang, item } = this.props;
        const { date_times, selected_time, selected_employee } = this.state;
        const anyone = {
            entity_employee_id: -1,
            for_gender: "all",
            full_name: translate.anyone[lang]
        };
        return (
            <View style={styles.modalContainer}>
                <View style={styles.modalContent}>
                    <ScrollView>
                        <Text rtl={rtl} style={styles.sectionTitle}>{translate.select_date[lang]}</Text>
                        {this.renderDate({rtl})}
                        {(date_times !== null) ? (
                            <Fragment>
                                <Text rtl={rtl} style={styles.sectionTitle}>{translate.select_time[lang]}</Text>
                                <View style={styles.modal.list}>
                                    {_.map(date_times, (time, key) => (
                                        <TouchableOpacity key={key} style={rtl ? styles.modal.rtl_item : styles.modal.item} onPress={()=>{ this.setState({selected_time: key});}}>
                                            <View style={styles.selectContainer}>
                                                {(selected_time === key) ? (
                                                    <View style={[styles.select, { borderColor: secondary}]}><Icon color={secondary} size={12} name="check" solid /></View>
                                                ) : (
                                                    <View style={styles.select} />
                                                )}
                                            </View>
                                            <View style={styles.modal.icon}>
                                                <Icon color="#666" size={16} name="clock" solid />
                                            </View>
                                            <Text rtl={rtl} style={styles.modal.name}>{key}</Text>
                                        </TouchableOpacity>
                                    ))}
                                </View>
                            </Fragment>
                        ) : null }
                        <Text rtl={rtl} style={styles.sectionTitle}>{translate.select_staff[lang]}</Text>
                        <View style={styles.modal.list}>
                            <TouchableOpacity style={rtl ? styles.modal.rtl_item : styles.modal.item} onPress={()=>{ this.setState({selected_employee: anyone});}}>
                                <View style={styles.selectContainer}>
                                    {(selected_employee !== null && selected_employee.entity_employee_id === anyone.entity_employee_id) ? (
                                        <View style={[styles.select, { borderColor: secondary}]}><Icon color={secondary} size={12} name="check" solid /></View>
                                    ) : (
                                        <View style={styles.select} />
                                    )}
                                </View>
                                <View style={styles.modal.icon}>
                                    <Icon color="#666" size={16} name="male" solid />
                                    <Icon color="#666" size={16} name="female" solid />
                                </View>
                                <Text rtl={rtl} style={styles.modal.name}>{anyone.full_name}</Text>
                            </TouchableOpacity>
                            {_.map(item.extera_data.employees, (employee, key) => (
                                <TouchableOpacity key={key} style={rtl ? styles.modal.rtl_item : styles.modal.item} onPress={()=>{ this.setState({selected_employee: employee});}}>
                                    <View style={styles.selectContainer}>
                                        {(selected_employee !== null && selected_employee.entity_employee_id === employee.entity_employee_id) ? (
                                            <View style={[styles.select, { borderColor: secondary}]}><Icon color={secondary} size={12} name="check" solid /></View>
                                        ) : (
                                            <View style={styles.select} />
                                        )}
                                    </View>
                                    <View style={styles.modal.icon}>
                                        <Icon color="#666" size={16} name="male" solid />
                                        <Icon color="#666" size={16} name="female" solid />
                                    </View>
                                    <Text rtl={rtl} style={styles.modal.name}>{employee.full_name}</Text>
                                </TouchableOpacity>
                            ))}
                        </View>
                    </ScrollView>
                    <View style={{ flexDirection: 'row', flexWrap: 'nowrap', width: '100%'}}>
                        <Button rtl={rtl} title={translate.select[lang].toUpperCase()} style={styles.modal.button} onPress={this.onSelect.bind(this)} />
                        <Button rtl={rtl} title={translate.cancel[lang].toUpperCase()} titleColor={secondary} style={styles.modal.cancel} onPress={this.props.onClose.bind(this)} />
                    </View>
                </View>
                <TouchableOpacity activeOpacity={1} style={styles.layoutModal} onPress={this.props.onClose.bind(this)} />
            </View>
        )
    }
}

export default ServiceOptions;
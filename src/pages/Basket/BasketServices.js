import React, {Fragment} from 'react';
import {Text} from "../../components/Elements";
import styles from "./BasketStyles";
import translate from "../../translate";
import {TouchableOpacity, View} from "react-native";
import _ from "lodash";
import Icon from "react-native-vector-icons/FontAwesome5";
import {danger, secondary} from "../../resources/Colors";

const BasketServices = ({ basket, rtl, lang, onSelect, onDelete, onDisabled, selected_services }) => (
    <Fragment>
        <Text rtl={rtl} style={styles.sectionTitle}>{translate.services[lang]}</Text>
        <View style={styles.services}>
            {_.map(basket.cart_items, (service, key) => {
                let service_employee = null;
                let service_time = null;
                let selected = false;
                if(typeof selected_services[service.id] !== "undefined") {
                    const { date, employee, item, time } = selected_services[service.id];
                    if(typeof time !== "undefined") service_time = time;
                    if(typeof employee !== "undefined") service_employee = employee.full_name;
                    selected = (typeof employee !== "undefined" && employee !== null && typeof time !== "undefined" && time !== null);
                }
                const has_timesheet = service.extera_data.timesheet.length !== 0;
                return (
                    <View style={{ position: 'relative' }} key={key} >
                        <TouchableOpacity style={rtl ? styles.rtl_service : styles.service}
                                          activeOpacity={(has_timesheet) ? 0.6 : 1}
                                          onPress={() => { if(has_timesheet > 0) onSelect(service); }}>
                            <View style={rtl ? styles.rtl_selectContainer : styles.selectContainer}>
                                {(selected)
                                    ? <View style={[styles.select, { borderColor: secondary}]}><Icon color={secondary} size={12} name="check" solid /></View>
                                    : <View style={styles.select} />}
                            </View>
                            <View style={rtl ? styles.rtl_info_service : styles.info_service}>
                                <Text rtl={rtl} style={styles.title}>{service.name}</Text>
                                {(service_employee !== null) ? <Text rtl={rtl} style={styles.meta}>{translate.staff[lang]}: {service_employee}</Text> : null}
                                {(service_time !== null) ? <Text rtl={rtl} style={styles.meta}>{translate.time[lang]}: {service_time}</Text> : null}
                            </View>
                            <View style={rtl ? styles.rtl_data : styles.data}>
                                <View style={styles.prices}>
                                    {(service.discount_amount !== 0) ?
                                        <Text rtl={rtl} style={rtl ? styles.rtl_oldPrice : styles.oldPrice}>{service.discount_text}</Text> : null}
                                    <Text rtl={rtl} style={rtl ? styles.rtl_newPrice : styles.newPrice}>{service.total}</Text>
                                </View>
                            </View>
                            <TouchableOpacity style={rtl ? styles.rtl_selectContainer : styles.selectContainer} onPress={() => { onDelete(service); }}>
                                <Icon color={danger} size={20} name="times" solid />
                            </TouchableOpacity>
                        </TouchableOpacity>
                        {(!has_timesheet) ? <View style={styles.disable_service}><Text rtl={rtl} style={styles.disable_service_text}>{translate.disable_cart_service[lang]}</Text></View> : null }
                    </View>
                )
            })}
        </View>
    </Fragment>
);

export default BasketServices;
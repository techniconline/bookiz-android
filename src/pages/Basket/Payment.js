import React, { Component } from 'react';
import { connect } from 'react-redux';
import translate from "../../translate";
import {ScrollView, TouchableOpacity, View, Image, ToastAndroid, AsyncStorage, ActivityIndicator} from "react-native";
import {Navbar, Button, Text, ImageIcon} from "../../components";
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";
import _ from 'lodash';
import styles from "./PaymentStyles";
import { on_payment } from '../../redux/actions';
import {ACCESS_TOKEN} from "../../redux/types";
import {secondary} from "../../resources/Colors";
import {IconLeftArrow} from "../../resources/Icon";

class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openWeb: false,
            auth: null,
            selected: null,
            html: null,
            submit: false
        }
    }
    componentDidMount() {
        AsyncStorage.getItem(ACCESS_TOKEN).then(auth => { this.setState({ auth })});
    }
    onSubmit() {
        const { lang, selected_services } = this.props;
        if(this.state.selected !== null) {
            let booking_details = {};
            _.map(selected_services, service => {
                const {item, employee, date, time} = service;
                const entity_relation_service = item.extera_data.entity_relation_service;
                booking_details[item.item_id] = {
                    service: item.item_id,
                    booking_model_id: entity_relation_service.entity_id,
                    booking_model_type: 'entities',
                    model_id: entity_relation_service.id,
                    model_type: 'entity_relation_services',
                    entity_service: entity_relation_service.entity_service_id,
                    employee: employee.entity_employee_id,
                    date,
                    time,
                    entity: entity_relation_service.entity_id,
                    booking_calendar: item.extera_data.calendar.id,
                }
            });
            const data = {
                option: {payment: this.state.selected},
                payment: 'online',
                booking_details
            };
            this.setState({ submit: true });
            this.props.on_payment(data);
        } else {
            ToastAndroid.showWithGravity(translate.select_payment[lang], ToastAndroid.SHORT, ToastAndroid.CENTER);
        }
    }
    renderButton() {
        const { lang, direction } = this.props;
        const rtl = direction === 'rtl';
        if (this.state.submit) return <Button style={styles.submitBtn}><ActivityIndicator color="#fff" size="small" /></Button>;
        return <Button style={styles.submitBtn} titleColor="#fff" title={translate.payment[lang].toUpperCase()} rtl={rtl} onPress={this.onSubmit.bind(this)} />;
    }
    render() {
        const { basket, lang, direction, selected_services } = this.props;
        const { selected } = this.state;
        const rtl = direction === 'rtl';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <ImageIcon source={IconLeftArrow} size={25} />
            </TouchableOpacity>
        );

        return (
            <View style={{ flex: 1}}>
                <Navbar rtl={rtl} leftComponent={left} title={translate.payment[lang].toUpperCase()}/>
                <ScrollView>
                    <Text rtl={rtl} style={styles.sectionTitle}>{translate.payment_type[lang]}</Text>
                    <View style={styles.container}>
                        {_.map(basket.gateways.online, (item, key) => (
                            <TouchableOpacity style={rtl ? styles.rtl_payment : styles.payment} key={key} onPress={() => { this.setState({ selected: key }); }}>
                                <View style={rtl ? styles.rtl_selectContainer : styles.selectContainer}>
                                    {(selected === key)
                                        ? <View style={[styles.select, { borderColor: secondary}]}><Icon color={secondary} size={12} name="check" solid /></View>
                                        : <View style={styles.select} />}
                                </View>
                                <View style={rtl ? styles.rtl_info : styles.info}>
                                    <Image source={{ uri: item.option.image }}
                                           onError={(e)=>{}}
                                           style={rtl ? styles.rtl_img : styles.img} />
                                    <Text rtl={rtl} style={styles.title}>{item.option.name}</Text>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                    <Text rtl={rtl} style={styles.sectionTitle}>{translate.services[lang]}</Text>
                    <View style={styles.container}>
                        {_.map(selected_services, (service, key) => (
                            <View style={rtl ? styles.rtl_service : styles.service} key={key}>
                                <View style={rtl ? styles.rtl_selectContainer : styles.selectContainer}>
                                    <View style={[styles.select, { borderColor: secondary}]}><Icon color={secondary} size={12} name="check" solid /></View>
                                </View>
                                <View style={rtl ? styles.rtl_info_service : styles.info_service}>
                                    <Text rtl={rtl} style={styles.title}>{service.item.name}</Text>
                                    {<Text rtl={rtl} style={styles.meta}>{translate.staff[lang]}: {service.employee.full_name}</Text>}
                                    {<Text rtl={rtl} style={styles.meta}>{translate.time[lang]}: {service.date} {service.time}</Text>}
                                </View>
                                <View style={rtl ? styles.rtl_data : styles.data}>
                                    <View style={styles.prices}>
                                        {(service.item.discount_amount !== 0) ?
                                            <Text rtl={rtl} style={rtl ? styles.rtl_oldPrice : styles.oldPrice}>{service.item.discount_text}</Text> : null}
                                        <Text rtl={rtl} style={rtl ? styles.rtl_newPrice : styles.newPrice}>{service.item.total}</Text>
                                    </View>
                                </View>
                            </View>
                        ))}
                    </View>
                </ScrollView>
                {this.renderButton()}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        lang: state.setting.lang,
        direction: state.setting.direction,
        basket: state.basket.basket,
    };
};

export default connect(mapStateToProps, { on_payment })(Payment);
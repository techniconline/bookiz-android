import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {View, ScrollView, Image, Modal, TouchableOpacity, Alert, ToastAndroid} from 'react-native';
import {Navbar} from "../../components/Layouts";
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";
import styles from './BasketStyles';
import {Text, Button, FullLoading, ErrorPage, ImageIcon} from "../../components";
import {add_basket, remove_basket, get_cart} from "../../redux/actions";
import translate from '../../translate';
import BasketEntity from "./BasketEntity";
import BasketServices from "./BasketServices";
import ServiceOptions from "./ServiceOptions";
import {IconLeftArrow, IconLeftArrowWhite} from "../../resources/Icon";

class Basket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            serviceModal: null,
            selected_services: {},
            set_data: false,
            disabled_service: []
        }
    }
    componentDidMount() {
        this.props.get_cart();
    }
    componentDidUpdate() {
        const { basket ,loading } = this.props;
        if(!this.state.set_data && !loading && basket !== null) {
            let disabled_service = [];
            _.map(basket.cart_items, item => {
                if(item.extera_data.timesheet.length === 0) disabled_service.push(item.id)
            });
            this.setState({ set_data: true, disabled_service });
        }
    }
    onSelectService(data) {
        let {selected_services} = this.state;
        selected_services = { ...selected_services, [data.item.id]: data};
        this.setState({ selected_services, serviceModal: null })
    }
    renderServiceModal() {
        const { direction, lang } = this.props;
        const rtl = direction === 'rtl';
        const item = this.state.serviceModal;
        return (
            <Modal animationType="fade"
                transparent={true}
                visible={item !== null}
                onRequestClose={() => this.setState({ serviceModal: null })}>
                {(item !== null) ? <ServiceOptions rtl={rtl}
                                                   item={item}
                                                   lang={lang}
                                                   selected_service={this.state.selected_services[item.id]}
                                                   onSelect={this.onSelectService.bind(this)}
                                                   onClose={() => this.setState({ serviceModal: null })} /> : null }
            </Modal>
        )
    }
    onDeleteService(service) {
        const { lang } = this.props;
        Alert.alert(
            translate.delete_cart[lang],
            translate.confirm_delete_service_cart[lang],
            [
                {text: translate.yes[lang], onPress: () => { this.props.remove_basket(service.item_id, service.cart_id); } },
                {text: translate.no[lang] },
            ],
        );
    }
    onSubmit() {
        const { selected_services , disabled_service } = this.state;
        const { basket, lang } = this.props;
        let complete_data = false;
        _.map(basket.cart_items, item => {
            const selected = selected_services[item.id];
            if(disabled_service.indexOf(item.id) === -1) {
                if(typeof selected !== "undefined") {
                    const { employee, time } = selected;
                    if(!(typeof employee !== "undefined" && employee !== null && typeof time !== "undefined" && time !== null))
                        complete_data = true;
                }else {
                    complete_data = true;
                }
            }
        });
        if (complete_data)
            ToastAndroid.showWithGravity(translate.complete_services[lang], ToastAndroid.SHORT, ToastAndroid.CENTER);
        else
            Actions.payment({selected_services});
    }
    render() {
        const { errors, loading, basket, direction, lang } = this.props;
        const rtl = direction === 'rtl';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <ImageIcon source={IconLeftArrow} size={25} />
            </TouchableOpacity>
        );
        if(loading) return <FullLoading />;
        if(errors !== null) return <ErrorPage rtl={rtl} errors={errors} />;
        if(basket !== null && basket.cart_items.length > 0) {
            const entity = basket.cart_items[0].extera_data.entity_relation_service;
            return (
                <View style={{ flex: 1 }}>
                    {this.renderServiceModal()}
                    <Navbar rtl={rtl} leftComponent={left} title={translate.basket[lang].toUpperCase()}/>
                    <ScrollView>
                        <BasketEntity entity={entity} rtl={rtl} lang={lang} />
                        <BasketServices basket={basket}
                                        rtl={rtl}
                                        lang={lang}
                                        selected_services={this.state.selected_services}
                                        onDelete={this.onDeleteService.bind(this)}
                                        onSelect={(service)=>{ this.setState({ serviceModal: service }) }} />
                    </ScrollView>
                    <Button style={styles.submitBtn} title={translate.submit[lang]} rtl={rtl} onPress={this.onSubmit.bind(this)} />
                </View>
            )
        }
        return null;
    }
}

const mapStateToProps = state => {
    return {
        basket: state.basket.basket,
        errors: state.basket.errors,
        // basket: state.basket.data,
        loading: state.basket.loading,
        basket_count: state.basket.counter,
        entity: state.basket.entity,
        lang: state.setting.lang,
        direction: state.setting.direction,
    }
};

export default connect(mapStateToProps, { get_cart, add_basket, remove_basket })(Basket);
import {Font_Regular, Rtl_Font_Regular} from "../../redux/types";
import {danger, primary, secondary, success} from "../../resources/Colors";

export default {
    fullLoading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionTitle: {
        width: '100%',
        fontSize: 14,
        backgroundColor: '#eee',
        color: '#999',
        padding: 15,
    },
    entity: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    rtl_entity: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    entityImgContainer: {
        width: 70,
        height: 70,
        borderRadius: 50,
        overflow: 'hidden',
        marginRight: 10
    },
    rtl_entityImgContainer: {
        width: 70,
        height: 70,
        borderRadius: 50,
        overflow: 'hidden',
        marginLeft: 10
    },
    entityImg: {
        width: '100%',
        height: '100%'
    },
    services: {
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    service: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    rtl_service: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    selectContainer: {
        paddingRight: 10
    },
    rtl_selectContainer: {
        paddingLeft: 10
    },
    select: {
        width: 30,
        height: 30,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 15,
        color: secondary
    },
    meta: {
        fontFamily: Font_Regular,
        fontSize: 14,
        color: '#999'
    },
    rtl_meta: {
        fontFamily: Rtl_Font_Regular,
        fontSize: 13,
        color: '#999'
    },
    info: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    rtl_info: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    data: {
        flex: 1,
        maxWidth: 150,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    rtl_data: {
        flex: 1,
        maxWidth: 150,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center'
    },
    prices: {
        width: 150,
        justifyContent: 'center',
        alignItems: 'center'
    },
    oldPrice: {
        fontSize: 13,
        width: '100%',
        color: danger,
        textAlign: 'center',
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    rtl_oldPrice: {
        fontSize: 11,
        width: '100%',
        color: danger,
        textAlign: 'center',
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    newPrice: {
        width: '100%',
        fontSize: 16,
        textAlign: 'center',
        color: secondary
    },
    rtl_newPrice: {
        width: '100%',
        fontSize: 14,
        textAlign: 'center',
        color: secondary
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    layoutModal: {
        flex: 1,
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.3)',
        zIndex: 1,
    },
    modalBody: {
        backgroundColor: '#fff',
    },
    modalContent: {
        position: 'absolute',
        width: '90%',
        backgroundColor: '#fff',
        height: '65%',
        borderRadius: 7,
        overflow: 'hidden',
        elevation: 8,
        zIndex: 10,
    },
    modal: {
        list: {
            backgroundColor: '#fff',
            paddingHorizontal: 15,
            paddingVertical: 10
        },
        item: {
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingVertical: 15,
            borderBottomWidth: 1,
            borderBottomColor: '#eee'
        },
        rtl_item: {
            width: '100%',
            flexDirection: 'row-reverse',
            alignItems: 'center',
            paddingVertical: 15,
            borderBottomWidth: 1,
            borderBottomColor: '#eee'
        },
        name: {
            fontSize: 16,
        },
        icon: {
            width: 35,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
        },
        buttons: {
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'nowrap'
        },
        button: {
            flex: 1,
            backgroundColor: secondary,
            borderWidth: 0,
            borderRadius: 0
        },
        cancel: {
            flex: 1,
            backgroundColor: primary,
            borderWidth: 0,
            borderRadius: 0
        }
    },
    submitBtn: {
        backgroundColor: secondary,
        paddingVertical: 15
    },
    date_list: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    rtl_date_list: {
        flexDirection: 'row-reverse',
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    date: {
        width: 45,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 70,
        borderColor: '#999',
        borderWidth: 2,
        overflow: 'hidden',
        marginRight: 5
    },
    active_date: {
        width: 45,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 70,
        borderColor: primary,
        borderWidth: 2,
        overflow: 'hidden',
        marginRight: 5
    },
    date_text: {
        fontSize: 12,
        width: '100%',
        textAlign: 'center',
        color: '#999'
    },
    active_date_text: {
        fontSize: 12,
        width: '100%',
        textAlign: 'center',
        color: primary
    },
    disable_service: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 10,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    disable_service_text: {
        fontSize: 16,
        color: '#fff'
    },

}
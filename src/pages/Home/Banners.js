import React from 'react';
import { TouchableOpacity, ImageBackground, ScrollView, View } from 'react-native';
import {Text} from "../../components";
import { SidebarImage } from "../../resources/Image";

const Banners = () => {
    return (
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={styles.container}>
                <TouchableOpacity activeOpacity={0.8} style={styles.item}>
                    <ImageBackground style={styles.img} source={SidebarImage}>
                        <View style={styles.textContainer}>
                            <Text style={styles.text}>Banner 1</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.item}>
                    <ImageBackground style={styles.img} source={SidebarImage}>
                        <View style={styles.textContainer}>
                            <Text style={styles.text}>Banner 2</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.item}>
                    <ImageBackground style={styles.img} source={SidebarImage}>
                        <View style={styles.textContainer}>
                            <Text style={styles.text}>Banner 3</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.item}>
                    <ImageBackground style={styles.img} source={SidebarImage}>
                        <View style={styles.textContainer}>
                            <Text style={styles.text}>Banner 4</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
};

const styles = {
    container: {
        flexDirection: 'row',
        marginVertical: 10,
        paddingHorizontal: 5,
    },
    item: {
        height: 140,
        width: 250,
        borderRadius: 8,
        overflow: 'hidden',
        marginHorizontal: 8,
        elevation: 3
    },
    img: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 15,
        color: '#fff'
    }
};

export default Banners;
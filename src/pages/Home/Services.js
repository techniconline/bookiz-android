import React from 'react';
import { TouchableOpacity, Image, ScrollView, View } from 'react-native';
import {Text} from "../../components";
import {Icon_Barber, Icon_Body, Icon_Face, Icon_Nail, Icon_Spa} from "../../resources/Image";
import {Actions} from "react-native-router-flux";

const Services = () => {
    return (
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={styles.container}>
                <TouchableOpacity style={styles.item} onPress={()=>{ Actions.services(); }}>
                    <Image source={Icon_Barber} style={styles.icon} />
                    <Text style={styles.text}>Barber</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item} onPress={()=>{ Actions.services(); }}>
                    <Image source={Icon_Body} style={styles.icon} />
                    <Text style={styles.text}>Body</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item} onPress={()=>{ Actions.services(); }}>
                    <Image source={Icon_Face} style={styles.icon} />
                    <Text style={styles.text}>Face</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item} onPress={()=>{ Actions.services(); }}>
                    <Image source={Icon_Nail} style={styles.icon} />
                    <Text style={styles.text}>Nail</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item} onPress={()=>{ Actions.services(); }}>
                    <Image source={Icon_Spa} style={styles.icon} />
                    <Text style={styles.text}>Spa</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item} onPress={()=>{ Actions.services(); }}>
                    <Image source={Icon_Barber} style={styles.icon} />
                    <Text style={styles.text}>Barber</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item} onPress={()=>{ Actions.services(); }}>
                    <Image source={Icon_Body} style={styles.icon} />
                    <Text style={styles.text}>Body</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item} onPress={()=>{ Actions.services(); }}>
                    <Image source={Icon_Face} style={styles.icon} />
                    <Text style={styles.text}>Face</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
};

const styles = {
    container: {
        flexDirection: 'row',
        marginVertical: 10,
        marginHorizontal: 15,
        paddingVertical: 5,
        paddingHorizontal: 5,
        backgroundColor: '#fff',
        elevation: 2
    },
    item: {
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 15
    },
    icon: {
        width: 40,
        height: 40,
        marginBottom: 5
    },
    text: {
        fontSize: 15,
        color: '#999'
    }
};

export default Services;
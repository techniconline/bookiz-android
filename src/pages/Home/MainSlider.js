import React, { Component } from 'react';
import { ViewPagerAndroid, View, Image } from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';

class MainSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 0,
            length: 3
        }
    }
    componentDidMount() {
        const { animate, animateSpeed } = this.props.configs;
        if(animate) {
            setInterval(() => {
                this.setState({ active: (this.state.active+1)%this.state.length }, () => {
                    this.viewPager.setPage(this.state.active);
                });
            }, animateSpeed);
        }
    }
    render() {
        const { active } = this.state;
        const { data, configs, style, height, caption } = this.props;
        if(data.length === 0) return null;
        const containerStyles = [styles.container];
        if(Array.isArray(style)) containerStyles.concat(style);
        else containerStyles.push(style);
        containerStyles.push({ height });
        return (
            <View style={containerStyles}>
                <ViewPagerAndroid style={styles.pager} ref={(viewPager) => {this.viewPager = viewPager}} onPageSelected={(e)=>{ this.setState({ active: e.nativeEvent.position }); }}>
                    {_.map(data, (slide, key) => (
                        <Image source={{ uri: (slide.url_image) ? slide.url_image: slide.image_url}} style={styles.img} key={key} />
                    ))}
                </ViewPagerAndroid>
                {(caption) ? <View style={styles.caption}>{caption}</View>: null }
                {(configs.dots) ? (
                    <View style={styles.bullets}>
                        {_.map(data, (slide, key) => (
                            <View key={key} style={(active === key) ? styles.activeBullet : styles.bullet} />
                        ))}
                    </View>
                ) : null}
            </View>
        )
    }
}

MainSlider.defaultProps = {
    data: [],
    configs: {
        dots: true,
        animate: true,
        animateSpeed: 7000
    },
    style: {},
    height: 250
};

MainSlider.propTypes = {
    data: PropTypes.array,
    configs: PropTypes.shape({
        dots: PropTypes.bool,
        animate: PropTypes.bool,
        animateSpeed: PropTypes.number,
    }),
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    caption: PropTypes.node
};

const styles = {
    container: {
        position: 'relative',
        width: '100%',
        height: 250,
        backgroundColor: '#fff',
        marginBottom: 8,
        elevation: 2
    },
    pager: {
        width: '100%',
        height: '100%',
    },
    img: {
        width: '100%',
        height: '100%',
    },
    caption: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    bullets: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 10,
        right: 10
    },
    bullet: {
        width: 20,
        height: 2,
        backgroundColor: '#999',
        marginLeft: 4
    },
    activeBullet: {
        width: 20,
        height: 2,
        backgroundColor: '#fff',
        marginLeft: 4
    }
};

export default MainSlider;
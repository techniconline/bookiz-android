import React, {Component} from 'react';
import { connect } from 'react-redux';
import {View, ScrollView, ImageBackground, TouchableOpacity} from 'react-native';
import {ErrorPage, FullLoading, Navbar, Text} from "../../../components";
import { get_blog } from '../../../redux/actions';
import translate from '../../../translate';
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";

class Blog extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.get_blog(this.props.item.id);
    }
    render() {
        const { direction, item, loading, data, errors } = this.props;
        const rtl = direction === 'rtl';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 10 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <Icon color="#fff" size={20} name="arrow-left" solid />
            </TouchableOpacity>
        );
        return (
            <View style={{ flex: 1 }}>
                <Navbar rtl={rtl} leftComponent={left} title={item.title} />
                {(loading) ? <FullLoading/> : (errors !== null) ? <ErrorPage rtl={rtl} errors={errors} /> : (data !== null) ? (
                    <ScrollView style={{ flex: 1}}>
                        <View style={styles.container}>
                            <ImageBackground source={{ uri: data.image }} style={styles.img} >
                                <Text rtl={rtl} style={styles.title}>{data.title}</Text>
                            </ImageBackground>
                            <View style={styles.data}>
                                <Text rtl={rtl} style={styles.description}>{data.short_description}</Text>
                            </View>
                        </View>
                    </ScrollView>
                ) : null}
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        alignItems: 'center'
    },
    img: {
        flex: 1,
        width: '100%',
        height: 300,
        justifyContent: 'flex-end'
    },
    data: {
        flex: 1,
        backgroundColor: '#fff',
        width: '100%',
        marginVertical: 10,
        paddingVertical: 15,
        paddingHorizontal: 15,
    },
    title: {
        color: '#fff',
        fontSize: 20,
        padding: 20,
        textShadowColor: 'rgba(0, 0, 0, 0.6)',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 2
    },
    description: {
        color: '#666',
        fontSize: 16
    }
};

const mapStateToProps = state => {
    const { lang, direction } = state.setting;
    const { loading, data, errors } = state.blog;
    return {
        loading,
        data,
        errors,
        direction,
        lang
    }
};

export default connect(mapStateToProps, { get_blog })(Blog);
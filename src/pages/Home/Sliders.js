import React, { Component } from 'react';
import {TouchableOpacity, Image, ScrollView, View} from 'react-native';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import {Stars, Text} from "../../components";
import {primary} from "../../resources/Colors";
import {Font_Medium, Font_Regular, Rtl_Font_Medium, Rtl_Font_Regular} from "../../redux/types";

class Sliders extends Component {
    constructor(props) {
        super(props);
    }
    scrollToEnd() {
        this.carousel.scrollToEnd({ animated: false });
    }
    render() {
        const {data, rtl} = this.props;
        const title = data.configs.title;
        return (
            <View style={styles.wrapper}>
                <Text style={[styles.headTitle, {fontFamily: (rtl) ? Rtl_Font_Medium : Font_Medium}]} rtl={rtl}>{title}</Text>
                <ScrollView contentContainerStyle={(rtl)?{flexDirection: 'row-reverse'}: null}
                            horizontal
                            ref={it => { this.carousel = it; }}
                            onContentSizeChange={(rtl) ? this.scrollToEnd.bind(this) : null}
                            showsHorizontalScrollIndicator={false}>
                    <View style={rtl ? styles.rtl_container : styles.container}>
                        {_.map(data.data, (item, key) => {
                            const { title, media_image, rate } = item;
                            return (
                                <TouchableOpacity key={key} activeOpacity={0.6} style={styles.item} onPress={()=>{ Actions.entity({ item }); }}>
                                    <Image source={{ uri: media_image.url_thumbnail }} style={styles.img} />
                                    <View style={styles.detail}>
                                        <Text numberOfLines={1}
                                              ellipsizeMode="tail"
                                              rtl={rtl}
                                              style={(rtl) ? styles.rtl_title : styles.title}>{title}</Text>
                                        <View style={rtl ? styles.rtl_rates : styles.rates}>
                                            <Stars value={rate.data_rate} rtl={rtl}/>
                                            <Text style={(rtl) ? styles.rtl_rate : styles.rate}>{rate.data_rate}</Text>
                                        </View>
                                        {/*<Text style={styles.address}>{address.city} - {address.address}</Text>*/}
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const color = '#333';

const styles = {
    wrapper: {
        marginBottom: 8,
        backgroundColor: '#fff',
        paddingVertical: 10,
        elevation: 1
    },
    headTitle: {
        fontSize: 18,
        color: '#333',
        paddingHorizontal: 15
    },
    container: {
        flexDirection: 'row',
        marginVertical: 10,
        paddingHorizontal: 5,
    },
    rtl_container: {
        flexDirection: 'row-reverse',
        marginVertical: 10,
        paddingHorizontal: 5,
    },
    item: {
        width: 270,
        borderRadius: 2,
        // backgroundColor: primary,
        overflow: 'hidden',
        marginHorizontal: 8
    },
    img: {
        width: '100%',
        height: 170,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5
    },
    detail: {
        paddingHorizontal: 5,
        paddingVertical: 5
    },
    title: {
        fontFamily: Font_Medium,
        fontSize: 18,
        color
    },
    rtl_title: {
        fontFamily: Rtl_Font_Medium,
        fontSize: 16,
        color
    },
    rates: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 3
    },
    rtl_rates: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        marginVertical: 3
    },
    rate: {
        fontFamily: Font_Regular,
        fontSize: 13,
        color,
        marginLeft: 5
    },
    rtl_rate: {
        fontFamily: Rtl_Font_Regular,
        fontSize: 12,
        color,
        marginRight: 5
    },
    address: {
        fontFamily: Font_Regular,
        fontSize: 13,
        color: '#666',
    },
    rtl_address: {
        fontFamily: Rtl_Font_Regular,
        fontSize: 12,
        color: '#666',
    }
};

export default Sliders;
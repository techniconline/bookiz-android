import {blue} from "../../resources/Colors";

export default {
    rightNav: {
        flexDirection: 'row-reverse',
    },
    basket: {
        position: 'relative',
        paddingLeft: 10,
        marginRight: 5
    },
    basketCount: {
        fontSize: 14,
        position: 'absolute',
        top: -2,
        left: -2,
        width: 20,
        height: 20,
        color: '#fff',
        backgroundColor: blue,
        borderRadius: 30,
        borderColor: 'rgba(0,0,0,0.1)',
        borderWidth: 1,
        textAlign: 'center'
    },
}
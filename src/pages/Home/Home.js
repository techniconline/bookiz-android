import React, {Component, Fragment} from 'react';
import {View, TouchableOpacity, ScrollView, StatusBar} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Actions } from 'react-native-router-flux';
import {Navbar} from "../../components/Layouts";
import MainSlider from "./MainSlider";
import Services from "./Services";
import Banners from "./Banners";
import Sliders from "./Sliders";
import { get_home } from '../../redux/actions';
import {ErrorPage, FullLoading, Text} from "../../components/Elements";
import _ from 'lodash';
import styles from "./HomeStyles";
import BlogSlider from "./Blog/BlogSlider";
import Loading from "../../Loading";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: ''
        }
    }
    componentDidMount() {
        this.props.get_home();
    }
    renderHome() {
        const data = this.props.data.center;
        const rtl = this.props.direction === 'rtl';
        if(data){
            return (
                <ScrollView>
                    {_.map(data, (item, key) => {
                        switch (item.name) {
                            case 'slider_block':
                                return <MainSlider data={item.configs.slides} key={key} />;
                            case 'entities_block':
                                return <Sliders rtl={rtl} data={item} key={key} />;
                            case 'blogs_block':
                                return <BlogSlider rtl={rtl} data={item} key={key} />;
                            default:
                                return null;
                        }
                    })}
                    {/*<Services />*/}
                    {/*<Banners />*/}
                </ScrollView>
            )
        }
        return null;
    }
    render() {
        const { loading, errors, data, basket_count } = this.props;
        // const left = <TouchableOpacity style={{ paddingHorizontal: 10 }} onPress={()=>{ Actions.drawerOpen(); }}><Icon name="bars" solid color="#fff" size={28} /></TouchableOpacity>;
        // const right = (
        //     <View style={styles.rightNav}>
        //         <TouchableOpacity style={{ paddingHorizontal: 10 }} onPress={()=>{ Actions.search(); }}>
        //             <Icon name="search" solid color="#fff" size={22} />
        //         </TouchableOpacity>
        //         {(basket_count > 0) ? (
        //             <TouchableOpacity style={styles.basket} activeOpacity={0.6} onPress={() => { Actions.basket(); }}>
        //                 <Icon color="#fff" size={22} name="shopping-basket" solid />
        //                 <Text style={styles.basketCount}>{basket_count}</Text>
        //             </TouchableOpacity>
        //         ): null}
        //     </View>
        // );
        return (
            <View style={{ flex: 1, backgroundColor: '#eee' }}>
                <StatusBar backgroundColor="black" barStyle="light-content" />
                {/*<Navbar leftComponent={left} title="Bookiz" rightComponent={right} />*/}
                {(loading) ? <Loading /> : (errors) ? <ErrorPage errors={errors} /> : this.renderHome()}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        basket_count: state.basket.counter,
        loading: state.setting.home_loading,
        data: state.setting.home_data,
        errors: state.setting.home_errors,
        direction: state.setting.direction,
    }
};

export default connect(mapStateToProps, { get_home })(Home);
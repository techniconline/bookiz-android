import React  from 'react';
import {TouchableOpacity, ImageBackground, View, Dimensions} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Text } from '../../components/index';

export default Item = (props) => {
    const { image_url, title } = props.data;
    return (
        <TouchableOpacity style={styles.container} activeOpacity={0.6} onPress={() => { Actions.entities({ category: props.data }); }}>
            <ImageBackground source={{ uri: image_url }} style={styles.image} >
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>{title}</Text>
                </View>
            </ImageBackground>
        </TouchableOpacity>
    )
};

const styles = {
    container: {
        width: (Dimensions.get('window').width/2)-15,
        height: (Dimensions.get('window').height/5),
        marginVertical: 5,
        borderRadius: 7,
        elevation: 3,
        overflow: 'hidden'
    },
    image: {
        position: 'relative',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        width: '100%',
        textAlign: 'center',
        color: '#fff',
        textShadowColor: 'rgba(0, 0, 0, 0.6)',
        textShadowOffset: {width: 0, height: 0},
        textShadowRadius: 1
    }
};
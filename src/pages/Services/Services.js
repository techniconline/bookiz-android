import React, { Component } from 'react';
import {ScrollView, View, ActivityIndicator, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { get_categories } from '../../redux/actions';
import Item from './Item';
import {Navbar, Searchbar} from "../../components";
import {primary} from "../../resources/Colors";
import styles from './ServicesStyles.js';
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";

class Services extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: ''
        }
    }
    componentDidMount() {
        this.props.get_categories();
    }
    render() {
        const { loading, categories } = this.props;
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 10 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <Icon color="#fff" size={20} name="arrow-left" solid />
            </TouchableOpacity>
        );
        return (
            <View style={{ flex: 1 }}>
                <Navbar leftComponent={left} title="Beauty Day" />
                <Searchbar value={this.state.search} onChange={(search) => { this.setState({ search }); }}/>
                {(loading) ? (<View style={styles.fullLoading}><ActivityIndicator size="large" color={primary} animating /></View>): (
                    <ScrollView>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-evenly'}}>
                            {_.map(categories, (category, ind) => (
                                <Item data={category} key={ind} />
                            ))}
                        </View>
                    </ScrollView>
                )}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.category.loading,
        categories: state.category.data
    }
};

export default connect(mapStateToProps, { get_categories })(Services);
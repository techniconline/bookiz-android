import React, { Component } from 'react';
import {View, FlatList} from 'react-native';
import { connect } from 'react-redux';
import { get_favorites } from '../../redux/actions';
import {ErrorPage, FullLoading, Navbar, PageLoading, Text} from "../../components";
import styles from './FavoritesStyles';
import ItemEntity from "../Entities/ItemEntity";
import translate from '../../translate';
import Loading from "../../Loading";

class Favorites extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.get_favorites();
    }
    handleLoadMore() {
        const {response, get_favorites, loading_page, loading_refresh, loading} = this.props;
        if(!loading && !loading_page && !loading_refresh && response.next_page_url !== null)
            get_favorites({ text: this.state.text }, response.next_page_url);
    }
    handleRefresh() {
        this.props.get_favorites(null, true);
    }
    renderEmpty() {
        const { direction, lang } = this.props;
        return <View style={styles.empty}><Text rtl={direction === 'rtl'} style={styles.emptyTxt}>{translate.empty_date[lang]}</Text></View>;
    }
    renderHeader() {
        const { data, lang, direction } = this.props;
        if(data.length === 0) return null;
        return <Text style={styles.resultTitle} rtl={(direction === 'rtl')}>{translate.results[lang]}</Text>
    }
    renderFooter() {
        const { data, response, loading_page, lang, direction } = this.props;
        if(response === null || data.length === 0 ) return null;
        if(loading_page) return <PageLoading />;
        return <Text rtl={direction === 'rtl'} style={styles.footerText}>{response.total} {translate.total_results[lang]}</Text>
    }
    render() {
        const { loading_refresh, loading, data, errors, direction, lang } = this.props;
        const rtl = direction === 'rtl';
        return (loading) ? <Loading />: (
            <View style={{ flex: 1 }}>
                <Navbar rtl={rtl} title={translate.favorite[lang].toUpperCase()} />
                {(loading) ? <FullLoading /> : (errors !== null) ? <ErrorPage rtl={rtl} errors={errors} /> : (
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={data}
                            ListHeaderComponent={() => this.renderHeader()}
                            ListFooterComponent={() => this.renderFooter()}
                            ListEmptyComponent={() => this.renderEmpty()}
                            keyExtractor={(item, ind) => String(item.id)}
                            renderItem={({item}) => <ItemEntity rtl={rtl} item={item} />}
                            onEndReachedThreshold={0.01}
                            onEndReached={this.handleLoadMore.bind(this)}
                            onRefresh={this.handleRefresh.bind(this)}
                            refreshing={loading_refresh}
                        />
                    </View>
                )}
            </View>
        )
    }
}

const mapStateToProps = state => {
    const {
        data,
        response,
        errors,
        loading,
        loading_page,
        loading_refresh
    } = state.favorite;
    return {
        lang: state.setting.lang,
        direction: state.setting.direction,
        data,
        response,
        errors,
        loading,
        loading_page,
        loading_refresh
    }
};

export default connect(mapStateToProps, { get_favorites })(Favorites);
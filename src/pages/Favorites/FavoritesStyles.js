import {danger} from "../../resources/Colors";

export default {
    footerText: {
        fontSize: 12,
        textAlign: 'center',
        paddingVertical: 5,
        borderTopColor: '#ccc',
        borderTopWidth: 1,
        color: '#999',
    },
    resultTitle: {
        paddingVertical: 5,
        paddingHorizontal: 20,
        color: '#999',
        borderTopColor: '#ccc',
        borderBottomColor: '#ccc',
        borderTopWidth: 1,
        borderBottomWidth: 1,
    },
    empty: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    emptyTxt: {
        width: '100%',
        fontSize: 20,
        color: danger,
        textAlign: 'center'
    }
}
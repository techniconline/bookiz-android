import React, { Component } from 'react';
import { connect } from 'react-redux';
import {View, Image, TouchableOpacity} from 'react-native';
import _ from 'lodash';
import {Text, format_date, format_date_item, Navbar, ImageIcon, string_format_date_time} from "../../components";
import {Font_Medium, Rtl_Font_Medium} from "../../redux/types";
import translate from "../../translate";
import {danger, secondary, success} from "../../resources/Colors";
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";
import {IconLeftArrow} from "../../resources/Icon";

class Booking extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { item, direction, lang } = this.props;
        const { created_date, entity, reservation_status_text, booking_details } = item;
        const rtl = direction === 'rtl';
        const left = (
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <ImageIcon source={IconLeftArrow} size={25} />
            </TouchableOpacity>
        );
        console.log('single item', item );
        return (
            <View style={styles.container}>
                <Navbar leftComponent={left} rtl={rtl} title={translate.order[lang].toUpperCase()} />
                <Text style={styles.sectionTitle} rtl={rtl}>{translate.entity[lang]}</Text>
                <View style={rtl ? styles.rtl_entity : styles.entity}>
                    <View style={styles.img_container}>
                        <Image style={styles.img} source={{ uri: entity.media_image.url_thumbnail }} />
                    </View>
                    <View style={styles.entity_info}>
                        <Text rtl={rtl} style={rtl ? styles.rtl_title : styles.title}>{entity.title}</Text>
                        <Text rtl={rtl} style={styles.status}>{reservation_status_text}</Text>
                    </View>
                    <Text rtl={rtl} style={styles.date}>{format_date(string_format_date_time(created_date))}</Text>
                </View>
                <Text style={styles.sectionTitle} rtl={(direction === 'rtl')}>{translate.services[lang]}</Text>
                {_.map(booking_details, (service, key) => {
                    const { amount_text, title, discount_text } = service.entity_relation_service;
                    const full_name  = (service.entity_employee !== null) ? service.entity_employee.user.full_name : translate.anyone[lang];
                    return (
                        <View key={key} style={rtl ? styles.rtl_service : styles.service}>
                            <View style={{ flex: 1 }}>
                                <Text rtl={rtl} style={rtl ? styles.rtl_title : styles.title}>{title}</Text>
                                <Text rtl={rtl} style={styles.meta}>{translate.staff[lang]}: {full_name}</Text>
                                <Text rtl={rtl} style={styles.meta}>
                                    <Text rtl={rtl} style={styles.meta}>{translate.time[lang]}</Text>:
                                    <Text style={styles.meta}>{format_date(string_format_date_time(service.book_time))}</Text>
                                </Text>
                            </View>
                            <View style={styles.prices}>
                                {(discount_text !== null) ? <Text rtl={rtl} style={styles.old_price}>{discount_text}</Text> : null}
                                <Text rtl={rtl} style={styles.price}>{amount_text}</Text>
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }
}

const styles = {
    sectionTitle: {
        width: '100%',
        fontSize: 14,
        backgroundColor: '#e9e9e9',
        color: '#999',
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderTopColor: '#ccc',
        borderBottomColor: '#ccc',
        borderTopWidth: 1,
        borderBottomWidth: 1,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    entity: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    rtl_entity: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    img_container: {
        width: 70,
        height: 70,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#eee',
        backgroundColor: '#eee',
        overflow: 'hidden'
    },
    img: {
        width: '100%',
        height: '100%'
    },
    entity_info: {
        flex: 1,
        marginHorizontal: 10
    },
    title: {
        fontFamily: Font_Medium,
        fontSize: 17,
        color: '#666'
    },
    rtl_title: {
        fontFamily: Rtl_Font_Medium,
        fontSize: 16,
        color: '#666'
    },
    status: {
        fontSize: 13,
        color: '#666'
    },
    date: {
        flex: 1,
        maxWidth: 120,
        textAlign: 'center',
        fontSize: 13,
        color: '#999'
    },
    service: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    rtl_service: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    meta: {
        fontSize: 14,
        color: '#666'
    },
    prices: {
        flex: 1,
        maxWidth: 150,
        justifyContent: 'center',
        alignItems: 'center'
    },
    old_price: {
        fontSize: 13,
        width: '100%',
        color: danger,
        textAlign: 'center',
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    price: {
        width: '100%',
        fontSize: 16,
        textAlign: 'center',
        color: secondary
    },
};

const mapStateToProps = state => {
    return {
        lang: state.setting.lang,
        direction: state.setting.direction,
    }
};

export default connect(mapStateToProps, null)(Booking);
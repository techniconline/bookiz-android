import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {Text, format_date, string_format_date_time} from "../../components";
import {Font_Medium, Rtl_Font_Medium} from "../../redux/types";

const Item = ({ item, rtl }) => {
    const { created_date, entity, reservation_status_text } = item;
    return (
        <TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{ Actions.booking({ item }); }}>
            <View style={rtl ? styles.rtl_entity : styles.entity}>
                <View style={styles.img_container}>
                    <Image style={styles.img} source={{ uri: entity.media_image.url_thumbnail }} />
                </View>
                <View style={styles.entity_info}>
                    <Text rtl={rtl} style={rtl ? styles.rtl_entity_title : styles.entity_title}>{entity.title}</Text>
                    <Text rtl={rtl} style={styles.status}>{reservation_status_text}</Text>
                </View>
                <Text rtl={rtl} style={styles.date}>{format_date(string_format_date_time(created_date))}</Text>
            </View>
        </TouchableOpacity>
    )
};

const styles = {
    item: {
        width: '100%',
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: '#fff',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1
    },
    entity: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    rtl_entity: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    img_container: {
        width: 70,
        height: 70,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#eee',
        backgroundColor: '#eee',
        overflow: 'hidden'
    },
    img: {
        width: '100%',
        height: '100%'
    },
    entity_info: {
        flex: 1,
        marginHorizontal: 10
    },
    entity_title: {
        fontFamily: Font_Medium,
        fontSize: 16,
        color: '#666'
    },
    rtl_entity_title: {
        fontFamily: Rtl_Font_Medium,
        fontSize: 16,
        color: '#666'
    },
    status: {
        fontSize: 13,
        color: '#666'
    },
    date: {
        flex: 1,
        maxWidth: 120,
        textAlign: 'center',
        fontSize: 13,
        color: '#999'
    }
};

export default Item;
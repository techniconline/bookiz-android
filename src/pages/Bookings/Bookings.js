import React, { Component } from 'react';
import {View, FlatList, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { get_bookings } from '../../redux/actions';
import {ErrorPage, FullLoading, ImageIcon, Navbar, PageLoading, Text} from "../../components";
import styles from './BookingsStyles';
import translate from '../../translate';
import Item from "./Item";
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";
import Loading from "../../Loading";
import {IconLeftArrow} from "../../resources/Icon";

class Bookings extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.get_bookings();
    }
    handleLoadMore() {
        const {response, get_bookings, loading_page, loading_refresh, loading} = this.props;
        if(!loading && !loading_page && !loading_refresh && response.next_page_url !== null)
            get_bookings({ text: this.state.text }, response.next_page_url);
    }
    handleRefresh() {
        this.props.get_bookings(null, true);
    }
    renderEmpty() {
        const { direction, lang } = this.props;
        return <View style={styles.empty}><Text rtl={direction === 'rtl'} style={styles.emptyTxt}>{translate.empty_date[lang]}</Text></View>;
    }
    renderHeader() {
        const { lang, direction } = this.props;
        return <Text style={styles.resultTitle} rtl={(direction === 'rtl')}>{translate.results[lang]}</Text>
    }
    renderFooter() {
        const { response, loading_page, lang, direction } = this.props;
        if(response === null) return null;
        if(loading_page) return <PageLoading />;
        return <Text rtl={direction === 'rtl'} style={styles.footerText}>{response.total} {translate.total_results[lang]}</Text>
    }
    render() {
        const { loading_refresh, loading, data, errors, direction, lang, fromUrl, action } = this.props;
        const rtl = direction === 'rtl';
        const left = (fromUrl && action) ? (
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7 }} activeOpacity={0.6} onPress={() => { Actions.pop(); }}>
                <ImageIcon source={IconLeftArrow} size={25} />
            </TouchableOpacity>
        ) : null;
        return (loading) ? <Loading /> : (
            <View style={{ flex: 1 }}>
                <Navbar leftComponent={left} rtl={rtl} title={translate.orders[lang].toUpperCase()} />
                {(loading) ? <FullLoading /> : (errors !== null) ? <ErrorPage rtl={rtl} errors={errors} /> : (
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={data}
                            ListHeaderComponent={() => this.renderHeader()}
                            ListFooterComponent={() => this.renderFooter()}
                            ListEmptyComponent={() => this.renderEmpty()}
                            keyExtractor={(item, ind) => String(item.id)}
                            renderItem={({item}) => <Item rtl={rtl} item={item} />}
                            onEndReachedThreshold={0.01}
                            onEndReached={this.handleLoadMore.bind(this)}
                            onRefresh={this.handleRefresh.bind(this)}
                            refreshing={loading_refresh}
                        />
                    </View>
                )}
            </View>
        )
    }
}

const mapStateToProps = state => {
    const {
        data,
        response,
        errors,
        loading,
        loading_page,
        loading_refresh
    } = state.booking;
    return {
        lang: state.setting.lang,
        direction: state.setting.direction,
        data,
        response,
        errors,
        loading,
        loading_page,
        loading_refresh
    }
};

export default connect(mapStateToProps, { get_bookings })(Bookings);